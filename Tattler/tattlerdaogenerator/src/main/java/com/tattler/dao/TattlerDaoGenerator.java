package com.tattler.dao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class TattlerDaoGenerator {

    public static void main(String... args) throws Exception {

        Schema schema = new Schema(6, "mx.tattler.android.daos");

        Entity conversation = addConversation(schema);
        Entity message = addMessage(schema);

        new DaoGenerator().generateAll(schema, args[0]);

    }

    private static Entity addMessage(Schema schema){

        Entity message = schema.addEntity("TattlerMessage");

        message.addIdProperty();
        message.addStringProperty("messageId").unique();
        message.addStringProperty("conversationId").index();
        message.addStringProperty("body").notNull();
        message.addStringProperty("sender").notNull();
        message.addStringProperty("status").notNull();
        message.addStringProperty("type").notNull();
        message.addDateProperty("sentAt").notNull();
        message.addBooleanProperty("mine").notNull();

        return message;

    }

    private static Entity addConversation(Schema schema){

        Entity conversation = schema.addEntity("TattlerConversation");

        conversation.addIdProperty();
        conversation.addStringProperty("conversationId").index().unique();
        conversation.addIntProperty("receiverId");
        conversation.addStringProperty("receiverName");
        conversation.addStringProperty("receiverPicture");
        conversation.addStringProperty("receiverCompanyName");
        conversation.addStringProperty("lastMessage");
        conversation.addStringProperty("lastMessageType");
        conversation.addDateProperty("lastMessageDate");

        conversation.setHasKeepSections(true);

        return conversation;

    }

}
