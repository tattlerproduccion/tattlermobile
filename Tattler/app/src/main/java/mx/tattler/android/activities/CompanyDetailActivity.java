package mx.tattler.android.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.overlay.Marker;
import com.squareup.otto.Subscribe;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.constants.CompanyConstants;
import mx.tattler.android.events.tattler.GetBranchesForCompanyEvent;
import mx.tattler.android.events.tattler.GetBranchesForCompanyResultEvent;
import mx.tattler.android.events.tattler.GetCompanyEvent;
import mx.tattler.android.events.tattler.GetCompanyResultEvent;
import mx.tattler.android.fragments.CompanyDetailFragment;
import mx.tattler.android.fragments.listeners.OnBusinessInteractionListener;
import mx.tattler.android.rest.tattler.models.Branch;
import mx.tattler.android.rest.tattler.models.Company;
import mx.tattler.android.utils.location.BestLocationManager;
import mx.tattler.android.utils.location.GpsNotEnabledException;
import mx.tattler.android.utils.location.UnknownLastLocationException;
import mx.tattler.android.widgets.CompanyMapView;

public class CompanyDetailActivity extends AnimatedBusFragmentActivity implements OnBusinessInteractionListener, CompanyMapView.OnTouchListener {

    private static final String TAG = CompanyDetailActivity.class.getName();

    private View progressbarContainer;
    private View containerView;

    private CompanyDetailFragment profileFragment;
    private CompanyMapView map; // Might be null if Google Play services APK is not available.

    private boolean isMapOpen;

    private String companyId;
    private List<Branch> branches = new ArrayList<>();
    private Company currentCompany;
    private Branch nearestFromPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_profile);

        progressbarContainer = findViewById(R.id.pb_loading);
        containerView = findViewById(R.id.ly_container);

        companyId = getIntent().getStringExtra(CompanyConstants.COMPANY_ID);

        Log.i(TAG, String.format("Company id: %s", companyId));

        isMapOpen = false;

        profileFragment = (CompanyDetailFragment) getSupportFragmentManager().findFragmentById(R.id.fgr_business_profile);
        setUpMapIfNeeded();

        //Configure actionbar
        try {
            inflateCustomActionbar();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        showProgress(true);
        getBus().post(new GetCompanyEvent(companyId));
    }

    @Subscribe
    public void onCompanyLoaded(GetCompanyResultEvent event) {

        showProgress(false);
        currentCompany = event.company;

        setActionbarTitle(currentCompany.getName(), null);
        getBus().post(new GetBranchesForCompanyEvent(currentCompany));

        Log.i(TAG, String.format("Activity's Company[id]: %s, [name]: %s", currentCompany.getId(), currentCompany.getName()));

    }


    @Subscribe
    public void onBranchesLoaded(GetBranchesForCompanyResultEvent event) {
        showProgress(false);
        setMarkers(event.branches);
    }

    @Override
    public void onBranchesDetail() {

        Log.i(TAG, branches.toString());

        Bundle bundle = new Bundle();
        Parcelable branchesWrapped = Parcels.wrap(branches);
        Parcelable companyWrapped = Parcels.wrap(currentCompany);
        bundle.putParcelable("branches", branchesWrapped);
        bundle.putParcelable("company", companyWrapped);

        Intent intent = new Intent(this, BranchListActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);

    }

    @Override
    public void onPromotionsDetail() {

        Intent intent = new Intent(this, CompanyPromotionsActivity.class);
        intent.putExtra(CompanyConstants.COMPANY_ID, companyId);

        startActivity(intent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if (isMapOpen) {
                    closeMap();
                    return true;
                }
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isMapOpen) {
            closeMap();
        } else {
            finish();
        }
    }

    @Override
    public boolean isMapOpen() {
        return isMapOpen;
    }

    @Override
    public void expandMap() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.hide(profileFragment).commit();
        isMapOpen = true;
    }

    @Override
    public void closeMap() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.show(profileFragment).commit();
        //map.getUiSettings().setAllGesturesEnabled(false);
        isMapOpen = false;

        //Restore to nearest location
        if (nearestFromPoint != null) {
            LatLng near = nearestFromPoint.getLocationAsMapboxLatLng();
            map.setCenter(near);
            map.setZoom(15);
        }

    }

    @Override
    protected View getContainerView() {
        return containerView;
    }

    @Override
    protected View getProgressView() {
        return progressbarContainer;
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (map == null) {

            // Try to obtain the map from the SupportMapFragment.
            map = (CompanyMapView) findViewById(R.id.mapview);

            map.setListener(this);

        }
    }

    private void setMarkers(final List<Branch> branches) {

        this.branches = branches;

        LatLng point = null;
        String title;

        for (Branch branch : branches) {

            point = branch.getLocationAsMapboxLatLng();
            title = branch.getName();

            Marker marker = new Marker(map, title, "Sucursal", point);
            marker.setMarker(getResources().getDrawable(R.drawable.ic_btn_branch_marker));
            map.addMarker(marker);

        }

        if (branches.isEmpty()) {
            return;
        }


        BestLocationManager.getLastKnownLocation(this, new BestLocationManager.LocationManagerCallback() {
            @Override
            public void onLocation(Location location) {
                com.google.android.gms.maps.model.LatLng currentLocation = new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude());
                nearestFromPoint = Branch.nearestFromPoint(branches, currentLocation);

                if (nearestFromPoint != null) {
                    LatLng near = nearestFromPoint.getLocationAsMapboxLatLng();
                    map.setCenter(near);
                    map.setZoom(15);
                }
            }

            @Override
            public void onGpsNotEnabled() {
                askForGps();
            }

            @Override
            public void requestingLocation() {
                Toast.makeText(CompanyDetailActivity.this, "Solicitando ubicacion", Toast.LENGTH_SHORT).show();
            }

        });

    }

}
