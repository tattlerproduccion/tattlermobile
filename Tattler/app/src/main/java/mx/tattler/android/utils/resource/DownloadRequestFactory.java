package mx.tattler.android.utils.resource;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;

public class
        DownloadRequestFactory {

    private static final String TAG = DownloadRequestFactory.class.getName();

    public static void download(final Context context, String fileUrl, final ProgressBar progressBarDownload, final ThinDownloadManager downloadManager){

        if (!fileUrl.contains(".")) {
            return;
        }

        Uri downloadUri = Uri.parse(fileUrl);
        final String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
        final Uri destinationUri = Uri.parse(String.format("%s/%s", context.getExternalFilesDir("tattler"), fileName));

        File file = FileUtils.getFile(context, destinationUri);

        if (file != null) {
            FileUtils.tryToOpenFile(context, file);
            return;
        }

        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadListener(new DownloadStatusListener() {
                    @Override
                    public void onDownloadComplete(int i) {

                        progressBarDownload.setVisibility(View.GONE);
                        Log.i(TAG, String.format("Download finished, destination uri: %s", destinationUri.toString()));
                        FileUtils.tryToOpenFile(context, destinationUri);

                    }

                    @Override
                    public void onDownloadFailed(int i, int i2, String s) {

                        Log.e(TAG, "Download failed");
                        Toast.makeText(context, "Descarga ha fallado", Toast.LENGTH_LONG).show();
                        progressBarDownload.setVisibility(View.GONE);

                    }

                    @Override
                    public void onProgress(int i, long l, int i2) {

                    }

                });

        //Start download
        progressBarDownload.setVisibility(View.VISIBLE);
        downloadManager.add(downloadRequest);

    }

}
