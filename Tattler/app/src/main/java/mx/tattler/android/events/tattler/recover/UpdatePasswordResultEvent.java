package mx.tattler.android.events.tattler.recover;

import mx.tattler.android.rest.tattler.OperationResponse;

public class UpdatePasswordResultEvent {

    public final OperationResponse response;

    public UpdatePasswordResultEvent(OperationResponse response) {
        this.response = response;
    }
}
