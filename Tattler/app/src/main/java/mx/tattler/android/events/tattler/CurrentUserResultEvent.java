package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.User;

public class CurrentUserResultEvent {

    public final User user;

    public CurrentUserResultEvent(User user) {
        this.user = user;
    }
}
