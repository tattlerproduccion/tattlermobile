package mx.tattler.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.activities.ProfileActivity;
import mx.tattler.android.activities.SearchContactActivity;
import mx.tattler.android.adapters.FriendListAdapter;
import mx.tattler.android.constants.ContactConstants;
import mx.tattler.android.events.tattler.AllContactsEvent;
import mx.tattler.android.events.tattler.AllContactsResultEvent;
import mx.tattler.android.rest.tattler.models.Contact;

public class FriendListFragment extends BusFragment {

    private static final String TAG = FriendListFragment.class.getName();

    private FriendListAdapter adapter;

    private ViewGroup layoutContainer;
    private ProgressBar progressBar;

    public FriendListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_friend_list, container, false);

        layoutContainer = (ViewGroup)rootView.findViewById(R.id.list_layout_container);
        progressBar = (ProgressBar)rootView.findViewById(R.id.loading_progress_bar);

        EditText inputSearch = (EditText) rootView.findViewById(R.id.et_contact_filter);

        ListView list = (ListView)rootView.findViewById(R.id.lv_friends);

        ImageButton addContactBtn = (ImageButton)rootView.findViewById(R.id.ib_add_contact);
        addContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SearchContactActivity.class));
            }
        });

        adapter = new FriendListAdapter(getActivity());
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //super.onListItemClick(l, v, position, id);
                Contact contact = adapter.getItem(position);
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra(ContactConstants.CONTACT_ID, contact.getId());
                startActivity(intent);
            }
        });

        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }

        });

        listener.onRequest().post(new AllContactsEvent());
        showProgressBar(true);


        return rootView;
    }


    private void showProgressBar(boolean show){

        if(show){
            progressBar.setVisibility(View.VISIBLE);
            layoutContainer.setVisibility(View.GONE);
        }else{
            progressBar.setVisibility(View.GONE);
            layoutContainer.setVisibility(View.VISIBLE);
        }

    }

    @Subscribe
    public void onFriendsLoaded(AllContactsResultEvent event) {
        adapter.setData(event.friends);
        showProgressBar(false);
    }

}
