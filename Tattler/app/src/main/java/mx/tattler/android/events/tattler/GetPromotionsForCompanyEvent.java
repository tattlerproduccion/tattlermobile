package mx.tattler.android.events.tattler;

/**
 * Created by irving on 11/11/14.
 */
public class GetPromotionsForCompanyEvent {

    public String companyId;

    public GetPromotionsForCompanyEvent(String companyId) {
        this.companyId = companyId;
    }
}
