package mx.tattler.android.events.tattler;

import java.util.List;

import mx.tattler.android.rest.tattler.models.Contact;

public class GetContactContactsResultEvent {

    public List<Contact> contacts;

    public GetContactContactsResultEvent(List<Contact> contacts) {
        this.contacts = contacts;
    }
}
