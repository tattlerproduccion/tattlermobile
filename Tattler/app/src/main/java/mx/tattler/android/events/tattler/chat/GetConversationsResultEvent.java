package mx.tattler.android.events.tattler.chat;

import java.util.List;

import mx.tattler.android.rest.tattler.models.chat.Conversation;

public class GetConversationsResultEvent {

    public final List<Conversation> conversations;

    public GetConversationsResultEvent(List<Conversation> conversations) {
        this.conversations = conversations;
    }

}
