package mx.tattler.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.events.tattler.recover.AlreadyTokenEvent;
import mx.tattler.android.events.tattler.recover.CancelRecoverEvent;
import mx.tattler.android.events.tattler.recover.RecoverEmailSentEvent;
import mx.tattler.android.events.tattler.recover.SendRecoverEmailEvent;
import mx.tattler.android.events.tattler.recover.UpdatePasswordResultEvent;
import mx.tattler.android.events.tattler.recover.UserFoundEvent;
import mx.tattler.android.fragments.password.CredentialsConfirmationFragment;
import mx.tattler.android.fragments.password.CredentialsInputFragment;
import mx.tattler.android.fragments.password.UpdatePasswordFragment;

public class PasswordRecoveryActivity extends BusFragmentActivity {

    private static final String TAG = PasswordRecoveryActivity.class.getName();
    private CredentialsInputFragment credentialsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);

        credentialsFragment = new CredentialsInputFragment();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, credentialsFragment)
                    .commit();
        }

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.title_activity_profile_wizard), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Subscribe
    public void onUserFound(UserFoundEvent event) {
        Log.i(TAG, String.format("Usuario encontrado: %s", event.user.getEmail()));

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, CredentialsConfirmationFragment.newInstance(event.user))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack("confirmation_fragment")
                .commit();

    }

    @Subscribe
    public void onSendEmail(SendRecoverEmailEvent event){
        getSupportFragmentManager().beginTransaction().remove(credentialsFragment).commit();
    }

    @Subscribe
    public void onCancelRecovery(CancelRecoverEvent event) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new CredentialsInputFragment())
                .commit();
    }

    @Subscribe
    public void onAlreadyToken(AlreadyTokenEvent event) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new UpdatePasswordFragment())
                .commit();
    }

    @Subscribe
    public void onRecoverEmailSent(RecoverEmailSentEvent event) {

        Toast.makeText(this, "Email enviado. Verifica tu bandeja de entrada para continuar con el proceso de recuperación", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }

    @Subscribe
    public void onPasswordUpdated(UpdatePasswordResultEvent event) {

        if (!event.response.isSuccessful()) {
            Toast.makeText(this, "La contraseña no se pudo actualizar", Toast.LENGTH_LONG).show();
            return;
        }

        Toast.makeText(this, "Contraseña actualizada", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }

}
