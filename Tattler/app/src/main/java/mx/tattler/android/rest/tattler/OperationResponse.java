package mx.tattler.android.rest.tattler;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OperationResponse {

    @SerializedName("was_successful")
    @Expose
    private boolean successful;

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}
