package mx.tattler.android.fragments.password;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.events.tattler.recover.AlreadyTokenEvent;
import mx.tattler.android.events.tattler.recover.QueryUserEvent;
import mx.tattler.android.events.tattler.recover.UserNotFoundEvent;
import mx.tattler.android.fragments.BusFragment;
import mx.tattler.android.utils.text.TextContextHelper;

public class CredentialsInputFragment extends BusFragment {

    private static final String TAG = CredentialsInputFragment.class.getName();
    private EditText emailInput;

    public CredentialsInputFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_credentials_input, container, false);

        emailInput = (EditText) rootView.findViewById(R.id.et_email_recovery);

        emailInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                queryEmail();
                return true;
            }
        });


        Button queryUserBtn = (Button) rootView.findViewById(R.id.btn_query_user_by_email);
        queryUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queryEmail();
            }
        });

        return rootView;

    }

    @Subscribe
    public void onUserNotFound(UserNotFoundEvent event){
        Toast.makeText(getActivity(), String.format("Usuario con email %s no existe", event.email), Toast.LENGTH_LONG).show();
    }

    private void queryEmail(){

        String email = emailInput.getText().toString();

        if (!TextContextHelper.isValidEmail(email)) {
            emailInput.setError("Introduce correo electrónico válido");
            return;
        }

        listener.onRequest().post(new QueryUserEvent(email));
    }

}