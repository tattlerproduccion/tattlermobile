package mx.tattler.android.daos;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.TattlerApplication;

public class MessageRepository {

    public interface BatchUpsertCallback {
        void onNewMessages(List<TattlerMessage> newMessages);
    }

    public static void batchUpsert(final Context context, final List<TattlerMessage> messages, final BatchUpsertCallback callback) {

        final DaoSession session = getSession(context);

        session.runInTx(new Runnable() {
            @Override
            public void run() {

                List<TattlerMessage> newMessages = new ArrayList<TattlerMessage>();

                boolean isNew = false;

                for (TattlerMessage message : messages) {

                    TattlerMessage found = session.queryBuilder(TattlerMessage.class)
                            .where(TattlerMessageDao.Properties.MessageId.eq(message.getMessageId()))
                            .unique();

                    if (found != null) {
                        message.setId(found.getId());
                        isNew = false;
                    } else {
                        isNew = true;
                    }

                    session.insertOrReplace(message);

                    if (isNew)
                        newMessages.add(message);

                }

                callback.onNewMessages(newMessages);

            }
        });

    }

    public static void insertOrUpdate(Context context, TattlerMessage message) {
        getDao(context).insertOrReplace(message);
        ConversationRepository.updateLastMessageForConversation(context, message);
    }

    public static void clearMessages(Context context) {
        getDao(context).deleteAll();
    }

    public static List<TattlerMessage> getAllForConversation(Context context, TattlerConversation conversation) {

        String conversationId = conversation.getConversationId();
        if(TextUtils.isEmpty(conversationId))
            return new ArrayList<>();

        return getDao(context).queryBuilder().where(TattlerMessageDao.Properties.ConversationId.eq(conversationId)).list();
    }

    private static TattlerMessageDao getDao(Context c) {
        return getSession(c).getTattlerMessageDao();
    }

    private static TattlerConversationDao getConversationDao(Context c) {
        return getSession(c).getTattlerConversationDao();
    }

    private static DaoSession getSession(Context c) {
        return ((TattlerApplication) c.getApplicationContext()).getDaoSession();
    }

}
