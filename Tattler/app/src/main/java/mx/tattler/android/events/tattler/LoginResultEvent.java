package mx.tattler.android.events.tattler;

import mx.tattler.android.chat.UserCredentials;
import mx.tattler.android.rest.tattler.models.AccessResponse;

public class LoginResultEvent {

    public final AccessResponse accessResponse;
    public final UserCredentials credentials;

    public LoginResultEvent(AccessResponse accessResponse, UserCredentials credentials) {
        this.accessResponse = accessResponse;
        this.credentials = credentials;
    }

}