package mx.tattler.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.rest.tattler.models.Contact;

public class ContactListAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private List<Contact> friends = new ArrayList<Contact>();

    public ContactListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder;
        if (view == null) {

            view = inflater.inflate(R.layout.item_list_contact, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.tv_profile_name);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        Contact contact = friends.get(position);
        holder.name.setText(contact.getName() + contact.getLastName());

        return view;

    }

    @Override
    public int getCount() {
        return friends.size();
    }

    @Override
    public Contact getItem(int position) {
        return friends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {

        TextView name;
        ImageView profileImage;

    }

}