package mx.tattler.android.activities.call;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallListener;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import mx.tattler.android.R;
import mx.tattler.android.TattlerApplication;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.services.TattlerCallService;
import mx.tattler.android.utils.CircleTransform;
import mx.tattler.android.utils.call.AudioPlayer;

public class CallScreenActivity extends BaseActivity implements TattlerCallService.StartFailedListener {

    private String currentUserEmail;
    private TattlerConversation conversation; static final String TAG = CallScreenActivity.class.getSimpleName();
    private Picasso picasso;
    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;
    private Button mEndCallButton;
    private TextView mCallDuration;
    private ImageView imgBranch;
    private TextView mCallState;
    private TextView nameBranch;
    Call call;
    private TextView mCallerName;
    private String mCallId;
    private long mCallStart = 0;

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            CallScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mAudioPlayer != null) {
                        mAudioPlayer.stopProgressTone();
                    }
                    updateCallDuration();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_call_screen);

        try {
            currentUserEmail = ((TattlerApplication) getApplication()).getCurrentUserMail();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Get Conversation from Extras
        conversation = Parcels.unwrap(getIntent().getExtras().getParcelable(ConversationConstants.CONVERSATION));
        mCallDuration = (TextView) findViewById(R.id.callDuration);
        mCallerName = (TextView) findViewById(R.id.remoteUser);
        mCallState = (TextView) findViewById(R.id.callState);
        nameBranch = (TextView) findViewById(R.id.nameBranch);
        mEndCallButton = (Button) findViewById(R.id.hangupButton);
        imgBranch = (ImageView)findViewById(R.id.imgBranch);

        loadImage();

        if (conversation.getReceiverId() == null) {
            Toast.makeText(this, "No existe el contacto!!", Toast.LENGTH_LONG).show();
            finish();
        }
        mEndCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
            }
        });

    }

    public void loadImage(){
        Context contexto = this;
        Picasso.with(contexto).load(conversation.getReceiverPicture())
                .placeholder(R.drawable.ic_default_company)
                .transform(new CircleTransform())
                .error(R.drawable.ic_default_company)
                .into(imgBranch);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_call_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onServiceConnected() {
       getSinchServiceInterface().startClient(currentUserEmail);
       getSinchServiceInterface().setStartListener(this);
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {
        openPlaceCallActivity();
    }

    private void openPlaceCallActivity() {
        call = getSinchServiceInterface().callPhoneNumber("+52"+conversation.getPhone());
        String callId = call.getCallId();
        mAudioPlayer = new AudioPlayer(this);
        mCallStart = System.currentTimeMillis();
        mCallId = getIntent().getStringExtra(TattlerCallService.CALL_ID);
        mCallerName.setText(call.getRemoteUserId());
        nameBranch.setText(conversation.getReceiverCompanyName());
        mCallState.setText(call.getState().toString());
        mCallStart = System.currentTimeMillis();

        if (call != null) {
            call.addCallListener(new SinchCallListener());

        } else {
            Log.e(TAG, "Started with invalid callId, aborting.");
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mDurationTask.cancel();
        mTimer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        Log.e("Llamada Finalizada","La lLamada ha finalizado");

        if (call != null) {
            Log.e("Llamada Finalizada","La llamada a finalizado");
            call.hangup();
            getSinchServiceInterface().stopClient();
        }
        finish();
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (mCallStart > 0) {
            mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
        }
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + cause.toString());
            if (mAudioPlayer != null) {
                mAudioPlayer.stopProgressTone();
            }
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();
            Toast.makeText(CallScreenActivity.this, endMsg, Toast.LENGTH_LONG).show();
            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");

            if (mAudioPlayer != null) {
                mAudioPlayer.stopProgressTone();
            }
            mCallState.setText(call.getState().toString());
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
            mCallStart = System.currentTimeMillis();
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }
    }

}
