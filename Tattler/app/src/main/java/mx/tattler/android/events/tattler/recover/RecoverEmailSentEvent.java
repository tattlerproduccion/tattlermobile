package mx.tattler.android.events.tattler.recover;

import mx.tattler.android.rest.tattler.OperationResponse;

public class RecoverEmailSentEvent {

    public final OperationResponse response;

    public RecoverEmailSentEvent(OperationResponse response) {
        this.response = response;
    }

}
