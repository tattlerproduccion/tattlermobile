package mx.tattler.android.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.exception.BaseServiceException;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.util.List;

import mx.tattler.android.BusProvider;
import mx.tattler.android.TattlerApplication;
import mx.tattler.android.chat.ChatManager;
import mx.tattler.android.chat.ChatServiceFactory;
import mx.tattler.android.chat.ConversationNotificationFactory;
import mx.tattler.android.chat.PrivateChatManagerImpl;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.ConversationRepository;
import mx.tattler.android.daos.MessageRepository;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.daos.TattlerMessage;
import mx.tattler.android.daos.TattlerMessageFactory;
import mx.tattler.android.events.tattler.chat.AttachmentUploadedEvent;
import mx.tattler.android.events.tattler.chat.ConversationReadyToExportEvent;
import mx.tattler.android.events.tattler.chat.ConversationUpdatedEvent;
import mx.tattler.android.events.tattler.chat.EndChatEvent;
import mx.tattler.android.events.tattler.chat.GetConversationMessagesResultEvent;
import mx.tattler.android.events.tattler.chat.InitChatEvent;
import mx.tattler.android.events.tattler.chat.InitQbServiceEvent;
import mx.tattler.android.events.tattler.chat.MessageReceivedEvent;
import mx.tattler.android.events.tattler.chat.MessageSentEvent;
import mx.tattler.android.events.tattler.chat.NewMessagesArrivedEvent;
import mx.tattler.android.events.tattler.chat.RequestQbTokenEvent;
import mx.tattler.android.events.tattler.chat.RequestQbTokenResultEvent;
import mx.tattler.android.events.tattler.chat.TTMessageSentEvent;
import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.utils.ApplicationStateHelper;

public class TattlerService extends Service implements ChatManager.OnMessageLister{

    private static final String TAG = TattlerService.class.getName();

    private Bus bus;

    private ChatManager chatManager;
    private NotificationManager mNotificationManager;
    private String currentUserEmail;

    private TattlerConversation currentConversation;

    @Override
    public void onCreate() {
        super.onCreate();

        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        this.bus = BusProvider.getInstance();
        this.bus.register(this);
    }

    @Subscribe
    public void onInitQb(InitQbServiceEvent event) throws BaseServiceException, Exception {

        currentUserEmail = event.credentials.getEmail();
        String password = event.credentials.getPassword();

        ChatServiceFactory.instanceService(this, currentUserEmail, password, new ChatServiceFactory.OnInitServiceListener() {
            @Override
            public void onSuccess(QBChatService chatService) {
                Log.i(TAG, "Chat service obtained");
            }

            @Override
            public void onError(List errors) {
                for (Object error : errors)
                    Log.e(TAG, error.toString());

                //TODO: Close session and make user login again!
                //Toast.makeText(TattlerService.this, "Error al configurar chat. Inicia sesi�n nuevamente", Toast.LENGTH_LONG).show();
                //((TattlerApplication) getApplication()).clearSession();
                //ApplicationStateHelper.doRestart(getContext());

            }
        });

    }

    @Subscribe
    public void onInitChat(InitChatEvent event){

        currentConversation = event.conversation;

        chatManager = new PrivateChatManagerImpl(TattlerService.this, currentConversation.getReceiverId());

        //Disable notifications
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(ConversationConstants.TATTLER_NOTIFICATION_ID);

    }

    @Subscribe
    public void onEndChat(EndChatEvent event){
        currentConversation = null;
    }

    @Subscribe
    public void onTTMessageSent(TTMessageSentEvent event){

        Message remoteMessage = event.message;

        TattlerMessage message = TattlerMessageFactory.convert(remoteMessage);

        MessageRepository.insertOrUpdate(this, message);

        //TODO: Notify ui if qb message deliver failed
        try {
            chatManager.sendMessage(message);
        } catch (XMPPException | SmackException.NotConnectedException e) {
            e.printStackTrace();
        }

    }

    @Subscribe
    public void onAttachmentUploaded(AttachmentUploadedEvent event) throws XMPPException, SmackException.NotConnectedException {
        //chatManager.sendMessage(event.message);
    }

    @Override
    public void onMessageSent(TattlerMessage message) {
        bus.post(new MessageSentEvent(message));
    }

    @Subscribe
    public void onConversationMessages(GetConversationMessagesResultEvent event) {

        //Mark my messages
        for (Message message : event.messages)
            message.markIfUserOwns(currentUserEmail);

        final List<TattlerMessage> messages = TattlerMessageFactory.batchConvert(event.messages);

        MessageRepository.batchUpsert(this, messages, new MessageRepository.BatchUpsertCallback() {
            @Override
            public void onNewMessages(List<TattlerMessage> newMessages) {

                Log.i(TAG, "New Messages");

                if(!newMessages.isEmpty()){
                    ConversationRepository.updateLastMessageForConversation(TattlerService.this, newMessages.get(newMessages.size() - 1 ));
                }

                bus.post(new NewMessagesArrivedEvent(newMessages));

            }
        });

        //TODO: Notify conversation exporter
        if(event.toExport && !messages.isEmpty())
            bus.post(new ConversationReadyToExportEvent(messages.get(0).getConversationId()));

    }

    @Override
    public void onMessageReceived(QBChatMessage qbMessage) {

        TattlerConversation conversation = ConversationRepository.getByReceiverId(this, qbMessage.getSenderId());
        TattlerMessage message = TattlerMessageFactory.convert(qbMessage, conversation.getReceiverName(), conversation.getConversationId());
        message.setMine(false);

        MessageRepository.insertOrUpdate(this, message);

        if(currentConversation != null && currentConversation.getId().equals(conversation.getId())){
            bus.post(new MessageReceivedEvent(message));
        }else{
            Notification notification = ConversationNotificationFactory.create(this, message, conversation);
            mNotificationManager.notify(ConversationConstants.TATTLER_NOTIFICATION_ID, notification);
        }

        //Notify conversation list
        conversation.setLastMessageType(message.getType());
        conversation.setLastMessage(message.getBody());
        conversation.setLastMessageDate(message.getSentAt());
        bus.post(new ConversationUpdatedEvent(conversation));

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);

        //TODO: Release current chat event
        try {
            chatManager.release();
        } catch (XMPPException e) {
            Log.e(TAG, "failed to release chat", e);
        }

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onTokenExpired() {
        Toast.makeText(this, "Token caducado", Toast.LENGTH_LONG).show();
        //bus.post(new RequestQbTokenEvent());
    }

    @Subscribe
    public void onQbTokenReceived(RequestQbTokenResultEvent event){

    }

}