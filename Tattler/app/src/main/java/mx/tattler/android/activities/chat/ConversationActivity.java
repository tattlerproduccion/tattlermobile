package mx.tattler.android.activities.chat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import org.parceler.Parcels;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;
import mx.tattler.android.R;
import mx.tattler.android.TattlerApplication;
import mx.tattler.android.activities.AnimatedBusFragmentActivity;
import mx.tattler.android.adapters.chat.MessagesAdapter;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.MessageRepository;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.daos.TattlerMessage;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.chat.EndChatEvent;
import mx.tattler.android.events.tattler.chat.GetConversationMessagesEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsResultEvent;
import mx.tattler.android.events.tattler.chat.InitChatEvent;
import mx.tattler.android.events.tattler.chat.MessageReceivedEvent;
import mx.tattler.android.events.tattler.chat.MessageSentEvent;
import mx.tattler.android.events.tattler.chat.NewMessagesArrivedEvent;
import mx.tattler.android.events.tattler.chat.SendTTMessageEvent;
import mx.tattler.android.rest.tattler.models.Promotion;
import mx.tattler.android.rest.tattler.models.User;
import mx.tattler.android.rest.tattler.models.chat.Conversation;
import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.utils.location.BestLocationManager;
import mx.tattler.android.utils.location.GpsNotEnabledException;
import mx.tattler.android.utils.location.UnknownLastLocationException;
import mx.tattler.android.utils.resource.FileUtils;
import mx.tattler.android.utils.resource.ImageMediaHelper;
import mx.tattler.android.utils.resource.TypedFileFromBytes;
import mx.tattler.android.utils.text.DateTextHelper;
import mx.tattler.android.widgets.ConversationActionbar;

public class ConversationActivity extends AnimatedBusFragmentActivity implements MessagesAdapter.DateMessageListener {

    private static final String TAG = ConversationActivity.class.getName();

    private static final String GOOGLE_MAPS_URI = "http://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=17&size=537x437&key=AIzaSyB9oHzp0DtDqkfb4PCxFNRf9Yyy9-9Z8Lo";

    private static final int ATTACHMENT_REQUEST_CODE = 1111;
    private static final int GALLERY_REQUEST_CODE = 222;
    private static final int CAMERA_REQUEST_CODE = 333;
    private static final int VIDEO_GALLERY_REQUEST_CODE = 444;
    private static final int VIDEO_CAPTURE_REQUEST_CODE = 122;

    private String currentUserEmail;

    private MessagesAdapter adapter;
    private EmojiconEditText emojiconEditText;

    private TattlerConversation conversation;

    private Uri newCameraImageUri;
    private ConversationActionbar conversationActionbar;

    private TextView conversationDateView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        currentUserEmail = ((TattlerApplication) getApplication()).getCurrentUserMail();

        //Get from Extras
        conversation = Parcels.unwrap(getIntent().getExtras().getParcelable(ConversationConstants.CONVERSATION));

        if (conversation == null || conversation.getReceiverId() == null) {
            Toast.makeText(this, "No existe contacto!", Toast.LENGTH_LONG).show();
            finish();
        }

        conversationDateView = (TextView) findViewById(R.id.tv_conversation_date);
        conversationDateView.setText(DateTextHelper.nowChatHeaderFormat());

        ListView messagesListView = (ListView) findViewById(R.id.lv_messages);
        adapter = new MessagesAdapter(this, this);
        messagesListView.setAdapter(adapter);

        View rootView = findViewById(R.id.root_view);
        emojiconEditText = (EmojiconEditText) findViewById(R.id.et_message_input);
        ImageView emojiButton = (ImageView) findViewById(R.id.btn_emoji);
        configureEmojicons(rootView, emojiconEditText, emojiButton);

        ImageView submitButton = (ImageView) findViewById(R.id.btn_submit_message);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = emojiconEditText.getText().toString();
                if (TextUtils.isEmpty(message))
                    return;

                sendMessage(message);

            }
        });

        //Configure actionbar
        conversationActionbar = ConversationActionbar.instance(getActionBar(), this, conversation);

        List<TattlerMessage> messages = MessageRepository.getAllForConversation(this, conversation);
        adapter.setMessages(messages);

        getBus().post(new InitChatEvent(conversation));

        //Request conversations from Tattler API
        getBus().post(new GetConversationsEvent());
        conversationActionbar.showLoading(true);

    }

    private void configureEmojicons(View rootView, final EmojiconEditText emojiconEditText, final ImageView emojiButton) {

        // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, this);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //Set on emojicon click listener
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                emojiconEditText.append(emojicon.getEmoji());
            }
        });

        //Set on backspace click listener
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new android.widget.PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_smile);
            }

        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                emojiconEditText.append(emojicon.getEmoji());
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if (!popup.isShowing()) {

                    //If keyboard is visible, simply show the emoji popup
                    if (popup.isKeyBoardOpen()) {
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else {
                        emojiconEditText.setFocusableInTouchMode(true);
                        emojiconEditText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(emojiconEditText, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else {
                    popup.dismiss();
                }
            }
        });

    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }

    @Override
    protected View getContainerView() {
        return null;
    }

    @Override
    protected View getProgressView() {
        return null;
    }

    @Override
    public void onBackPressed() {

        //TODO: Release current chat listener
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_gallery:

                Intent gallery_intent = new Intent(Intent.ACTION_PICK);
                gallery_intent.setType("image/*");
                startActivityForResult(gallery_intent, GALLERY_REQUEST_CODE);
                break;

            case R.id.action_add_attachment:

                Intent attachment_intent = new Intent(getBaseContext(), FileDialogActivity.class);
                attachment_intent.putExtra(FileDialogActivity.START_PATH, "/sdcard");
                attachment_intent.putExtra(FileDialogActivity.CAN_SELECT_DIR, false);
                attachment_intent.putExtra(FileDialogActivity.SELECTION_MODE, FileDialogActivity.SelectionMode.MODE_OPEN);
                attachment_intent.putExtra(FileDialogActivity.FORMAT_FILTER, new String[]{"xml", "pdf", "doc",
                        "docx", "ppt", "pptx", "xlsx", "xls", "txt"});
                startActivityForResult(attachment_intent, ATTACHMENT_REQUEST_CODE);

                break;
            case R.id.action_pick_photo:

                // Camera
                newCameraImageUri = getContentResolver()
                        .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                new ContentValues());

                Intent photoFromCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                photoFromCamera.putExtra(MediaStore.EXTRA_OUTPUT, newCameraImageUri);
                photoFromCamera.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);

                startActivityForResult(photoFromCamera, CAMERA_REQUEST_CODE);

                break;
            case R.id.action_share_location:

                BestLocationManager.getLastKnownLocation(this, new BestLocationManager.LocationManagerCallback() {
                    @Override
                    public void onLocation(Location location) {
                        String mapsUriBody = location.getLatitude() + "," + location.getLongitude();
                        Message message = Message.toSentInstance(mapsUriBody, conversation.getReceiverId(), currentUserEmail, Message.Types.Geolocation);
                        getBus().post(new SendTTMessageEvent(message));
                    }

                    @Override
                    public void onGpsNotEnabled() {
                        askForGps();
                    }

                    @Override
                    public void requestingLocation() {
                        Toast.makeText(ConversationActivity.this, "Solicitando ubicacion", Toast.LENGTH_SHORT).show();
                    }
                });

                break;
            case R.id.action_pick_video:

                DialogFragment pickPhotoSourceDialog = new DialogFragment() {

                    @NonNull
                    @Override
                    public Dialog onCreateDialog(Bundle savedInstanceState) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                getActivity());
                        builder.setItems(R.array.spanish_video_sources,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {

                                        switch (which) {
                                            case 0:

                                                // Gallery
                                                Intent videoIntent = new Intent(Intent.ACTION_PICK);
                                                videoIntent.setType("video/*");
                                                getActivity().startActivityForResult(videoIntent, VIDEO_GALLERY_REQUEST_CODE);
                                                break;

                                            default:

                                                // Video
                                                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                                //photoFromCamera.putExtra(MediaStore.EXTRA_OUTPUT, newCameraImageUri);
                                                if (takeVideoIntent.resolveActivity(getPackageManager()) != null)
                                                    getActivity().startActivityForResult(takeVideoIntent, VIDEO_CAPTURE_REQUEST_CODE);

                                                break;
                                        }

                                    }
                                });
                        return builder.create();
                    }
                };

                pickPhotoSourceDialog.show(getSupportFragmentManager(),
                        "pickPhotoSourceDialog");

                break;
            default:
                break;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        Uri selectedFileUri = new Uri.Builder().build();
        Message message = Message.toSentInstance("", conversation.getReceiverId(), currentUserEmail, Message.Types.Text);

        switch (requestCode) {
            case GALLERY_REQUEST_CODE:

                if (data == null) {
                    Toast.makeText(this, "Error al obtener archivo de galería", Toast.LENGTH_LONG).show();
                    return;
                }

                selectedFileUri = data.getData();

                message.setType(Message.Types.Image.name());
                break;
            case VIDEO_GALLERY_REQUEST_CODE:

                if (data == null) {
                    Toast.makeText(this, "Error al obtener archivo de galería", Toast.LENGTH_LONG).show();
                    return;
                }

                selectedFileUri = data.getData();

                if (ImageMediaHelper.invalidVideoDuration(this, selectedFileUri)) {
                    Toast.makeText(this, "Duración máxima de video permitida son 3 segundos", Toast.LENGTH_LONG).show();
                    return;
                }

                message.setType(Message.Types.Video.name());
                break;

            case ATTACHMENT_REQUEST_CODE:

                if (data == null) {
                    Toast.makeText(this, "Error al obtener archivo de galería", Toast.LENGTH_LONG).show();
                    return;
                }

                //TODO: send message with file name, upload the file
                String filePath = data.getStringExtra(FileDialogActivity.RESULT_PATH);
                selectedFileUri = FileUtils.getUri(new File(filePath));

                message.setType(Message.Types.File.name());
                break;

            case CAMERA_REQUEST_CODE:

                try {
                    selectedFileUri = ImageMediaHelper.resizedBitmapUriIfNeeded(this, newCameraImageUri);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Error al subir imagen de la cámara", Toast.LENGTH_LONG).show();
                    return;
                }
                message.setType(Message.Types.Image.name());
                break;

            case VIDEO_CAPTURE_REQUEST_CODE:

                selectedFileUri = data.getData();

                if (ImageMediaHelper.invalidVideoDuration(this, selectedFileUri)) {
                    Toast.makeText(this, "Duración máxima de video permitida son 3 segundos", Toast.LENGTH_LONG).show();
                    return;
                }

                message.setType(Message.Types.Video.name());
                break;

            default:
                Toast.makeText(this, "Operación no reconocida", Toast.LENGTH_LONG).show();
                return;
        }


        message.setDataUri(selectedFileUri);

        TypedFileFromBytes attachment = TypedFileFromBytes.instance(this, selectedFileUri);
        getBus().post(new SendTTMessageEvent(attachment, message));
        conversationActionbar.showLoading(true);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("message", emojiconEditText.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        emojiconEditText.setText(savedInstanceState.getString("message"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.stopDownloads();

        getBus().post(new EndChatEvent(conversation));

    }

    //Request conversations so we can try to recover conversation history
    @Subscribe
    public void onConversations(GetConversationsResultEvent event) {

        boolean found = false;

        for (Conversation fetched : event.conversations) {
            if (fetched.hasSameReceiver(conversation.getReceiverId())) {
                //Request conversation messages from Tattler API, if exits
                getBus().post(new GetConversationMessagesEvent(fetched, false));
                found = true;
                break;
            }
        }

        if (!found)
            conversationActionbar.showLoading(false);

    }

    @Subscribe
    public void onNewMessagesArrived(NewMessagesArrivedEvent event) {
        conversationActionbar.showLoading(false);
        adapter.addMessages(event.messages);

        //Post promotion if exists
        Intent intent = getIntent();
        if (intent == null)
            return;

        Bundle extras = intent.getExtras();

        if (extras == null)
            return;

        Parcelable parcelable = extras.getParcelable(ConversationConstants.PROMOTION);
        if (parcelable == null)
            return;

        Promotion promotion = Parcels.unwrap(parcelable);
        if (promotion == null)
            return;

        Gson gson = new Gson();
        String json = gson.toJson(promotion);

        Message message = Message.toSentInstance(json, conversation.getReceiverId(), currentUserEmail, Message.Types.Promotion);
        getBus().post(new SendTTMessageEvent(message));

    }

    @Subscribe
    public void onMessageSent(MessageSentEvent event) {
        //TODO: Refactor sent messages
        addMessage(event.message);
        conversationActionbar.showLoading(false);
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {
        //TODO: Filter sent message from other api errors
        String rawError = event.error.getRawBody();

        Log.e(TAG, rawError);
        Toast.makeText(this, rawError, Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void onMessageReceived(MessageReceivedEvent event) {
        adapter.addMessage(event.message);
    }

    private void sendMessage(String text) {

        Message message = Message.toSentInstance(text, conversation.getReceiverId(), currentUserEmail, Message.Types.Text);
        User sender = new User();
        sender.setEmail(currentUserEmail);

        emojiconEditText.setText("");
        getBus().post(new SendTTMessageEvent(message));

    }

    private void addMessage(TattlerMessage message) {
        adapter.addMessage(message);
    }

    @Override
    public void onDateChanged(Date date) {
        conversationDateView.setText(DateTextHelper.formatToChatHeader(date));
    }

}
