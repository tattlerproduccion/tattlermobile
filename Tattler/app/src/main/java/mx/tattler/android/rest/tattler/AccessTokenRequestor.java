package mx.tattler.android.rest.tattler;

import mx.tattler.android.rest.tattler.models.AccessResponse;

public interface AccessTokenRequestor {
    AccessResponse refreshToken(String token);
}