package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.Company;

public class GetBranchesForCompanyEvent {

    public Company company;

    public GetBranchesForCompanyEvent(Company company) {
        this.company = company;
    }

}
