package mx.tattler.android.events.tattler.login;

public class EmailAsUsernameExceptionEvent {

    private final String username;

    public EmailAsUsernameExceptionEvent(String username) {
        this.username = username;
    }
}
