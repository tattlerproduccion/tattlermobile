package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Company {

    @Expose
    private String id;
    @Expose
    private String name;
    @Expose
    private String rfc;
    @Expose
    private String description;
    @Expose
    private String category;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("background_image")
    @Expose
    private String backgroundImage;
    @Expose
    private String address;
    @SerializedName("allows_phone_calling")
    @Expose
    private Boolean allowsPhoneCalling;
    @SerializedName("allows_tattler_calling")
    @Expose
    private Boolean allowsTattlerCalling;
    @SerializedName("allows_tattler_messaging")
    @Expose
    private Boolean allowsTattlerMessaging;
    @SerializedName("favorites_count")
    @Expose
    private Integer favoritesCount;
    @SerializedName("is_favorite")
    @Expose
    private String isFavorite;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getAllowsPhoneCalling() {
        return allowsPhoneCalling;
    }

    public void setAllowsPhoneCalling(Boolean allowsPhoneCalling) {
        this.allowsPhoneCalling = allowsPhoneCalling;
    }

    public Boolean getAllowsTattlerCalling() {
        return allowsTattlerCalling;
    }

    public void setAllowsTattlerCalling(Boolean allowsTattlerCalling) {
        this.allowsTattlerCalling = allowsTattlerCalling;
    }

    public Boolean getAllowsTattlerMessaging() {
        return allowsTattlerMessaging;
    }

    public void setAllowsTattlerMessaging(Boolean allowsTattlerMessaging) {
        this.allowsTattlerMessaging = allowsTattlerMessaging;
    }

    public Integer getFavoritesCount() {
        return favoritesCount;
    }

    public void setFavoritesCount(Integer favoritesCount) {
        this.favoritesCount = favoritesCount;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

}