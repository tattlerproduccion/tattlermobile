package mx.tattler.android.fragments.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.ReviewItem;
import com.tech.freak.wizardpager.model.TextPage;

import java.util.ArrayList;

public class CustomImagePage extends TextPage {

    public CustomImagePage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return CustomImageFragment.create(getKey());
    }

    public CustomImagePage setValue(String value) {
        mData.putString(SIMPLE_DATA_KEY, value);
        return this;
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        String displayValue = mData.getString(SIMPLE_DATA_KEY) != null ? "Imagen seleccionada" : "No seleccionada";
        dest.add(new ReviewItem(getTitle(), displayValue,
                getKey()));
    }

}