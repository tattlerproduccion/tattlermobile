package mx.tattler.android.utils.adapters;

import android.view.LayoutInflater;
import android.view.View;

import mx.tattler.android.R;
import mx.tattler.android.rest.tattler.models.chat.Message;

public class MessageViewIdFactory {

    private MessageViewIdFactory(){}

    public static View getMessageView(LayoutInflater vi, int message_type){

        if(message_type == Message.MessageTypes.MESSAGE_TYPE_NORMAL){
            return vi.inflate(R.layout.item_list_message, null);
        } else if(message_type == Message.MessageTypes.MESSAGE_TYPE_FILE_ATTACHMENT){
            return vi.inflate(R.layout.item_list_message_attachment, null);
        } else if(message_type == Message.MessageTypes.MESSAGE_TYPE_LOCATION){
            return vi.inflate(R.layout.item_list_message_location, null);
        } else if(message_type == Message.MessageTypes.MESSAGE_TYPE_PHOTO){
            return vi.inflate(R.layout.item_list_message_photo, null);
        } else if(message_type == Message.MessageTypes.MESSAGE_TYPE_VIDEO) {
            return vi.inflate(R.layout.item_list_message_video, null);
        }

        return null;
    }

}
