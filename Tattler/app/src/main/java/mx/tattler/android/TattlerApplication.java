package mx.tattler.android;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newrelic.agent.android.NewRelic;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import mx.tattler.android.activities.MainDrawerActivity;
import mx.tattler.android.activities.wizard.ProfileWizardActivity;
import mx.tattler.android.chat.UserCredentials;
import mx.tattler.android.constants.UserConstants;
import mx.tattler.android.daos.ConversationRepository;
import mx.tattler.android.daos.DaoMaster;
import mx.tattler.android.daos.DaoSession;
import mx.tattler.android.daos.MessageRepository;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.LoginResultEvent;
import mx.tattler.android.events.tattler.chat.InitQbServiceEvent;
import mx.tattler.android.events.tattler.profile.InitialCurrentUserEvent;
import mx.tattler.android.events.tattler.profile.InitialCurrentUserResultEvent;
import mx.tattler.android.rest.maps.GooglePlaceService;
import mx.tattler.android.rest.maps.GooglePlacesApi;
import mx.tattler.android.rest.tattler.AccessTokenRepository;
import mx.tattler.android.rest.tattler.CustomInterceptor;
import mx.tattler.android.rest.tattler.TattlerApi;
import mx.tattler.android.rest.tattler.TattlerRestService;
import mx.tattler.android.rest.tattler.TattlerTokenApi;
import mx.tattler.android.rest.tattler.TokenRequestorService;
import mx.tattler.android.rest.tattler.models.AccessResponse;
import mx.tattler.android.services.TattlerService;
import mx.tattler.android.utils.ApplicationStateHelper;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class TattlerApplication extends Application implements AccessTokenRepository {

    private static final String TAG = TattlerApplication.class.getName();

    private Bus bus = BusProvider.getInstance();

    private TattlerRestService tattlerRestService;

    private SharedPreferences preferences;
    private DaoSession daoSession;
    private CustomInterceptor interceptor;

    @Override
    public void onCreate() {
        super.onCreate();

        //db configure
        setupDatabase();

        NewRelic.withApplicationToken("AAce8f2e306440bda580ed06c6f8a5340aca9c0c6b").start(this);

        //init tattler non-sticky service
        startService(new Intent(this, TattlerService.class));
        // startService(new Intent(this, TattlerCallService.class));
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        GooglePlaceService placeService = new GooglePlaceService(buildGoogleApi(), bus);
        bus.register(placeService);

        String accessToken = preferences.getString(UserConstants.ACCESS_TOKEN, null);
        String refreshToken = preferences.getString(UserConstants.REFRESH_TOKEN, null);

        interceptor = new CustomInterceptor(this, buildTokenApi(), accessToken, refreshToken);
        tattlerRestService = new TattlerRestService(buildTattlerApi(interceptor), bus);

        bus.register(tattlerRestService);
        bus.register(this);

        //resetDb();

    }

    private void resetDb() {

        ConversationRepository.clearConversations(this);
        MessageRepository.clearMessages(this);

    }

    @Subscribe
    public void onLoginSuccess(LoginResultEvent event) {

        Log.i(TAG, "User logged successfully");

        AccessResponse response = event.accessResponse;

        saveTokensThenConfigureInterceptor(response);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(UserConstants.USER_CHAT_SESSION, response.chatSessionAsJson());
        editor.putString(UserConstants.USER_CREDENTIALS, new Gson().toJson(event.credentials));
        editor.commit();

        TattlerApi refreshedApi = buildTattlerApi(interceptor);
        tattlerRestService.setApi(refreshedApi);

        bus.post(new InitialCurrentUserEvent());

    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {
        Log.e(TAG, event.error.getRawBody());
    }

    @Subscribe
    public void onInitialCurrentUser(InitialCurrentUserResultEvent event) throws Exception {

        UserCredentials credentials = getCredentials();

        if (credentials == null) { //Somethings is wrong, so better restart application
            clearSession();
            ApplicationStateHelper.doRestart(this);
            return;
        }

        bus.post(new InitQbServiceEvent(credentials));
        Log.i(TAG, "onInitialCurrentUser");
        Intent intent = (event.user.getName() == null) ? new Intent(this, ProfileWizardActivity.class) : new Intent(this, MainDrawerActivity.class);
        //Intent intent = new Intent(this, ProfileWizardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);


    }

    @Override
    public boolean saveTokensThenConfigureInterceptor(AccessResponse accessResponse) {

        String accessToken = accessResponse.getAccessToken();
        String refreshToken = accessResponse.getRefreshToken();

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(UserConstants.ACCESS_TOKEN, accessToken);
        editor.putString(UserConstants.REFRESH_TOKEN, refreshToken);
        editor.commit();

        interceptor.setTokens(accessToken, refreshToken);

        return true;
    }

    public void clearSession() {
        preferences.edit().clear().commit();
        resetDb();
    }

    public String getCurrentUserMail() {

        UserCredentials credentials = getCredentials();
        if (credentials == null)
            return "";

        return credentials.getEmail();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    private GooglePlacesApi buildGoogleApi() {
        return new RestAdapter.Builder()
                .setEndpoint(GooglePlacesApi.API_URL)
                .build()
                .create(GooglePlacesApi.class);
    }

    private TattlerApi buildTattlerApi(CustomInterceptor interceptor) {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(interceptor);

        return new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(TattlerApi.API_URL)
                .build()
                .create(TattlerApi.class);
    }

    private TokenRequestorService buildTokenApi() {

        return TokenRequestorService.instance(new RestAdapter.Builder()
                .setEndpoint(TattlerApi.API_URL)
                .build()
                .create(TattlerTokenApi.class));

    }

    private UserCredentials getCredentials() {

        String credentialsJson = preferences.getString(UserConstants.USER_CREDENTIALS, null);

        if (credentialsJson == null)
            return null;

        return new Gson().fromJson(credentialsJson, new TypeToken<UserCredentials>() {
        }.getType());

    }

    private void setupDatabase() {

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "tattler-dev-db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();

    }

}