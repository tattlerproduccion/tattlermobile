package mx.tattler.android.rest.maps.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class GeocodeResponse {

    @Expose
    private String status;
    @Expose
    private List<Result> results = new ArrayList<Result>();

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The results
     */
    public List<Result> getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(List<Result> results) {
        this.results = results;
    }

    public AddressComponent getLocality() {

        AddressComponent locality = null;

        if(results.size() > 0){
            locality = results.get(0).getLocalityComponent();
        }

        return locality;

    }
}