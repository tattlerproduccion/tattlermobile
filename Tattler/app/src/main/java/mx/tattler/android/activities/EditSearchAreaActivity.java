package mx.tattler.android.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.otto.Subscribe;

import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.adapters.PlacesResultAdapter;
import mx.tattler.android.events.maps.GeocodeLatLngEvent;
import mx.tattler.android.events.maps.GeocodeLatLngResultEvent;
import mx.tattler.android.events.maps.PlaceDetailEvent;
import mx.tattler.android.events.maps.PlaceDetailsResultEvent;
import mx.tattler.android.events.maps.PlaceInputEvent;
import mx.tattler.android.events.maps.PlaceQueryResultEvent;
import mx.tattler.android.rest.maps.models.AddressComponent;
import mx.tattler.android.rest.maps.models.GeocodeResponse;
import mx.tattler.android.rest.maps.models.Location;
import mx.tattler.android.rest.maps.models.PlaceResponse;
import mx.tattler.android.rest.maps.models.Prediction;
import mx.tattler.android.utils.cursors.CursorFactory;
import mx.tattler.android.utils.location.BestLocationManager;
import mx.tattler.android.utils.map.CircleManagerListener;
import mx.tattler.android.utils.map.MapAreaManager;
import mx.tattler.android.utils.map.MapAreaWrapper;

public class EditSearchAreaActivity extends BusActivity {

    private static final String TAG = EditSearchAreaActivity.class.getName();
    private static final String COUNTRY_CODE = "country:mx";
    public static String[] from = new String[]{"_id", "place_id", "description"};
    public static int[] to = new int[]{android.R.id.text1, android.R.id.text2};

    private SharedPreferences preferences;
    private GoogleMap map;
    private SearchView locationSearch;
    private SimpleCursorAdapter mSearchViewAdapter;
    private MapAreaManager circleManager;

    private String currentLocality;
    private LatLng currentLocationPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_search);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        locationSearch = (SearchView) findViewById(R.id.sv_location_search);

        LocalitySearchListener searcListener = new LocalitySearchListener();

        locationSearch.setOnQueryTextListener(searcListener);
        locationSearch.setOnSuggestionListener(searcListener);

        mSearchViewAdapter = new PlacesResultAdapter(this, android.R.layout.simple_list_item_1,
                null, from, null, -1000);
        locationSearch.setSuggestionsAdapter(mSearchViewAdapter);

        setUpMapIfNeeded();

        //Set my currentLocationPoint
        BestLocationManager.getLastKnownLocation(this, new BestLocationManager.LocationManagerCallback() {
            @Override
            public void onLocation(android.location.Location location) {
                EditSearchAreaActivity.this.currentLocationPoint = new LatLng(location.getLatitude(), location.getLongitude());
                setLocation(currentLocationPoint, true);
            }

            @Override
            public void onGpsNotEnabled() {
                askForGps();
            }

            @Override
            public void requestingLocation() {
                Toast.makeText(EditSearchAreaActivity.this, "Solicitando ubicacion", Toast.LENGTH_SHORT).show();
            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_map_done:
                saveLocation();

                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();

                break;
            case R.id.action_my_location:

                if (currentLocationPoint == null)
                    Toast.makeText(this, "No podemos saber tu ubicación si tienes el GPS desactivado.", Toast.LENGTH_LONG).show();
                else
                    setLocation(currentLocationPoint, true);

                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Subscribe
    public void onPlacesLoaded(PlaceQueryResultEvent event) {
        List<Prediction> predictions = event.predictions;
        mSearchViewAdapter.changeCursor(CursorFactory.convertToCursor(predictions, from));
    }

    @Subscribe
    public void onPlaceLoaded(PlaceDetailsResultEvent event) {

        PlaceResponse response = event.response;
        Location location = response.getLocation();

        AddressComponent locality = response.getLocality();

        if (locality != null) {

            currentLocality = locality.getLongName();
            setActionbarSubtitle(currentLocality);

            LatLng point = new LatLng(location.getLat(), location.getLng());
            setLocation(point, false);
        }

    }

    @Subscribe
    public void onGeocode(GeocodeLatLngResultEvent event) {

        GeocodeResponse response = event.response;
        AddressComponent locality = response.getLocality();

        if (locality != null) {
            currentLocality = locality.getLongName();
            setActionbarSubtitle(currentLocality);
        }

    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void saveLocation() {

        LatLng point = circleManager.getCenter();

        String latitude = String.valueOf(point.latitude);
        String longitude = String.valueOf(point.longitude);

        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(getString(R.string.latitude), latitude);
        editor.putString(getString(R.string.longitude), longitude);

        Float radius = circleManager.getRadius();

        if (radius != null) {
            editor.putFloat(getString(R.string.radius), radius);
        }

        if (currentLocality != null) {
            editor.putString(getString(R.string.locality), currentLocality);
        }

        editor.commit();

    }

    private void setUpMapIfNeeded() {
        if (map == null) {
            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.full_map)).getMap();

            if (map != null)
                setupMap();

        }
    }

    private void setupMap() {

        Double latitude = Double.parseDouble(preferences.getString(getString(R.string.latitude), getString(R.string.default_latitude)));
        Double longitude = Double.parseDouble(preferences.getString(getString(R.string.longitude), getString(R.string.default_longitude)));
        double radius = (double) preferences.getFloat(getString(R.string.radius), MapAreaManager.DEFAULT_MAP_AREA_RADIUS);
        String locality = preferences.getString(getString(R.string.locality), "Mérida");

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.title_activity_map_search), locality);
            hideTattlerActionbarLogo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        LatLng point = new LatLng(latitude, longitude);
        circleManager = MapAreaManager.newInstance(map, point, radius, new CircleListener());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 12));

    }

    private void setLocation(LatLng point, boolean notifyListener) {
        if (map != null) {
            circleManager.moveArea(point, notifyListener);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 12));
        }
    }

    class CircleListener implements CircleManagerListener {
        @Override
        public void onCreateCircle(MapAreaWrapper draggableCircle) {

        }

        @Override
        public void onResizeCircleEnd(MapAreaWrapper draggableCircle) {
            //radius = String.valueOf(draggableCircle.getRadius());
        }

        @Override
        public void onMoveCircleEnd(MapAreaWrapper draggableCircle) {

            Log.i(TAG, draggableCircle.getCenter().latitude + ", "
                    + draggableCircle.getCenter().longitude);

            getBus().post(new GeocodeLatLngEvent(draggableCircle.getCenter()));
        }

        @Override
        public void onMoveCircleStart(MapAreaWrapper draggableCircle) {

        }

        @Override
        public void onResizeCircleStart(MapAreaWrapper draggableCircle) {

        }

        @Override
        public void onMinRadius(MapAreaWrapper draggableCircle) {

        }

        @Override
        public void onMaxRadius(MapAreaWrapper draggableCircle) {

        }
    }

    class LocalitySearchListener implements SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {

        @Override
        public boolean onQueryTextSubmit(String query) {
            if (query.length() > 2) {
                getBus().post(new PlaceInputEvent(query, COUNTRY_CODE));
            }
            return true;
        }

        @Override
        public boolean onQueryTextChange(String query) {
            if (query.length() > 2) {
                getBus().post(new PlaceInputEvent(query, COUNTRY_CODE));
            }
            return true;
        }

        @Override
        public boolean onSuggestionSelect(int position) {
            Cursor cursor = (Cursor) locationSearch.getSuggestionsAdapter().getItem(position);
            String placeId = cursor.getString(1);
            if (!placeId.isEmpty()) {
                getBus().post(new PlaceDetailEvent(cursor.getString(1)));
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean onSuggestionClick(int position) {
            Cursor cursor = (Cursor) locationSearch.getSuggestionsAdapter().getItem(position);
            String placeId = cursor.getString(1);
            if (!placeId.isEmpty()) {
                hideKeyboard();
                getBus().post(new PlaceDetailEvent(cursor.getString(1)));
                return true;
            } else {
                return false;
            }
        }

    }

}