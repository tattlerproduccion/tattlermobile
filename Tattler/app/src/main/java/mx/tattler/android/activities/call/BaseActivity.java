package mx.tattler.android.activities.call;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import mx.tattler.android.services.TattlerCallService;

/**
 * Created by oem on 21/03/2015.
 */
public class BaseActivity extends Activity implements ServiceConnection {
    private TattlerCallService.SinchServiceInterface mSinchServiceInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationContext().bindService(new Intent(this, TattlerCallService.class), this,
                BIND_AUTO_CREATE);
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (TattlerCallService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (TattlerCallService.SinchServiceInterface) iBinder;
            Log.e("CONECTADO ", "baseactivity");
            onServiceConnected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (TattlerCallService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    protected void onServiceConnected() {
        // for subclasses
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    protected TattlerCallService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }
}
