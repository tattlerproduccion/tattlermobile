package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.SerializedName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;

public class RestError {

    @SerializedName("Message")
    public String message;

    private transient RetrofitError innerError;

    public void setInnerError(RetrofitError innerError) {
        this.innerError = innerError;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getRawBody() {

        //Try to get response body
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {

            Response response = innerError.getResponse();

            if (response == null)
                return "";

            TypedInput input = response.getBody();
            if (input == null)
                return "";

            reader = new BufferedReader(new InputStreamReader(input.in()));

            String line;

            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                return "";
                //e.printStackTrace();
            }
        } catch (IOException | NullPointerException e) {
            return "";
            //e.printStackTrace();
        }

        return sb.toString();

    }

    public String getUrl() {
        return innerError.getUrl();
    }
}