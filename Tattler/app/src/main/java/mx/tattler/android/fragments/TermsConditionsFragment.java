package mx.tattler.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import mx.tattler.android.R;

public class TermsConditionsFragment extends BusFragment {

    public interface OnTermsListener {
        void onTermsInteraction(boolean accepted);
    }

    private OnTermsListener listener;

    public TermsConditionsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_terms_conditions, container, false);

        WebView termsAndConditions = (WebView)rootView.findViewById(R.id.wb_terms_and_conditions);
        termsAndConditions.loadUrl("http://tattler.mx/privacity/mobileprivacity.html");

        Button btn_accept = (Button) rootView.findViewById(R.id.btn_accept_terms);
        Button btn_disagree = (Button) rootView.findViewById(R.id.btn_disagree_terms);

        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTermsInteraction(true);
            }
        });

        btn_disagree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onTermsInteraction(false);
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnTermsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

}