package mx.tattler.android.rest.tattler.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Irving on 18/04/2015.
 */
public class QbTattlerToken {

    @Expose
    private String token;

    @SerializedName("application_id")
    @Expose
    private Integer applicationId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }
}
