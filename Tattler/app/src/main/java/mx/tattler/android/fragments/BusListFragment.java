package mx.tattler.android.fragments;

import android.app.Activity;
import android.app.ListFragment;

import mx.tattler.android.fragments.listeners.OnBusRequestListener;

public abstract class BusListFragment extends ListFragment {

    protected OnBusRequestListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            listener = (OnBusRequestListener) activity;
            listener.onRequest().register(this);
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnBusRequestListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener.onRequest().unregister(this);
        listener = null;

    }

}
