package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.daos.TattlerConversation;

public class ConversationUpdatedEvent {

    public final TattlerConversation conversation;

    public ConversationUpdatedEvent(TattlerConversation conversation) {
        this.conversation = conversation;
    }

}
