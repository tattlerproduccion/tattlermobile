package mx.tattler.android.rest.tattler.models.schedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Day {

    @SerializedName("opening_time")
    @Expose
    private String openingTime;
    @SerializedName("closing_time")
    @Expose
    private String closingTime;

    /**
     *
     * @return
     *     The openingTime
     */
    public String getOpeningTime() {
        return openingTime;
    }

    /**
     *
     * @param openingTime
     *     The opening_time
     */
    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    /**
     *
     * @return
     *     The closingTime
     */
    public String getClosingTime() {
        return closingTime;
    }

    /**
     *
     * @param closingTime
     *     The closing_time
     */
    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }

    public String getTimes(){
        return this.openingTime + " - " + this.closingTime;
    }

}
