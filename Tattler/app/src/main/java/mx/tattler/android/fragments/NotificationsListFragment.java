package mx.tattler.android.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.adapters.NotificationListAdapter;
import mx.tattler.android.events.tattler.AllNotificationsResultEvent;

public class NotificationsListFragment extends BusFragment {

    private NotificationListAdapter adapter;

    public NotificationsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_notifications_list, container, false);

        ListView friendList = (ListView) rootView.findViewById(R.id.lv_notifications);

        adapter = new NotificationListAdapter(getActivity());
        friendList.setAdapter(adapter);

        //listener.onRequest().post(new AllNotificationsEvent());

        return rootView;

    }

    @Subscribe
    public void onNotificationsLoaded(AllNotificationsResultEvent event) {
        adapter.setData(event.notifications);
    }


}
