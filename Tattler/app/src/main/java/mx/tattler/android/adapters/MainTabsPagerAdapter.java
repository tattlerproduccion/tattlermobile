package mx.tattler.android.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import mx.tattler.android.fragments.CompanySearchFragment;
import mx.tattler.android.fragments.ConversationsFragment;
import mx.tattler.android.fragments.SettingsFragment;

public class MainTabsPagerAdapter extends FragmentPagerAdapter {

    public MainTabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        Fragment fragment = null;

        switch (index) {
            case 0:
                fragment = new SettingsFragment();
                break;
            case 1:
                fragment = new ConversationsFragment();
                break;
            case 2:
                fragment = new CompanySearchFragment();
                break;
            default:
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

}
