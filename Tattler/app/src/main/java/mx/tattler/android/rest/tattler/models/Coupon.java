package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Coupon {

    @SerializedName("coupon_code")
    @Expose
    private String couponCode;

    /**
     * @return The couponCode
     */
    public String getCouponCode() {
        return couponCode;
    }

    /**
     * @param couponCode The coupon_code
     */
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

}