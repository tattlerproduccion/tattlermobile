package mx.tattler.android.fragments.search;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import mx.tattler.android.R;
import mx.tattler.android.adapters.ContactListAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class ContactListFragment extends Fragment {

    private ContactListAdapter adapter;

    public ContactListFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_contact, container, false);

        ListView contactList = (ListView) rootView.findViewById(R.id.lv_contact_result);
        adapter = new ContactListAdapter(getActivity());
        contactList.setAdapter(adapter);

        return rootView;
    }
}