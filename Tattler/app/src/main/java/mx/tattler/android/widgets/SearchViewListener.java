package mx.tattler.android.widgets;

import android.widget.Filter;
import android.widget.SearchView;

public class SearchViewListener implements SearchView.OnQueryTextListener {

    private Filter filter;

    public SearchViewListener(Filter filter) {
        this.filter = filter;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        filter.filter(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        filter.filter(s);
        return true;
    }

}
