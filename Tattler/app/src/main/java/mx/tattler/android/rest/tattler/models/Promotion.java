package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Promotion {

    @Expose
    private String id;
    @Expose
    private String image;
    @Expose
    private String title;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("expiration_date")
    @Expose
    private String expirationDate;
    @SerializedName("issued_coupons")
    @Expose
    private int issuedCoupons;
    @SerializedName("claimed_coupons")
    @Expose
    private int claimedCoupons;
    @SerializedName("visits_coupons")
    @Expose
    private int visitsCount;
    @Expose
    private List<Branch> branches;

    @SerializedName("company_id")
    @Expose
    private String companyId;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The start_date
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return The expirationDate
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate The expiration_date
     */
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getClaimedCoupons() {
        return claimedCoupons;
    }

    public void setClaimedCoupons(int claimedCoupons) {
        this.claimedCoupons = claimedCoupons;
    }

    public int getIssuedCoupons() {
        return issuedCoupons;
    }

    public void setIssuedCoupons(int issuedCoupons) {
        this.issuedCoupons = issuedCoupons;
    }

    public int getVisitsCount() {
        return visitsCount;
    }

    public void setVisitsCount(int visitsCount) {
        this.visitsCount = visitsCount;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public String describeBranches(){
        StringBuilder builder = new StringBuilder();

        if(branches != null){
            builder.append("Válido:\n");
            for (int i = 0; i < branches.size(); i++) {
                Branch branch = branches.get(i);
                builder.append(branch.getName());
                if(i < branches.size() - 1)
                    builder.append(", ");
            }
        }
        return builder.toString();
    }

}