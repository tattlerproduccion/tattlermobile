package mx.tattler.android.events.tattler;

/**
 * Created by irving on 11/11/14.
 */
public class GetCompanyEvent {

    public String companyId;

    public GetCompanyEvent(String companyId) {
        this.companyId = companyId;
    }
}
