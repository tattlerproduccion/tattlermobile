package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.Coupon;

public class RequestCouponResultEvent {

    public final Coupon coupon;

    public RequestCouponResultEvent(Coupon coupon) {
        this.coupon = coupon;
    }
}
