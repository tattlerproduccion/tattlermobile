package mx.tattler.android.events.tattler.chat;

public class ConversationReadyToExportEvent {

    public final String conversationId;

    public ConversationReadyToExportEvent(String conversationId) {
        this.conversationId = conversationId;
    }

}
