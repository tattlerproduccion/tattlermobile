package mx.tattler.android.rest.tattler;

import mx.tattler.android.rest.tattler.models.AccessResponse;

public interface AccessTokenRepository {
    boolean saveTokensThenConfigureInterceptor(AccessResponse accessResponse);
}