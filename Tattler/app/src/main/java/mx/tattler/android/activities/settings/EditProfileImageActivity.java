package mx.tattler.android.activities.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import mx.tattler.android.R;
import mx.tattler.android.activities.BusFragmentActivity;
import mx.tattler.android.activities.FavoriteCompaniesActivity;
import mx.tattler.android.events.tattler.CurrentUserEvent;
import mx.tattler.android.events.tattler.CurrentUserResultEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileImageEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileImageResultEvent;
import mx.tattler.android.fragments.AnimatedBusFragment;
import mx.tattler.android.utils.TypedFileFactory;
import mx.tattler.android.utils.image.RoundTransform;
import mx.tattler.android.utils.resource.ImageMediaHelper;
import retrofit.mime.TypedFile;

public class EditProfileImageActivity extends BusFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new EditProfileImageFragment())
                    .commit();
        }

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.edit_profile_image_activity), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof EditProfileImageFragment) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class EditProfileImageFragment extends AnimatedBusFragment implements View.OnClickListener {

        private static final String TAG = EditProfileImageFragment.class.getName();

        public static final int GALLERY_REQUEST_CODE = 0;
        public static final int CAMERA_REQUEST_CODE = 1;

        private View progressbarContainer;
        private View containerView;

        private Uri mNewImageUri;
        private ImageView profileImage;
        private TextView profileName;

        public EditProfileImageFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_edit_image_profile, container, false);

            progressbarContainer = rootView.findViewById(R.id.pb_loading);
            containerView = rootView.findViewById(R.id.ly_container);

            profileName = (TextView)rootView.findViewById(R.id.tv_profile_name);

            profileImage = (ImageView) rootView.findViewById(R.id.iv_profile_image);
            profileImage.setOnClickListener(this);

            ImageView userFavoritesBtn = (ImageView) rootView.findViewById(R.id.iv_user_favorites);
            userFavoritesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), FavoriteCompaniesActivity.class);
                    getActivity().startActivity(intent);
                }
            });

            //Request current user
            listener.onRequest().post(new CurrentUserEvent());
            showProgress(true);

            return rootView;
        }

        @Override
        public void onClick(View view) {

            DialogFragment pickPhotoSourceDialog = new DialogFragment() {
                @Override
                public Dialog onCreateDialog(Bundle savedInstanceState) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getActivity());
                    builder.setItems(mx.tattler.android.R.array.spanish_image_photo_sources,
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {

                                    switch (which) {
                                        case 0:

                                            // Gallery
                                            Intent photoPickerIntent = new Intent(
                                                    Intent.ACTION_GET_CONTENT);
                                            photoPickerIntent
                                                    .setType("image/*");
                                            startActivityForResult(
                                                    photoPickerIntent,
                                                    GALLERY_REQUEST_CODE);
                                            break;

                                        default:

                                            // Camera
                                            mNewImageUri = getActivity()
                                                    .getContentResolver()
                                                    .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                                            new ContentValues());

                                            Intent photoFromCamera = new Intent(
                                                    MediaStore.ACTION_IMAGE_CAPTURE);
                                            photoFromCamera.putExtra(
                                                    MediaStore.EXTRA_OUTPUT,
                                                    mNewImageUri);

                                            photoFromCamera
                                                    .putExtra(
                                                            MediaStore.EXTRA_VIDEO_QUALITY,
                                                            0);
                                            startActivityForResult(
                                                    photoFromCamera,
                                                    CAMERA_REQUEST_CODE);

                                            break;
                                    }

                                }
                            });
                    return builder.create();
                }
            };

            pickPhotoSourceDialog.show(getFragmentManager(),
                    "pickPhotoSourceDialog");

        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {

                if (data != null) {
                    mNewImageUri = data.getData();
                }

                try{
                    Uri imageUri = ImageMediaHelper.resizedBitmapUriIfNeeded(getActivity(), mNewImageUri);
                    ImageMediaHelper.loadImageIntoView(getActivity(), imageUri, profileImage);
                    TypedFile file = TypedFileFactory.create(getActivity(), imageUri);
                    listener.onRequest().post(new UpdateProfileImageEvent(file));
                    showProgress(true);
                }catch (Exception e){
                    Toast.makeText(getActivity(), "Imagen no pudo ser cargada", Toast.LENGTH_LONG).show();
                }

            }

        }

        @Subscribe
        public void onCurrentUser(CurrentUserResultEvent event) {

            Picasso picasso = Picasso.with(getActivity());

            profileName.setText(event.user.getCompleteName());

            String imageUrl = event.user.getProfileImage();

            Log.i(TAG, "Imagen de usuario: " + imageUrl);

            if(imageUrl == null){

                picasso.load(R.drawable.ic_profile_placeholder)
                        .error(R.drawable.ic_profile_placeholder)
                        .transform(new RoundTransform())
                        .into(profileImage);

            }else{

                picasso.load(imageUrl)
                        .error(R.drawable.ic_profile_placeholder)
                        .transform(new RoundTransform())
                        .into(profileImage);

            }



            showProgress(false);

        }

        @Subscribe
        public void onImageUpdated(UpdateProfileImageResultEvent event) {
            if (!TextUtils.isEmpty(event.uploadFileResponse.getUrl())) {
                Toast.makeText(getActivity(), "Imagen de perfil actualizada", Toast.LENGTH_SHORT).show();
            }
            showProgress(false);
        }

        @Override
        protected View getContainerView() {
            return containerView;
        }

        @Override
        protected View getProgressView() {
            return progressbarContainer;
        }
    }

}
