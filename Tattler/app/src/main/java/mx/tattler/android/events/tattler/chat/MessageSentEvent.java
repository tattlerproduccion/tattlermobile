package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.daos.TattlerMessage;

public class MessageSentEvent {

    public final TattlerMessage message;


    public MessageSentEvent(TattlerMessage message) {
        this.message = message;
    }

}
