package mx.tattler.android.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

import org.parceler.Parcels;

import mx.tattler.android.R;
import mx.tattler.android.rest.tattler.models.Branch;
import mx.tattler.android.rest.tattler.models.schedule.Schedule;

public class BranchDialogFragment extends DialogFragment {

    public static BranchDialogFragment newInstance(Branch branch) {
        BranchDialogFragment fragment = new BranchDialogFragment();
        Bundle args = new Bundle();
        Parcelable wrapped = Parcels.wrap(branch);
        args.putParcelable("branch", wrapped);
        fragment.setArguments(args);
        return fragment;
    }

    public BranchDialogFragment() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Branch branch = Parcels.unwrap(getArguments().getParcelable("branch"));

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_fragment_branch, null);

        TextView branchName = (TextView) view.findViewById(R.id.tv_branch_name);
        TextView branchAddress = (TextView) view.findViewById(R.id.tv_branch_address);

        TextView monday = (TextView) view.findViewById(R.id.tv_branch_schedule_monday);
        TextView tuesday = (TextView) view.findViewById(R.id.tv_branch_schedule_tuesday);
        TextView wednesday = (TextView) view.findViewById(R.id.tv_branch_schedule_wednesday);
        TextView thursday = (TextView) view.findViewById(R.id.tv_branch_schedule_thursday);
        TextView friday = (TextView) view.findViewById(R.id.tv_branch_schedule_friday);
        TextView saturday = (TextView) view.findViewById(R.id.tv_branch_schedule_saturday);
        TextView sunday = (TextView) view.findViewById(R.id.tv_branch_schedule_sunday);

        assert branch != null;

        branchName.setText(branch.getName());
        branchAddress.setText(branch.getAddress().describe());

        Schedule branchSchedule = branch.getSchedule();

        if(branchSchedule != null){
            monday.setText(branchSchedule.getMonday().getTimes());
            tuesday.setText(branchSchedule.getTuesday().getTimes());
            wednesday.setText(branchSchedule.getWednesday().getTimes());
            thursday.setText(branchSchedule.getThursday().getTimes());
            friday.setText(branchSchedule.getFriday().getTimes());
            saturday.setText(branchSchedule.getSaturday().getTimes());
            sunday.setText(branchSchedule.getSunday().getTimes());
        }

        alertDialogBuilder.setView(view);

        return alertDialogBuilder.create();

    }

}
