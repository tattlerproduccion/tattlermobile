package mx.tattler.android.daos;

import com.quickblox.chat.model.QBChatMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.utils.text.DateTextHelper;

public class TattlerMessageFactory {

    public static List<TattlerMessage> batchConvert(List<Message> messages) {

        List<TattlerMessage> tattlerMessages = new ArrayList<>();

        for (Message message : messages) {
            tattlerMessages.add(convert(message));
        }

        return tattlerMessages;

    }

    public static TattlerMessage convert(Message message) {

        TattlerMessage tattlerMessage = new TattlerMessage();
        tattlerMessage.setMessageId(message.getId());
        tattlerMessage.setConversationId(message.getConversationId());
        tattlerMessage.setBody(message.getBody());
        tattlerMessage.setSender(message.getSender());
        tattlerMessage.setStatus(message.getStatus());
        tattlerMessage.setType(message.getType());
        tattlerMessage.setSentAt(message.getSentAtDate());
        tattlerMessage.setMine(message.isMine());

        return tattlerMessage;

    }

    public static TattlerMessage convert(QBChatMessage qbMessage, String senderName, String conversationId) {

        TattlerMessage tattlerMessage = new TattlerMessage();

        tattlerMessage.setMessageId(qbMessage.getProperty(Message.TATTLER_MESSAGE_ID));
        tattlerMessage.setConversationId(conversationId);

        tattlerMessage.setBody(qbMessage.getBody());
        tattlerMessage.setSender(senderName);
        tattlerMessage.setStatus("Unread");

        tattlerMessage.setType(qbMessage.getProperty(Message.MESSAGE_TYPE));
        String rawDate = qbMessage.getProperty(Message.DATE_SENT);
        Date sentDate = DateTextHelper.parseMessageDate(rawDate);
        tattlerMessage.setSentAt(sentDate);

        return tattlerMessage;

    }

}
