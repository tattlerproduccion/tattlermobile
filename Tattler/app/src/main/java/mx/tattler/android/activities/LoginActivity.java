package mx.tattler.android.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.chat.UserCredentials;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.LoginEvent;
import mx.tattler.android.events.tattler.login.EmailAsUsernameExceptionEvent;
import mx.tattler.android.rest.tattler.models.RestError;
import mx.tattler.android.widgets.EditTextBackEvent;

public class LoginActivity extends BusActivity {

    private static final String TAG = LoginActivity.class.getName();

    private ProgressBar progressView;
    private View containerView;
    private ImageView logo;

    private int logoHeight;

    private int windowHeight;

    private EditTextBackEvent et_username;
    private EditTextBackEvent et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        windowHeight = size.y;

        Log.i(TAG, String.format("Window height: %s", windowHeight));

        progressView = (ProgressBar) findViewById(R.id.pb_loading);
        containerView = findViewById(R.id.container);
        containerView.requestFocus();

        et_username = (EditTextBackEvent) findViewById(R.id.et_username);
        et_password = (EditTextBackEvent) findViewById(R.id.et_password);

        logo = (ImageView)findViewById(R.id.iv_tattler_logo);

        logoHeight = logo.getLayoutParams().height;

        et_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                signIn(null);
                return true;
            }
        });

        LoginFocusListener focusListener = new LoginFocusListener();
        et_username.setOnFocusChangeListener(focusListener);
        et_password.setOnFocusChangeListener(focusListener);

        BackListener backListener = new BackListener();
        et_username.setOnEditTextImeBackListener(backListener);
        et_password.setOnEditTextImeBackListener(backListener);

        et_username.setOnClickListener(focusListener);
        et_password.setOnClickListener(focusListener);

        TextView registerPrompt = (TextView) findViewById(R.id.tv_register_prompt);
        registerPrompt.setPaintFlags(registerPrompt.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        registerPrompt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        TextView recoveryPrompt = (TextView) findViewById(R.id.tv_remember_password_prompt);
        recoveryPrompt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, PasswordRecoveryActivity.class));
            }
        });

    }

    public void signIn(View view) {

        String username = et_username.getText().toString();
        String password = et_password.getText().toString();

        if (username.length() == 0 || password.length() == 0) {
            Toast.makeText(this, "Llena tus datos primero", Toast.LENGTH_SHORT).show();
            return;
        }

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        showProgress(true);

        UserCredentials credentials = UserCredentials.instance(username, password, "");
        getBus().post(new LoginEvent(credentials));

    }


    @Subscribe
    public void onDeprecatedAccount(EmailAsUsernameExceptionEvent event){
        Toast.makeText(this, "Tu cuenta tiene email como nombre de usuario en QB, no se puede loggear!!! Pidele al admin que cambie tu cuenta o reinicie la BD", Toast.LENGTH_LONG).show();
        showProgress(false);
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {

        RestError error = event.error;
        Log.e(TAG, error.getRawBody());

        String message = "Usuario/contraseña incorrectos";

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        showProgress(false);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
            containerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    containerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    class LoginFocusListener implements View.OnFocusChangeListener, View.OnClickListener{

        @Override
        public void onFocusChange(View view, boolean focusGained) {

            if(windowHeight > 900){
                return;
            }

            if(focusGained){

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight / 2);
                logo.setLayoutParams(layoutParams);

            }else{
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight);
                logo.setLayoutParams(layoutParams);
            }
        }

        @Override
        public void onClick(View view) {
            if(windowHeight > 900){
                return;
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight / 2);
            logo.setLayoutParams(layoutParams);

        }
    }

    class BackListener implements EditTextBackEvent.EditTextImeBackListener{
        @Override
        public void onImeBack(EditTextBackEvent ctrl, String text) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(logo.getLayoutParams().width, logoHeight);
            logo.setLayoutParams(layoutParams);
        }
    }

}