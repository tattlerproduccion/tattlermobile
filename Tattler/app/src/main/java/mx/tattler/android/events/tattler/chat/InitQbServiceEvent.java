package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.chat.UserCredentials;

public class InitQbServiceEvent {

    public final UserCredentials credentials;

    public InitQbServiceEvent(UserCredentials credentials) {
        this.credentials = credentials;
    }

}
