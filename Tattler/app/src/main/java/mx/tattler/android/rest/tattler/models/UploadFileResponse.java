package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;

public class UploadFileResponse {

    @Expose
    private String url;

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}