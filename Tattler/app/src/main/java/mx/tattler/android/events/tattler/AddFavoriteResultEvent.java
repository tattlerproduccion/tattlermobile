package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.Company;

public class AddFavoriteResultEvent {

    public final Company company;

    public AddFavoriteResultEvent(Company company) {
        this.company = company;
    }
}
