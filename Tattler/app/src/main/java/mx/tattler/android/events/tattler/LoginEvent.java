package mx.tattler.android.events.tattler;

import mx.tattler.android.chat.UserCredentials;

public class LoginEvent {

    public UserCredentials credentials;

    public LoginEvent(UserCredentials credentials) {
        this.credentials = credentials;
    }

}
