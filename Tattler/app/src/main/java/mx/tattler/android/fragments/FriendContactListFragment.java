package mx.tattler.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.activities.ProfileActivity;
import mx.tattler.android.adapters.FriendContactListAdapter;
import mx.tattler.android.constants.ContactConstants;
import mx.tattler.android.events.tattler.GetContactContactsEvent;
import mx.tattler.android.events.tattler.GetContactContactsResultEvent;
import mx.tattler.android.rest.tattler.models.Contact;

public class FriendContactListFragment extends BusFragment {

    private static final String TAG = FriendContactListFragment.class.getName();

    private String contactId = "123123d";
    private FriendContactListAdapter adapter;

    public FriendContactListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_contact_list, container, false);

        ListView friendList = (ListView) rootView.findViewById(R.id.lv_friends);
        EditText inputSearch = (EditText) rootView.findViewById(R.id.et_contact_filter);

        friendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Contact contact =  adapter.getItem(i);
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra(ContactConstants.CONTACT_ID, contact.getId());
                startActivity(intent);

            }
        });

        adapter = new FriendContactListAdapter(getActivity());
        friendList.setAdapter(adapter);

        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                adapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }

        });

        listener.onRequest().post(new GetContactContactsEvent(contactId));

        return rootView;
    }

    @Subscribe
    public void onFriendsLoaded(GetContactContactsResultEvent event) {
        adapter.setData(event.contacts);
    }

}
