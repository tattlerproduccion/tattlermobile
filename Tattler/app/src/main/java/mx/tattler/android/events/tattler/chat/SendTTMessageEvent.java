package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.utils.resource.TypedFileFromBytes;

public class SendTTMessageEvent {

    public final TypedFileFromBytes attachment;
    public final Message message;

    public SendTTMessageEvent(TypedFileFromBytes attachment, Message message) {
        this.attachment = attachment;
        this.message = message;
    }

    public SendTTMessageEvent(Message message) {
        this.attachment = null;
        this.message = message;
    }

}
