package mx.tattler.android.daos;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.rest.tattler.models.Company;

public class FavoritesRepository {

    private FavoritesRepository(){}

    public static boolean insertBatch(Context context, List<Company> favorites){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = gson.toJson(favorites);
        preferences.edit().putString(context.getString(R.string.favorites_list), json).commit();
        return true;
    }

    public static boolean isFavoriteFromList(List<Company> companies, String companyId){
        for(Company company : companies)
            if(company.getId().equals(companyId)) return true;
        return false;
    }

    public static boolean isCompanyFavorite(Context context, String companyId){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String json = preferences.getString(context.getString(R.string.favorites_list), null);

        if(json == null)
            return false;

        List<Company> companies = inflate(json);

        for(Company company : companies){
            if(company.getId().equals(companyId)) return true;
        }

        return false;
    }

    public static boolean deleteFromFavorites(Context context, String companyId){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String json = preferences.getString(context.getString(R.string.favorites_list), null);

        if(json == null)
            return true;

        List<Company> companies = inflate(json);

        for(Company company : companies){
            if(company.getId().equals(companyId)){
                companies.remove(company);
                return true;
            }
        }

        preferences.edit().putString(context.getString(R.string.favorites_list), json).commit();

        return false;

    }


    public static boolean insert(Context context, Company company) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String json = preferences.getString(context.getString(R.string.favorites_list), null);

        if(json == null)
            return false;

        List<Company> companies = inflate(json);
        companies.add(company);

        Gson gson = new Gson();
        json = gson.toJson(companies);
        preferences.edit().putString(context.getString(R.string.favorites_list), json).commit();

        return true;

    }

    private static List<Company> inflate(String json){
        return new Gson().fromJson(json, new TypeToken<List<Company>>(){}.getType());
    }

}
