package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.daos.TattlerConversation;

public class EndChatEvent {

    public final TattlerConversation conversation;

    public EndChatEvent(TattlerConversation conversation) {
        this.conversation = conversation;
    }

}
