package mx.tattler.android.activities.settings;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.activities.BusFragmentActivity;
import mx.tattler.android.events.tattler.CurrentUserEvent;
import mx.tattler.android.events.tattler.CurrentUserResultEvent;
import mx.tattler.android.events.tattler.profile.ChangeEmailEvent;
import mx.tattler.android.events.tattler.profile.ProfileBirthDateUpdatedEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileResultEvent;
import mx.tattler.android.fragments.dialogs.BirthDateDialogFragment;
import mx.tattler.android.fragments.dialogs.PasswordDialogFragment;
import mx.tattler.android.rest.tattler.models.User;
import mx.tattler.android.rest.tattler.models.profile.BirthDate;

public class EditProfileActivity extends BusFragmentActivity {

    private EditText lastName;
    private EditText name;
    //private String rawStringDate;

    private Spinner genderSpinner;

    private TextView emailLabel;

    private TextView birthdateLabel;
    private Button editPassword;
    private BirthDate birthDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        final FragmentManager manager = getSupportFragmentManager();

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.edit_profile_image_activity), null);
            hideTattlerActionbarLogo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        name = (EditText) findViewById(R.id.et_name);
        lastName = (EditText) findViewById(R.id.et_last_name);
        genderSpinner = (Spinner) findViewById(R.id.sp_genders);

        birthdateLabel = (TextView) findViewById(R.id.tv_birthdate);
        Button editBirthdate = (Button) findViewById(R.id.btn_edit_birthdate);
        editBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = BirthDateDialogFragment.newInstance(birthDate);
                dialog.show(manager, "birthdate");
            }
        });

        emailLabel = (TextView) findViewById(R.id.tv_email);

        editPassword = (Button) findViewById(R.id.btn_edit_password);

        getBus().post(new CurrentUserEvent());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_profile) {
            updateProfile();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateProfile() {

        User user = new User();
        user.setName(name.getText().toString());
        user.setLastName(lastName.getText().toString());

        //Todo: Update profile
        user.setBirthDate(birthDate.getFormattedDate());

        int genderPosition = genderSpinner.getSelectedItemPosition();
        String gender = genderPosition == 0 ? "Male" : "Female";
        user.setGender(gender);

        getBus().post(new UpdateProfileEvent(user));

    }

    @Subscribe
    public void onCurrentUser(CurrentUserResultEvent event) {

        final User currentUser = event.user;

        name.setText(currentUser.getName());
        lastName.setText(currentUser.getLastName());

        String rawDate = currentUser.getBirthDate();

        this.birthDate = new BirthDate(rawDate);

        String visibleDate = birthDate.getVisibleDate();

        birthdateLabel.setText(visibleDate);

        String gender = currentUser.getGender();

        if (gender != null) {
            int genderPosition = gender.equals("Male") ? 0 : 1;
            genderSpinner.setSelection(genderPosition);
        }

        emailLabel.setText(currentUser.getUsername());

        final FragmentManager manager = getSupportFragmentManager();
        editPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = PasswordDialogFragment.newInstance(currentUser.getUsername());
                dialog.show(manager, "password");
            }
        });

    }

    @Subscribe
    public void onProfileUpdated(UpdateProfileResultEvent event) {
        finish();
    }

    @Subscribe
    public void onBirthdateUpdated(ProfileBirthDateUpdatedEvent event) {

        this.birthDate = event.birthDate;
        birthdateLabel.setText(birthDate.getVisibleDate());

    }

    @Subscribe
    public void onChangeEmail(ChangeEmailEvent event) {
        Toast.makeText(this, "Próximamente podrás cambiar tu email!", Toast.LENGTH_LONG).show();
    }

}
