package mx.tattler.android.rest.tattler.models.profile;

import org.parceler.Parcel;

import java.util.Calendar;
import java.util.Date;

import mx.tattler.android.utils.text.DateTextHelper;

/**
 * Created by Irving on 25/03/2015.
 */
@Parcel
public class BirthDate {

    private Calendar calendarDate;

    public BirthDate() {
        this.calendarDate = Calendar.getInstance();
    }

    public BirthDate(String rawDate) {
        this.calendarDate = Calendar.getInstance();
        if (rawDate != null) {
            Date dunno = DateTextHelper.toProfileDate(rawDate);
            calendarDate.setTime(dunno);
        }
    }

    public String getVisibleDate() {
        return DateTextHelper.formatVisibleProfileDate(calendarDate.getTime());
    }

    public String getFormattedDate() {
        return DateTextHelper.formatToProfile(calendarDate.getTime());
    }

    public Calendar getCalendarDate() {
        return calendarDate;
    }

    public void setCalendarDate(Calendar calendarDate) {
        this.calendarDate = calendarDate;
    }
}
