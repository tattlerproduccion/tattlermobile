package mx.tattler.android.events.maps;

import com.google.android.gms.maps.model.LatLng;

public class GeocodeLatLngEvent {

    public final LatLng latLng;

    public GeocodeLatLngEvent(LatLng latLng) {
        this.latLng = latLng;
    }
}