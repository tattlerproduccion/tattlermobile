package mx.tattler.android.events.tattler;

import java.util.List;

import mx.tattler.android.rest.tattler.models.Contact;

/**
 * Created by irving on 11/11/14.
 */
public class AllContactsResultEvent {

    public List<Contact> friends;

    public AllContactsResultEvent(List<Contact> friends) {
        this.friends = friends;
    }
}
