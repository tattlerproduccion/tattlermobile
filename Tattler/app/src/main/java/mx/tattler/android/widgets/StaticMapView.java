package mx.tattler.android.widgets;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.overlay.UserLocationOverlay;
import com.mapbox.mapboxsdk.tileprovider.MapTileLayerBase;
import com.mapbox.mapboxsdk.views.MapView;

public class StaticMapView extends MapView {

    protected StaticMapView(Context aContext, int tileSizePixels, MapTileLayerBase tileProvider, Handler tileRequestCompleteHandler, AttributeSet attrs) {
        super(aContext, tileSizePixels, tileProvider, tileRequestCompleteHandler, attrs);
        this.setEnabled(false);
        setCurrentLocation();
    }

    public StaticMapView(Context aContext) {
        super(aContext);
        this.setEnabled(false);
        setCurrentLocation();
    }

    public StaticMapView(Context aContext, AttributeSet attrs) {
        super(aContext, attrs);
        this.setEnabled(false);
        setCurrentLocation();
    }

    protected StaticMapView(Context aContext, int tileSizePixels, MapTileLayerBase aTileProvider) {
        super(aContext, tileSizePixels, aTileProvider);
        this.setEnabled(false);
        setCurrentLocation();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return true;
        //return false;
        //return super.dispatchTouchEvent(ev);
    }

    public void configure(double latitude, double longitude, float zoom){

        LatLng point = new LatLng(latitude, longitude);

        this.setCenter(point);
        this.setZoom(zoom);

    }

    private void setCurrentLocation(){

        // Setup UserLocation monitoring
        this.setUserLocationEnabled(true);
        this.setUserLocationTrackingMode(UserLocationOverlay.TrackingMode.NONE);

        //Retrieve user location
        this.getUserLocation();

    }

}
