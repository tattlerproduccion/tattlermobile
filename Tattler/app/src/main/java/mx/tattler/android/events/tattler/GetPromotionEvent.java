package mx.tattler.android.events.tattler;

/**
 * Created by irving on 11/11/14.
 */
public class GetPromotionEvent {

    public final String companyId;
    public final String promotionId;

    public GetPromotionEvent(String companyId, String promotionId) {
        this.companyId = companyId;
        this.promotionId = promotionId;
    }

}
