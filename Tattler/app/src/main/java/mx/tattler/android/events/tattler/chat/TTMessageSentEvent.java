package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.rest.tattler.models.chat.Message;

public class TTMessageSentEvent {

    public final Message message;

    public TTMessageSentEvent(Message message) {
        this.message = message;
    }

}
