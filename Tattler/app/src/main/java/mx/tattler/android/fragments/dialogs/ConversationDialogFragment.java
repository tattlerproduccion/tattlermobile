package mx.tattler.android.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.parceler.Parcels;

import mx.tattler.android.R;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.events.tattler.chat.ExportConversationEvent;

public class ConversationDialogFragment extends AnimatedBusDialogFragment {

    private static final String TAG = ConversationDialogFragment.class.getName();

    private TattlerConversation conversation;

    private LinearLayout container;
    private ProgressBar progressBar;
    private AlertDialog self;

    public static ConversationDialogFragment newInstance(TattlerConversation conversation) {
        ConversationDialogFragment fragment = new ConversationDialogFragment();
        Bundle args = new Bundle();
        Parcelable wrapped = Parcels.wrap(conversation);
        args.putParcelable("promotion", wrapped);
        fragment.setArguments(args);
        return fragment;
    }

    public ConversationDialogFragment() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //String title = getArguments().getString("title");

        //List<Branch> branches = Parcels.unwrap(getIntent().getExtras().getParcelable("branches"));
        conversation = Parcels.unwrap(getArguments().getParcelable("promotion"));

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        alertDialogBuilder.setItems(R.array.conversation_options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: //View profile
                        Toast.makeText(getActivity(), "Proximamente", Toast.LENGTH_SHORT).show();
                        break;

                    case 1: //Call user
                        Toast.makeText(getActivity(), "Proximamente", Toast.LENGTH_SHORT).show();
                        break;

                    case 2: //Delete conversation
                        Toast.makeText(getActivity(), "Proximamente", Toast.LENGTH_SHORT).show();
                        break;

                    case 3: //Empty conversation
                        Toast.makeText(getActivity(), "Proximamente", Toast.LENGTH_SHORT).show();
                        break;

                    case 4: //Email conversation
                        fetchMessagesThenSend();
                        break;

                    default:
                        break;
                }
            }
        });

        return alertDialogBuilder.create();
    }

    private void fetchMessagesThenSend(){
        listener.onRequest().post(new ExportConversationEvent(conversation));
        self.dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();

        self = (AlertDialog) getDialog();

        if (self == null)
            return;

        self.setCanceledOnTouchOutside(false);

    }

    @Override
    protected View getContainerView() {
        return container;
    }

    @Override
    protected View getProgressView() {
        return progressBar;
    }

}
