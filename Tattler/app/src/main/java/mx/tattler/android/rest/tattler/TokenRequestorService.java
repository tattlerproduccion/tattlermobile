package mx.tattler.android.rest.tattler;

import android.util.Log;

import mx.tattler.android.rest.tattler.models.AccessResponse;
import retrofit.RetrofitError;

public class TokenRequestorService implements AccessTokenRequestor {

    private static final String TAG = TokenRequestorService.class.getName();

    private TattlerTokenApi api;

    public static TokenRequestorService instance(TattlerTokenApi api) {
        TokenRequestorService service = new TokenRequestorService();
        service.api = api;
        return service;
    }

    @Override
    public AccessResponse refreshToken(String token) {

        String clientId = "tattlerMobileApp";
        String clientSecret = "tattler@mobile";
        String grantType = "refresh_token";

        AccessResponse accessResponse = null;

        try {
            accessResponse = api.refreshToken(token, clientId, clientSecret, grantType);
        } catch (RetrofitError error) {

            String message = error.getMessage();

            Log.e(TAG, "What happened? + " + error);
        }

        return accessResponse;

    }

}