package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.RegistrationDetails;

public class RegisterEvent {

    public RegistrationDetails registrationDetails;

    public RegisterEvent(RegistrationDetails registrationDetails) {
        this.registrationDetails = registrationDetails;
    }
}
