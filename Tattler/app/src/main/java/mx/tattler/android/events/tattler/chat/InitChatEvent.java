package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.daos.TattlerConversation;

public class InitChatEvent {

    public final TattlerConversation conversation;

    public InitChatEvent(TattlerConversation conversation) {
        this.conversation = conversation;
    }

}
