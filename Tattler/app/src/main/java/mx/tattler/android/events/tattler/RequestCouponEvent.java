package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.Promotion;

public class RequestCouponEvent {

    public final Promotion promotion;

    public RequestCouponEvent(Promotion promotion) {
        this.promotion = promotion;
    }
}
