package mx.tattler.android.events.tattler;

import java.util.List;

import mx.tattler.android.rest.tattler.models.Notification;

public class AllNotificationsResultEvent {

    public List<Notification> notifications;

    public AllNotificationsResultEvent(List<Notification> notifications) {
        this.notifications = notifications;
    }
}
