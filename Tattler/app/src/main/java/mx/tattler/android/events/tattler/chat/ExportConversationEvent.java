package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.daos.TattlerConversation;

public class ExportConversationEvent {

    public final TattlerConversation conversation;

    public ExportConversationEvent(TattlerConversation conversation) {
        this.conversation = conversation;
    }
}
