package mx.tattler.android.fragments.password;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import mx.tattler.android.R;
import mx.tattler.android.activities.LoginActivity;
import mx.tattler.android.constants.UserConstants;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.recover.CancelRecoverEvent;
import mx.tattler.android.events.tattler.recover.SendRecoverEmailEvent;
import mx.tattler.android.fragments.AnimatedBusFragment;
import mx.tattler.android.fragments.BusFragment;
import mx.tattler.android.rest.tattler.models.User;
import mx.tattler.android.utils.image.RoundTransform;

public class CredentialsConfirmationFragment extends AnimatedBusFragment {

    private View progressbarContainer;
    private View containerView;

    private User user;

    public static CredentialsConfirmationFragment newInstance(User user) {
        CredentialsConfirmationFragment fragment = new CredentialsConfirmationFragment();
        Bundle args = new Bundle();
        Parcelable wrapped = Parcels.wrap(user);
        args.putParcelable(UserConstants.USER, wrapped);
        fragment.setArguments(args);
        return fragment;
    }

    public CredentialsConfirmationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = Parcels.unwrap(getArguments().getParcelable(UserConstants.USER));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_credentials_confirmation, container, false);

        progressbarContainer = rootView.findViewById(R.id.pb_loading);
        containerView = rootView.findViewById(R.id.ly_container);

        ImageView profileImage = (ImageView) rootView.findViewById(R.id.iv_profile_image);
        TextView profileName = (TextView) rootView.findViewById(R.id.tv_profile_name);
        Button requestRecoverTokenBtn = (Button) rootView.findViewById(R.id.btn_request_code);
        Button cancelRecoverBtn = (Button) rootView.findViewById(R.id.btn_cancel_recover);

        Picasso picasso = Picasso.with(getActivity());

        String imageUrl = user.getProfileImage();

        picasso.load(imageUrl)
                .placeholder(R.drawable.ic_default_avatar)
                .error(R.drawable.ic_default_avatar)
                .transform(new RoundTransform())
                .into(profileImage);

        profileName.setText(user.getCompleteName());

        requestRecoverTokenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgress(true);
                listener.onRequest().post(new SendRecoverEmailEvent(user));
            }
        });

        cancelRecoverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onRequest().post(new CancelRecoverEvent());
            }
        });

        return rootView;
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event){
        showProgress(false);
        Toast.makeText(getActivity(), "No se pudo continuar con el proceso, inténtalo nuevamente", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected View getContainerView() {
        return containerView;
    }

    @Override
    protected View getProgressView() {
        return progressbarContainer;
    }

}
