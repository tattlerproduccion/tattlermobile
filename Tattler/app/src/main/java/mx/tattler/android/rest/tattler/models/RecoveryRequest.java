package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecoveryRequest {

    @SerializedName("new_password")
    @Expose
    private String newPassword;
    @SerializedName("password_confirmation")
    @Expose
    private String passwordConfirmation;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

}