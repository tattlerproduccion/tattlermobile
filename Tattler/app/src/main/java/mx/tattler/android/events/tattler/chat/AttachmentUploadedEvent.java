package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.rest.tattler.models.chat.Message;

public class AttachmentUploadedEvent {

    public final Message message;

    public AttachmentUploadedEvent(Message message) {
        this.message = message;
    }
}
