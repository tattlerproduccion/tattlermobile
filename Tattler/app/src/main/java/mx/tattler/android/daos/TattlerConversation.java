package mx.tattler.android.daos;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
import org.parceler.Parcel;
@Parcel
// KEEP INCLUDES END
/**
 * Entity mapped to table TATTLER_CONVERSATION.
 */
public class TattlerConversation {

    private Long id;
    private String conversationId;
    private Integer receiverId;
    private String receiverName;
    private String receiverPicture;
    private String receiverCompanyName;
    private String lastMessage;
    private String lastMessageType;
    private java.util.Date lastMessageDate;

    // KEEP FIELDS - put your custom fields here
    private String phone;
    // KEEP FIELDS END

    public TattlerConversation() {
    }

    public TattlerConversation(Long id) {
        this.id = id;
    }

    public TattlerConversation(Long id, String conversationId, Integer receiverId, String receiverName, String receiverPicture, String receiverCompanyName, String lastMessage, String lastMessageType, java.util.Date lastMessageDate) {
        this.id = id;
        this.conversationId = conversationId;
        this.receiverId = receiverId;
        this.receiverName = receiverName;
        this.receiverPicture = receiverPicture;
        this.receiverCompanyName = receiverCompanyName;
        this.lastMessage = lastMessage;
        this.lastMessageType = lastMessageType;
        this.lastMessageDate = lastMessageDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public Integer getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverPicture() {
        return receiverPicture;
    }

    public void setReceiverPicture(String receiverPicture) {
        this.receiverPicture = receiverPicture;
    }

    public String getReceiverCompanyName() {
        return receiverCompanyName;
    }

    public void setReceiverCompanyName(String receiverCompanyName) {
        this.receiverCompanyName = receiverCompanyName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageType() {
        return lastMessageType;
    }

    public void setLastMessageType(String lastMessageType) {
        this.lastMessageType = lastMessageType;
    }

    public java.util.Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(java.util.Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    // KEEP METHODS - put your custom methods here

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    // KEEP METHODS END

}
