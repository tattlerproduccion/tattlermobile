package mx.tattler.android.events.tattler.call;

import mx.tattler.android.chat.UserCredentials;

/**
 * Created by oem on 20/03/2015.
 */
public class InitSinchServiceEvent {
    public final UserCredentials credentials;

    public InitSinchServiceEvent(UserCredentials credentials) {
        this.credentials = credentials;
    }
}
