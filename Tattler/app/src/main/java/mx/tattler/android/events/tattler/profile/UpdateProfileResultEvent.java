package mx.tattler.android.events.tattler.profile;

import mx.tattler.android.rest.tattler.models.User;

public class UpdateProfileResultEvent {

    public User user;

    public UpdateProfileResultEvent(User user) {
        this.user = user;
    }
}
