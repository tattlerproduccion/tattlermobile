package mx.tattler.android.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.adapters.FilterableCompanyAdapter;
import mx.tattler.android.adapters.chat.ConversationsAdapter;
import mx.tattler.android.events.tattler.GetFavoritesEvent;
import mx.tattler.android.events.tattler.GetFavoritesResultEvent;
import mx.tattler.android.rest.tattler.models.Company;
import mx.tattler.android.widgets.SearchViewListener;
import mx.tattler.android.widgets.SearchViewStyler;

public class FavoriteCompaniesActivity extends AnimatedBusFragmentActivity implements ConversationsAdapter.OnFilterListener, AdapterView.OnItemClickListener{

    private View progressbarContainer;
    private View containerView;
    private FilterableCompanyAdapter adapter;
    private TextView emptyFavoritesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_companies);

        progressbarContainer = findViewById(R.id.pb_loading);
        containerView = findViewById(R.id.ly_container);

        adapter = new FilterableCompanyAdapter(this, this);

        SearchView favoriteSearchView = (SearchView) findViewById(R.id.sv_favorites);
        ListView listView = (ListView) findViewById(R.id.lv_favorites);
        emptyFavoritesView = (TextView) findViewById(R.id.tv_empty_favorites);

        listView.setEmptyView(emptyFavoritesView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(adapter);

        favoriteSearchView.setIconifiedByDefault(false);
        favoriteSearchView.setSubmitButtonEnabled(true);
        favoriteSearchView.setOnQueryTextListener(new SearchViewListener(adapter.getFilter()));

        SearchViewStyler.apply(this, favoriteSearchView);

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.title_activity_favorites), null);
            setCustomLogo(R.drawable.ic_btn_friend_favorites);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Request favorites
        getBus().post(new GetFavoritesEvent());

    }

    @Subscribe
    public void onFavorites(GetFavoritesResultEvent event){

        List<Company> favorites = event.companies;
        adapter.setCompanies(favorites, false);

    }

    @Override
    protected View getContainerView() {
        return containerView;
    }

    @Override
    protected View getProgressView() {
        return progressbarContainer;
    }

    @Override
    public void onEmptyResult() {
        emptyFavoritesView.setText("No existen favoritos para tu búsqueda");
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

}
