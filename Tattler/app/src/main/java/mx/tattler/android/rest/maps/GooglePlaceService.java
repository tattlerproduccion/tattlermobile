package mx.tattler.android.rest.maps;

import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.maps.GeocodeLatLngEvent;
import mx.tattler.android.events.maps.GeocodeLatLngResultEvent;
import mx.tattler.android.events.maps.PlaceDetailEvent;
import mx.tattler.android.events.maps.PlaceDetailsResultEvent;
import mx.tattler.android.events.maps.PlaceInputEvent;
import mx.tattler.android.events.maps.PlaceQueryResultEvent;
import mx.tattler.android.rest.maps.models.AutocompleteResponse;
import mx.tattler.android.rest.maps.models.GeocodeResponse;
import mx.tattler.android.rest.maps.models.PlaceResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GooglePlaceService {

    private static final String API_KEY = "AIzaSyDMk17Ri-kpTsHZOlOGVFUoWddOXBnfsso";
    private static final String DEFAULT_TYPES = "(cities)";
    private static final String DEFAULT_GEOCODE_TYPE = "(locality)";

    private static final String TAG = GooglePlaceService.class.getName();

    private GooglePlacesApi placeApi;
    private Bus bus;

    public GooglePlaceService(GooglePlacesApi placeApi, Bus bus) {
        this.placeApi = placeApi;
        this.bus = bus;
    }

    @Subscribe
    public void onQueryPlace(PlaceInputEvent event) {

        Log.e(TAG, "onQueryPlace");

        placeApi.getPredictions(API_KEY, event.countryCode, event.input, DEFAULT_TYPES, GooglePlacesApi.DEFAULT_LOCALE,
                new Callback<AutocompleteResponse>() {

            @Override
            public void success(AutocompleteResponse autocompleteResponse, Response response) {
                bus.post(new PlaceQueryResultEvent(autocompleteResponse.getPredictions()));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onPlaceDetails(PlaceDetailEvent event) {

        Log.e(TAG, "onQueryPlace");

        placeApi.getPlace(API_KEY, event.placeId, GooglePlacesApi.DEFAULT_LOCALE, new Callback<PlaceResponse>() {

            @Override
            public void success(PlaceResponse autocompleteResponse, Response response) {
                bus.post(new PlaceDetailsResultEvent(autocompleteResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onPlaceFromLatLng(GeocodeLatLngEvent event) {

        Log.e(TAG, "onPlaceFromLatLng");
        String formatedLatLng = event.latLng.latitude + "," + event.latLng.longitude;

        placeApi.getPlaceFromLatLng(formatedLatLng, new Callback<GeocodeResponse>() {
            @Override
            public void success(GeocodeResponse geocodeResponse, Response response) {
                bus.post(new GeocodeLatLngResultEvent(geocodeResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

}