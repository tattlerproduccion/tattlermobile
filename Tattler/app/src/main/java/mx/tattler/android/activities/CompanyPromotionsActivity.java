package mx.tattler.android.activities;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.constants.CompanyConstants;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.GetPromotionsForCompanyEvent;
import mx.tattler.android.events.tattler.GetPromotionsForCompanyResultEvent;
import mx.tattler.android.fragments.dialogs.CouponDialogFragment;
import mx.tattler.android.rest.tattler.models.Promotion;
import mx.tattler.android.rest.tattler.models.VisitCompanyPromotionsEvent;
import mx.tattler.android.utils.social.SocialNetworkHelper;

public class CompanyPromotionsActivity extends BusFragmentActivity implements BaseSliderView.OnSliderClickListener {

    private static final String TAG = CompanyPromotionsActivity.class.getName();
    private static final String PROMOTION_INDEX = "promotionIndex";

    private TextView emptyPromotions;
    private ShareActionProvider shareActionProvider;

    private SliderLayout sliderShow;

    private List<Promotion> promotions = new ArrayList<>();
    private String companyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_detail);

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.promotions), null);
            hideTattlerActionbarLogo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        companyId = getIntent().getStringExtra(CompanyConstants.COMPANY_ID);

        emptyPromotions = (TextView) findViewById(R.id.tv_empty_promotions);

        sliderShow = (SliderLayout) findViewById(R.id.slider);
        sliderShow.stopAutoCycle();

        getBus().post(new GetPromotionsForCompanyEvent(companyId));

    }

    @Subscribe
    public void onPromotionsLoaded(GetPromotionsForCompanyResultEvent event){

        if(event.promotions.size() == 0){
            emptyPromotions.setVisibility(View.VISIBLE);
            return;
        }

        sliderShow.removeAllSliders();

        this.promotions = event.promotions;
        TextSliderView textSliderView;

        Promotion promotion;
        for (int i = 0; i < promotions.size(); i++) {
            promotion = promotions.get(i);
            textSliderView = new TextSliderView(this);
            textSliderView.image(promotion.getImage());
            textSliderView.setOnSliderClickListener(this);
            textSliderView.getBundle().putInt(PROMOTION_INDEX, i);
            sliderShow.addSlider(textSliderView);
        }

        getBus().post(new VisitCompanyPromotionsEvent(companyId));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.promotion, menu);

        /*

        MenuItem item = menu.findItem(R.id.action_share_promotion);
        shareActionProvider = (ShareActionProvider) item.getActionProvider();
        Intent shareIntent = URLIntentFactory.create("Tattler App", "http://www.tattler.mx/");
        shareActionProvider.setShareIntent(shareIntent);

        shareActionProvider.setOnShareTargetSelectedListener(new ShareActionProvider.OnShareTargetSelectedListener() {
            @Override
            public boolean onShareTargetSelected(ShareActionProvider shareActionProvider, Intent intent) {



                return false;
            }
        });
        */

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if(promotions.isEmpty())
            return false;

        int id = item.getItemId();

        BaseSliderView sliderView = sliderShow.getCurrentSlider();

        if(sliderView == null)
            return false;

        int promotionIndex = sliderView.getBundle().getInt(PROMOTION_INDEX);

        Promotion currentPromotion = promotions.get(promotionIndex);

        String message = currentPromotion.getTitle();
        String imageUrl = currentPromotion.getImage();

        switch (id){
            case R.id.action_share_facebook:
                SocialNetworkHelper.shareOnFacebook(this, message, imageUrl);
                break;
            case R.id.action_share_tattler:
                SocialNetworkHelper.shareOnTattler(this, currentPromotion);
                break;
            case R.id.action_share_twitter:
                SocialNetworkHelper.shareOnTwitter(this, message, imageUrl);
                break;
            case R.id.action_share_whatsapp:
                message += " "  + imageUrl;
                SocialNetworkHelper.shareWhatsApp(this, message);
                break;
            default:

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

        int promotionIndex = slider.getBundle().getInt(PROMOTION_INDEX);
        Promotion currentPromotion = promotions.get(promotionIndex);

        FragmentManager manager = getSupportFragmentManager();
        DialogFragment dialog = CouponDialogFragment.newInstance(currentPromotion);
        dialog.show(manager, "coupon");

    }

    @Subscribe
    public void onApiError(ApiErrorEvent event){

        Log.e(TAG, event.error.getRawBody());
        Toast.makeText(this, event.error.getRawBody(), Toast.LENGTH_LONG).show();

    }

}
