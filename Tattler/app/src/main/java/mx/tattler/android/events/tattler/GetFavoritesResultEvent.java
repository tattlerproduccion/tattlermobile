package mx.tattler.android.events.tattler;

import java.util.List;

import mx.tattler.android.rest.tattler.models.Company;

public class GetFavoritesResultEvent {

    public final List<Company> companies;

    public GetFavoritesResultEvent(List<Company> companies) {
        this.companies = companies;
    }
}
