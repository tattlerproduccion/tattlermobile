package mx.tattler.android.rest.maps;

import mx.tattler.android.rest.maps.models.AutocompleteResponse;
import mx.tattler.android.rest.maps.models.GeocodeResponse;
import mx.tattler.android.rest.maps.models.PlaceResponse;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface GooglePlacesApi {

    public static final String API_URL = "https://maps.googleapis.com/maps/api";
    public static final String DEFAULT_LOCALE = "es";

    @GET("/place/autocomplete/json")
    public void getPredictions(@Query("key") String key, @Query("components") String components,
                               @Query("input") String input, @Query("input") String types, @Query("language") String language,
                               Callback<AutocompleteResponse> callback);

    @GET("/place/details/json")
    public void getPlace(@Query("key") String key, @Query("placeid") String placeId, @Query("language") String language,
                         Callback<PlaceResponse> callback);

    @GET("/geocode/json")
    public void getPlaceFromLatLng(@Query("latlng") String latlng, Callback<GeocodeResponse> callback);

}
