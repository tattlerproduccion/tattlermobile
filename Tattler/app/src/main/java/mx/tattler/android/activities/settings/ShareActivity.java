package mx.tattler.android.activities.settings;

import android.os.Bundle;

import mx.tattler.android.R;
import mx.tattler.android.activities.BusFragmentActivity;
import mx.tattler.android.fragments.search.ShareWithOthersFragment;

public class ShareActivity extends BusFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new ShareWithOthersFragment())
                    .commit();
        }

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.title_activity_invite_contacts), null);
            setCustomLogo(R.drawable.ic_btn_add_friend);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
