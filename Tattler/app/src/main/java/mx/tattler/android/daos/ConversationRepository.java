package mx.tattler.android.daos;

import android.content.Context;

import java.util.List;

import mx.tattler.android.TattlerApplication;
import mx.tattler.android.rest.tattler.models.chat.Conversation;

public class ConversationRepository {

    public interface BatchUpsertCallback{
        void afterTransaction(List<TattlerConversation> conversations);
    }

    public static void batchUpsert(final Context context, final List<Conversation> remoteConversations, final BatchUpsertCallback callback){

        final List<TattlerConversation> conversations = TattlerConversationFactory.batchConvert(remoteConversations);

        final DaoSession session = getSession(context);

        session.runInTx(new Runnable() {
            @Override
            public void run() {

                for(TattlerConversation conversation : conversations){

                    TattlerConversation found = session.queryBuilder(TattlerConversation.class)
                            .where(TattlerConversationDao.Properties.ConversationId.eq(conversation.getConversationId()))
                            .unique();

                    if(found != null)
                        conversation.setId(found.getId());
                    session.insertOrReplace(conversation);
                }

                List<TattlerConversation> ordered = getAll(context);

                callback.afterTransaction(ordered);

            }
        });

    }

    public static void updateLastMessageForConversation(Context context, TattlerMessage message) {
        TattlerConversation conversation = getByConversationId(context, message.getConversationId());

        if(conversation == null)
            return;

        conversation.setLastMessageType(message.getType());
        conversation.setLastMessage(message.getBody());
        conversation.setLastMessageDate(message.getSentAt());
        insertOrUpdate(context, conversation);

    }

    public static void insertOrUpdate(Context context, TattlerConversation conversation){
        getDao(context).insertOrReplace(conversation);
    }

    public static void clearConversations(Context context){
        getDao(context).deleteAll();
    }

    public static List<TattlerConversation> getAll(Context context){
        return getDao(context).queryBuilder().orderDesc(TattlerConversationDao.Properties.LastMessageDate).list();
    }

    public static TattlerConversation getByReceiverId(Context context, Integer receiverId){
        return getDao(context).queryBuilder().where(TattlerConversationDao.Properties.ReceiverId.eq(receiverId)).unique();
    }

    public static TattlerConversation getByConversationId(Context context, String conversationId){
        return getDao(context).queryBuilder().where(TattlerConversationDao.Properties.ConversationId.eq(conversationId)).unique();
    }

    private static TattlerConversationDao getDao(Context c) {
        return getSession(c).getTattlerConversationDao();
    }

    private static DaoSession getSession(Context c){
        return ((TattlerApplication) c.getApplicationContext()).getDaoSession();
    }

}
