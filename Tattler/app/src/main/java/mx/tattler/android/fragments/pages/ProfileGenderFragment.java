package mx.tattler.android.fragments.pages;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import mx.tattler.android.R;

public class ProfileGenderFragment extends Fragment implements RadioGroup.OnCheckedChangeListener{
    private static final String ARG_KEY = "key";

    private PageFragmentCallbacks mCallbacks;
    private ProfileGenderPage mPage;
    private RadioGroup group;

    public static ProfileGenderFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        ProfileGenderFragment fragment = new ProfileGenderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileGenderFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        String mKey = args.getString(ARG_KEY);
        mPage = (ProfileGenderPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_page_profile_gender, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        group = (RadioGroup) rootView.findViewById(R.id.rg_gender);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String selectedGender = mPage.getData().getString(ProfileGenderPage.GENDER, ProfileGenderPage.MALE);
        int selectedRadioId = selectedGender.equals(ProfileGenderPage.MALE)? R.id.rb_gender_male : R.id.rb_gender_female;
        group.setOnCheckedChangeListener(this);
        group.check(selectedRadioId);

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int id) {
        String gender = id == R.id.rb_gender_male? ProfileGenderPage.MALE : ProfileGenderPage.FEMALE;
        mPage.getData().putString(ProfileGenderPage.GENDER, gender);
        mPage.notifyDataChanged();
    }

}