package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.Promotion;

public class GetPromotionResultEvent {

    public final Promotion promotion;

    public GetPromotionResultEvent(Promotion promotion) {
        this.promotion = promotion;
    }
}
