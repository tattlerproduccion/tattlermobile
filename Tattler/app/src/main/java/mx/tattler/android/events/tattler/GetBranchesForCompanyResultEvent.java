package mx.tattler.android.events.tattler;

import java.util.List;

import mx.tattler.android.rest.tattler.models.Branch;

/**
 * Created by irving on 11/11/14.
 */
public class GetBranchesForCompanyResultEvent {

    public List<Branch> branches;

    public GetBranchesForCompanyResultEvent(List<Branch> branches) {
        this.branches = branches;
    }
}
