package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.rest.tattler.models.chat.Conversation;

public class GetConversationMessagesEvent {

    public final Conversation conversation;
    public final boolean toExport;

    public GetConversationMessagesEvent(Conversation conversation, boolean toExport) {
        this.conversation = conversation;
        this.toExport = toExport;
    }

}
