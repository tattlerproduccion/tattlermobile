package mx.tattler.android.adapters.chat;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import github.ankushsachdeva.emojicon.EmojiconTextView;
import mx.tattler.android.R;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.utils.text.DateTextHelper;

public class ConversationsAdapter extends BaseAdapter implements Filterable {

    public interface OnFilterListener {
        void onEmptyResult();
    }

    private static final String TAG = ConversationsAdapter.class.getName();

    private List<TattlerConversation> conversations = new ArrayList<>();
    private List<TattlerConversation> conversationsBackup = new ArrayList<>();
    private LayoutInflater inflater;
    private Picasso picasso;
    private OnFilterListener listener;

    public ConversationsAdapter(Context ctx, OnFilterListener listener) {
        this.listener = listener;
        this.inflater = LayoutInflater.from(ctx);
        picasso = Picasso.with(ctx);
    }

    @Override
    public int getCount() {
        return conversations.size();
    }

    @Override
    public TattlerConversation getItem(int position) {
        return conversations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_list_branch_conversation, parent, false);
            holder = new ViewHolder();
            holder.companyName = (TextView) convertView.findViewById(R.id.tv_company_name);
            holder.receiverName = (TextView) convertView.findViewById(R.id.tv_branch_name);
            holder.lastMessage = (EmojiconTextView) convertView.findViewById(R.id.tv_last_message);
            holder.lastMessageDate = (TextView) convertView.findViewById(R.id.tv_last_message_date);
            holder.profileImageView = (ImageView) convertView.findViewById(R.id.iv_profile_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        TattlerConversation conversation = conversations.get(position);

        holder.companyName.setText(conversation.getReceiverCompanyName());
        holder.receiverName.setText(conversation.getReceiverName());

        String lastMessage = Message.getLastMessageFromDb(conversation.getLastMessage(), conversation.getLastMessageType());
        holder.lastMessage.setText(lastMessage);

        String lastMessageDate = DateTextHelper.formatToChatDate(conversation.getLastMessageDate());
        holder.lastMessageDate.setText(lastMessageDate);

        String imageUrl = conversation.getReceiverPicture();

        Log.i(TAG, String.format("Company image: %s", imageUrl));

        picasso.load(imageUrl)
                .placeholder(R.drawable.ic_default_company)
                .error(R.drawable.ic_default_company).into(holder.profileImageView);

        return convertView;

    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public void setConversations(List<TattlerConversation> conversations, boolean filtered) {

        if (!filtered) {
            this.conversationsBackup = conversations;
        }

        this.conversations = conversations;
        notifyDataSetChanged();
    }


    public void itemUpdated(TattlerConversation updatedConversation) {

        for (TattlerConversation conversation : conversations) {
            if (conversation.getId().equals(updatedConversation.getId())) {
                conversation.setLastMessageType(updatedConversation.getLastMessageType());
                conversation.setLastMessage(updatedConversation.getLastMessage());
                conversation.setLastMessageDate(updatedConversation.getLastMessageDate());
                break;
            }
        }

        for (TattlerConversation conversation : conversationsBackup) {
            if (conversation.getId().equals(updatedConversation.getId())) {
                conversation.setLastMessageType(updatedConversation.getLastMessageType());
                conversation.setLastMessage(updatedConversation.getLastMessage());
                conversation.setLastMessageDate(updatedConversation.getLastMessageDate());
                break;
            }
        }

        notifyDataSetChanged();

    }

    private Filter filter = new Filter() {
        @Override
        protected Filter.FilterResults performFiltering(CharSequence constraint) {

            Log.i(TAG, "performFiltering");

            Filter.FilterResults filterResults = new Filter.FilterResults();

            if (TextUtils.isEmpty(constraint)) {

                filterResults.count = conversationsBackup.size();
                filterResults.values = conversationsBackup;

            } else {

                Log.i(TAG, "performFiltering for " + constraint);

                List<TattlerConversation> filteredConversations = new ArrayList<>();

                String receiverName;
                for (TattlerConversation conversation : conversations) {

                    receiverName = conversation.getReceiverCompanyName();

                    if (!TextUtils.isEmpty(receiverName) && receiverName.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filteredConversations.add(conversation);
                    }
                }

                filterResults.count = filteredConversations.size();
                filterResults.values = filteredConversations;

            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {

            if (results.values == null) {
                Log.e(TAG, "valores filtrados nulos!");
                return;
            }

            Log.i(TAG, "Results size for " + constraint + ": " + results.count);

            List<TattlerConversation> result = (List<TattlerConversation>) results.values;
            Log.i(TAG, "Conversations found" + result.size());
            setConversations(result, true);

            if (result.isEmpty())
                listener.onEmptyResult();

        }

    };


    private static class ViewHolder {
        TextView companyName;
        TextView receiverName;
        EmojiconTextView lastMessage;
        TextView lastMessageDate;
        ImageView profileImageView;
    }

}