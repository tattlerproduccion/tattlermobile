package mx.tattler.android.events.maps;

import java.util.List;

import mx.tattler.android.rest.maps.models.Prediction;

public class PlaceQueryResultEvent {

    public final List<Prediction> predictions;

    public PlaceQueryResultEvent(List<Prediction> predictions){
        this.predictions = predictions;
    }

}