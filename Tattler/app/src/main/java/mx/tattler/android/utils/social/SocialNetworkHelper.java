package mx.tattler.android.utils.social;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.facebook.widget.FacebookDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.parceler.Parcels;

import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.activities.CompanyPromotionsActivity;
import mx.tattler.android.activities.chat.ConversationActivity;
import mx.tattler.android.activities.chat.SharePromotionActivity;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.rest.tattler.models.Promotion;
import mx.tattler.android.utils.image.ImageHelper;
import mx.tattler.android.utils.resource.ResourceFactory;

public class SocialNetworkHelper {

    private SocialNetworkHelper() {
    }

    public static void shareWhatsApp(Activity appActivity, String message) {

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(android.content.Intent.EXTRA_TEXT, message);

        PackageManager pm = appActivity.getApplicationContext().getPackageManager();
        final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
        boolean temWhatsApp = false;
        for (final ResolveInfo info : matches) {
            if (info.activityInfo.packageName.startsWith("com.whatsapp")) {
                final ComponentName name = new ComponentName(info.activityInfo.applicationInfo.packageName, info.activityInfo.name);
                sendIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
                sendIntent.setComponent(name);
                temWhatsApp = true;
                break;
            }
        }

        if (temWhatsApp) {
            appActivity.startActivity(sendIntent);
        } else {
            Toast.makeText(appActivity, appActivity.getString(R.string.whatsapp_not_present), Toast.LENGTH_SHORT).show();
        }
    }

    public static void shareOnFacebook(Activity activity, String text, Fragment callerFragment) {

        // Check if the Facebook app is installed and we can present the share dialog
        FacebookDialog.MessageDialogBuilder builder = new FacebookDialog.MessageDialogBuilder(activity)
                .setLink("http://tattler.mx/")
                .setName(text)
                        //.setCaption("Build great social apps that engage your friends.")
                .setPicture("http://tattler.mx/components/app-data/img/home/phone-sm.png")
                .setDescription(text)
                .setFragment(callerFragment);

        // If the Facebook app is installed and we can present the share dialog
        if (builder.canPresent()) {
            // Enable button or other UI to initiate launch of the Message Dialog
            // Present message dialog
            FacebookDialog dialog = builder.build();
            dialog.present();
        }

    }

    public static void shareOnFacebook(Activity activity, String text, String imageUrl) {

        //FacebookDialog.ShareDialogBuilder

        // Check if the Facebook app is installed and we can present the share dialog
        FacebookDialog.ShareDialogBuilder builder = new FacebookDialog.ShareDialogBuilder(activity)
                .setLink(imageUrl)
                .setName(text)
                        //.setCaption("Build great social apps that engage your friends.")
                .setPicture(imageUrl)
                .setDescription(text);

        // If the Facebook app is installed and we can present the share dialog
        if (builder.canPresent()) {
            // Enable button or other UI to initiate launch of the Message Dialog
            // Present message dialog
            FacebookDialog dialog = builder.build();
            dialog.present();
        }

    }

    public static void shareOnTwitter(Activity activity, String message) {

        Intent tweetIntent = createShareIntent(activity, message, null);
        shareOnTwitter(activity, tweetIntent);

    }

    public static void shareOnTwitter(final Activity activity, final String message, final String imageUrl) {

        Target loadtarget = new Target() {

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                Intent tweetIntent = createShareIntent(activity, message, bitmap);
                shareOnTwitter(activity, tweetIntent);

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

                Intent tweetIntent = createShareIntent(activity, message, null);
                shareOnTwitter(activity, tweetIntent);

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

        };

        Picasso.with(activity).load(imageUrl).into(loadtarget);

    }

    public static void shareOnInstagram(Activity activity, String caption) {

        Intent shareIntent = createShareIntent(activity, caption, null);

        try {

            shareIntent.setPackage("com.instagram.android");
            activity.startActivity(shareIntent);

        } catch (Exception ignored) {

            // Broadcast the Intent.
            activity.startActivity(Intent.createChooser(shareIntent, "Compartir con"));

        }

    }

    private static Intent createShareIntent(Activity activity, String message, Bitmap bitmap) {

        Uri uri;

        // Create the URI from the media
        if (bitmap != null) {

            String path = MediaStore.Images.Media.insertImage(activity.getContentResolver(),
                    bitmap, "title", null);

            uri = Uri.parse(path);

        } else {
            uri = ResourceFactory.resourceToUri(activity, R.drawable.ic_tattler_logo_square);
        }

        // Create the new Intent using the 'Send' action.
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        // Set the MIME type
        shareIntent.setType("image/*");
        // Add the URI and the caption to the Intent.
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);

        return shareIntent;

    }

    private static void shareOnTwitter(Activity activity, Intent tweetIntent) {

        PackageManager packManager = activity.getApplicationContext().getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for (ResolveInfo resolveInfo : resolvedInfoList) {
            if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name);
                resolved = true;
                break;
            }
        }

        if (resolved) {
            activity.startActivity(tweetIntent);
        } else {
            Toast.makeText(activity, activity.getString(R.string.twitter_not_present), Toast.LENGTH_SHORT).show();
        }

    }

    public static void shareOnTattler(Activity activity, Promotion currentPromotion) {

        Intent intent = new Intent(activity, SharePromotionActivity.class);

        //TODO: Wrap conversation to target activity
        Bundle bundle = new Bundle();
        Parcelable wrapped = Parcels.wrap(currentPromotion);
        bundle.putParcelable(ConversationConstants.PROMOTION, wrapped);
        intent.putExtras(bundle);
        activity.startActivity(intent);

    }

}
