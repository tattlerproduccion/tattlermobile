package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.events.ApiErrorEvent;
import retrofit.RetrofitError;

public class UploadAttachmentErrorEvent extends ApiErrorEvent{

    public UploadAttachmentErrorEvent(RetrofitError error) {
        super(error);
    }
}
