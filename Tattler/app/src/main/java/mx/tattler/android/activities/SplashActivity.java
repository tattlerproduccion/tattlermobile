package mx.tattler.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.TattlerApplication;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.profile.InitialCurrentUserEvent;

public class SplashActivity extends BusActivity {

    private static final String TAG = SplashActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

    }

    @Override
    public void onResume() {
        super.onResume();
        getBus().post(new InitialCurrentUserEvent());
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {

        Log.e(TAG, event.error.getRawBody());

        TattlerApplication application = (TattlerApplication) getApplication();
        application.clearSession();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }


}
