package mx.tattler.android.activities;

import android.os.Bundle;

import com.squareup.otto.Bus;

import mx.tattler.android.R;
import mx.tattler.android.fragments.RegisterFragment;
import mx.tattler.android.fragments.TermsConditionsFragment;

public class SignUpActivity extends BusFragmentActivity
        implements TermsConditionsFragment.OnTermsListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new TermsConditionsFragment())
                    .commit();
        }
    }

    @Override
    public void onTermsInteraction(boolean accepted) {
        if (accepted) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new RegisterFragment())
                    .commit();
        } else {
            finish();
        }

    }

    @Override
    public Bus onRequest() {
        return getBus();
    }
}
