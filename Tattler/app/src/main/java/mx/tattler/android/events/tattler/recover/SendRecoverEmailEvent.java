package mx.tattler.android.events.tattler.recover;

import mx.tattler.android.rest.tattler.models.User;

public class SendRecoverEmailEvent {

    public final User user;

    public SendRecoverEmailEvent(User user) {
        this.user = user;
    }

}
