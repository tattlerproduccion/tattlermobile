package mx.tattler.android.rest.tattler.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.SphericalUtil;

import org.parceler.Parcel;

import java.util.List;

import mx.tattler.android.rest.tattler.models.schedule.Schedule;

@Parcel
public class Branch {

    @Expose
    private String id;
    @Expose
    private String name;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @Expose
    private Address address;
    @Expose
    private Location location;
    @SerializedName("messaging_id")
    @Expose
    private Integer messagingId;

    @SerializedName("allowsPhoneCalling")
    @Expose
    private boolean allowsPhoneCalling;

    @SerializedName("allowsMessaging")
    @Expose
    private boolean allowsMessaging;

    @Expose
    private Schedule schedule;

    private transient String conversationId;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber The phone_number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }


    public void setMessagingId(Integer messagingId) {
        this.messagingId = messagingId;
    }

    public Integer getMessagingId() {
        return messagingId;
    }

    public LatLng getLocationAsLatLng() {
        return getLocation().getLatLng();
    }

    public com.mapbox.mapboxsdk.geometry.LatLng getLocationAsMapboxLatLng() {
        LatLng raw = getLocation().getLatLng();
        return new com.mapbox.mapboxsdk.geometry.LatLng(raw.latitude, raw.longitude);
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public boolean isAllowsPhoneCalling() {
        return allowsPhoneCalling;
    }

    public void setAllowsPhoneCalling(boolean allowsPhoneCalling) {
        this.allowsPhoneCalling = allowsPhoneCalling;
    }

    public boolean isAllowsMessaging() {
        return allowsMessaging;
    }

    public void setAllowsMessaging(boolean allowsMessaging) {
        this.allowsMessaging = allowsMessaging;
    }

    public String distanceFromPoint(LatLng point){

        double distanceInMeters = SphericalUtil.computeDistanceBetween(location.getLatLng(), point);
        double distanceInKms = distanceInMeters / 1000;
        return String.format("%.2f Km", distanceInKms);

    }

    public double distanceFromPointInMeters(LatLng point){
        return SphericalUtil.computeDistanceBetween(location.getLatLng(), point);
    }

    public static Branch nearestFromPoint(List<Branch> branches, LatLng point){

        Branch nearest = branches.get(0);
        double nearestDistance = SphericalUtil.computeDistanceBetween(nearest.location.getLatLng(), point);
        double distance = 0;

        for(Branch branch : branches){

            distance = SphericalUtil.computeDistanceBetween(branch.location.getLatLng(), point);
            if(distance < nearestDistance){
                nearestDistance = distance;
                nearest = branch;
            }

        }

        return nearest;

    }

    /**
     *
     * @return
     *     The schedule
     */
    public Schedule getSchedule() {
        return schedule;
    }

    /**
     *
     * @param schedule
     *     The schedule
     */
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

}