package mx.tattler.android.chat;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.quickblox.auth.QBAuth;
import com.quickblox.auth.model.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBEntityCallbackImpl;
import com.quickblox.core.QBSettings;
import com.quickblox.core.exception.BaseServiceException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;

import org.jivesoftware.smack.SmackException;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.listeners.ChatConnectionListener;

public class ChatServiceFactory {

    private static final String TAG = ChatServiceFactory.class.getName();

    public interface OnInitServiceListener {

        void onSuccess(QBChatService chatService);
        void onError(List errors);

    }

    public static void instanceService(final Context context, final String email, final String password, final OnInitServiceListener callback) throws BaseServiceException {

        //Fast QB config
        QBSettings.getInstance().fastConfigInit("19098", "X2R8GzYuVS9rwjp", "ZkLuZMmfNMKhvxE");

        QBAuth.createSession(new QBEntityCallback<QBSession>() {
            @Override
            public void onSuccess(QBSession qbSession, Bundle bundle) {

                QBChatService chatService;

                QBChatService.setDebugEnabled(true);
                if (!QBChatService.isInitialized()) {
                    QBChatService.init(context);
                    chatService = QBChatService.getInstance();
                    chatService.addConnectionListener(new ChatConnectionListener());
                }else{
                    chatService = QBChatService.getInstance();
                }

                initUser(chatService, email, password, callback);

            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> strings) {

                // errors
                for (Object error : strings)
                    Log.e(TAG, String.format("QBAuth.createSession: %s", error.toString()));

                callback.onError(strings);

            }
        });

    }

    private static void initUser(final QBChatService chatService, final String email, final String password, final OnInitServiceListener callback){

        Log.i(TAG, "Login with email: " + email + ", password: " + password);

        QBUsers.signInByEmail(email, password, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                qbUser.setPassword(password);
                initChat(chatService, qbUser, callback);
            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(List<String> strings) {

                // errors
                for (Object error : strings)
                    Log.e(TAG, String.format("QBUsers.signInByEmail: %s", error.toString()));

                callback.onError(strings);

            }
        });

    }

    private static void initChat(final QBChatService chatService, QBUser qbUser, final OnInitServiceListener callback) {

        chatService.login(qbUser, new QBEntityCallbackImpl() {

            @Override
            public void onSuccess() {

                Log.i(TAG, "Success on chat login");

                try {
                    chatService.startAutoSendPresence(60);
                    callback.onSuccess(chatService);
                } catch (final SmackException.NotLoggedInException e) {
                    e.printStackTrace();
                    callback.onError(new ArrayList<String>() {{
                        add(e.getMessage());
                    }});
                }

            }

            @Override
            public void onError(List errors) {
                for (Object error : errors)
                    Log.e(TAG, String.format("QBChatService Login error: %s", error.toString()));

                callback.onError(errors);
            }

        });
    }


}
