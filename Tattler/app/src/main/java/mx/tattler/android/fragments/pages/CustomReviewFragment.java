package mx.tattler.android.fragments.pages;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech.freak.wizardpager.ui.ReviewFragment;

import mx.tattler.android.R;

public class CustomReviewFragment extends ReviewFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        TextView titleView = (TextView) rootView.findViewById(android.R.id.title);
        titleView.setText(R.string.review_spanish);
        titleView.setGravity(Gravity.CENTER_HORIZONTAL);
        titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);

        return rootView;
    }
}
