package mx.tattler.android.events.maps;

import mx.tattler.android.rest.maps.models.PlaceResponse;

public class PlaceDetailsResultEvent {

    public final PlaceResponse response;

    public PlaceDetailsResultEvent(PlaceResponse response) {
        this.response = response;
    }
}