package mx.tattler.android.events.tattler;

import java.util.Map;

public class SearchCompaniesEvent {

    public Map<String, String> params;

    public SearchCompaniesEvent(Map<String, String> params) {
        this.params = params;
    }
}
