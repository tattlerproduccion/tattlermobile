package mx.tattler.android.utils.resource;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ImageMediaHelper {

    public static boolean invalidVideoDuration(Context context, Uri videoUri){
        MediaPlayer mp = MediaPlayer.create(context, videoUri);
        long secondsDuration = TimeUnit.MILLISECONDS.toSeconds(mp.getDuration());
        mp.release();
        return  secondsDuration > 3;
    }

    public static void loadImageIntoView(Context context, Uri imageUri, ImageView target) throws Exception {

        String picturePath = FileUtils.getPath(context, imageUri);
        Bitmap source = BitmapFactory.decodeFile(picturePath);

        if (source == null) {
            throw new Exception("No se pudo cargar imagen");
        }

        target.setImageBitmap(source);

    }

    public static Uri saveRawToDisk(Context context, byte[] rawImageBytes) throws Exception {
        Bitmap originalBmp = BitmapFactory.decodeByteArray(rawImageBytes, 0, rawImageBytes.length);
        return saveBitmapToDisk(context, originalBmp);
    }

    public static Uri resizedBitmapUriIfNeeded(Context context, Uri imageUri) throws Exception {

        String picturePath = FileUtils.getPath(context, imageUri);
        Bitmap source = BitmapFactory.decodeFile(picturePath);

        if (hasToResize(source)) {
            source = ThumbnailUtils.extractThumbnail(source, 500, 500);
            return saveBitmapToDisk(context, source);
        } else {
            return imageUri;
        }

    }

    public static Uri saveBitmapToDisk(Context context, Bitmap bmImg) throws Exception {

        if (hasToResize(bmImg))
            bmImg = ThumbnailUtils.extractThumbnail(bmImg, 500, 500);

        File filename;

        File file = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        if (!file.exists())
            file.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = timeStamp + ".jpg";

        filename = new File(file.getAbsolutePath() + "/" + imageFileName);
        FileOutputStream out = new FileOutputStream(filename);
        bmImg.compress(Bitmap.CompressFormat.JPEG, 90, out);
        out.flush();
        out.close();

        ContentValues image = new ContentValues();
        image.put(MediaStore.Images.Media.TITLE, "Nearmart");
        image.put(MediaStore.Images.Media.DISPLAY_NAME, "Image");
        image.put(MediaStore.Images.Media.DESCRIPTION, "Camera Image");
        image.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
        image.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
        image.put(MediaStore.Images.Media.ORIENTATION, 0);

        File parent = filename.getParentFile();
        image.put(MediaStore.Images.ImageColumns.BUCKET_ID, parent.toString()
                .toLowerCase().hashCode());
        image.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, parent.getName()
                .toLowerCase());
        image.put(MediaStore.Images.Media.SIZE, filename.length());
        image.put(MediaStore.Images.Media.DATA, filename.getAbsolutePath());

        //byte[] data = Files.toByteArray(file);

        return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, image);

    }

    private static boolean hasToResize(Bitmap bmImg) {
        return (bmImg.getWidth() > 510 || bmImg.getHeight() > 510);
    }

    private ImageMediaHelper() {
    }

}