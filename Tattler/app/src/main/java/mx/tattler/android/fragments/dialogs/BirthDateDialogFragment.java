package mx.tattler.android.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.DatePicker;

import org.parceler.Parcels;

import java.util.Calendar;

import mx.tattler.android.R;
import mx.tattler.android.events.tattler.profile.ProfileBirthDateUpdatedEvent;
import mx.tattler.android.rest.tattler.models.profile.BirthDate;

public class BirthDateDialogFragment extends BusDialogFragment {

    private static final String TAG = BirthDateDialogFragment.class.getName();

    public static BirthDateDialogFragment newInstance(BirthDate birthDate) {

        BirthDateDialogFragment fragment = new BirthDateDialogFragment();
        Bundle args = new Bundle();
        Parcelable wrapped = Parcels.wrap(birthDate);
        args.putParcelable("birthdate", wrapped);

        fragment.setArguments(args);
        return fragment;

    }

    public BirthDateDialogFragment() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final BirthDate birthDate = Parcels.unwrap(getArguments().getParcelable("birthdate"));

        Calendar calendar = birthDate.getCalendarDate();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_fragment_birthdate, null);

        DatePicker birthDatePicker = (DatePicker) view.findViewById(R.id.dp_birthday);
        birthDatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker datePicker, int year, int month, int day) {

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);

                birthDate.setCalendarDate(calendar);

            }
        });

        alertDialogBuilder.setView(view);
        alertDialogBuilder.setTitle("Fecha de cumpleaños");
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onRequest().post(new ProfileBirthDateUpdatedEvent(birthDate));
            }
        });

        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        return alertDialogBuilder.create();
    }
}
