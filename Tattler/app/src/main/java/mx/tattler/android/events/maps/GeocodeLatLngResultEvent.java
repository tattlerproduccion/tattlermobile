package mx.tattler.android.events.maps;

import mx.tattler.android.rest.maps.models.GeocodeResponse;

public class GeocodeLatLngResultEvent {

    public final GeocodeResponse response;

    public GeocodeLatLngResultEvent(GeocodeResponse response) {
        this.response = response;
    }
}