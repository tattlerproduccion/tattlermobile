package mx.tattler.android.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.squareup.otto.Subscribe;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.activities.call.CallScreenActivity;
import mx.tattler.android.activities.chat.ConversationActivity;
import mx.tattler.android.adapters.BranchListAdapter;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.daos.TattlerConversationFactory;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.CurrentUserEvent;
import mx.tattler.android.events.tattler.CurrentUserResultEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsResultEvent;
import mx.tattler.android.fragments.dialogs.BranchDialogFragment;
import mx.tattler.android.rest.tattler.models.Branch;
import mx.tattler.android.rest.tattler.models.Company;
import mx.tattler.android.rest.tattler.models.chat.Conversation;
import mx.tattler.android.utils.location.BestLocationManager;
import mx.tattler.android.utils.map.MapAreaManager;

public class BranchListActivity extends AnimatedBusFragmentActivity implements BranchListAdapter.OnBranchListener, AdapterView.OnItemClickListener {

    private static final String TAG = BranchListActivity.class.getName();

    private View progressbarContainer;
    private View containerView;
    private Company company;
    private List<Branch> branches;
    private FragmentManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_list);

        final List<Branch> unfiltered = Parcels.unwrap(getIntent().getExtras().getParcelable("branches"));

        if (unfiltered == null) {
            Toast.makeText(this, "Empresa no tiene sucursales", Toast.LENGTH_LONG).show();
            finish();
        }

        //Configure actionbar
        try {
            inflateCustomActionbar();
            setActionbarTitle(getResources().getString(R.string.branches), null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        progressbarContainer = findViewById(R.id.pb_loading);
        containerView = findViewById(R.id.ly_container);

        final ListView listView = (ListView) findViewById(R.id.lv_branches);
        listView.setOnItemClickListener(BranchListActivity.this);
        TextView emptyListText = (TextView) findViewById(R.id.tv_empty_company_branches);
        listView.setEmptyView(emptyListText);

        company = Parcels.unwrap(getIntent().getExtras().getParcelable("company"));


        //Configure current location
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        Double latitude = Double.parseDouble(preferences.getString(getString(R.string.latitude), getString(R.string.default_latitude)));
        Double longitude = Double.parseDouble(preferences.getString(getString(R.string.longitude), getString(R.string.default_longitude)));
        final double boundRadius = (double) preferences.getFloat(getString(R.string.radius), MapAreaManager.DEFAULT_MAP_AREA_RADIUS);
        final LatLng point = new LatLng(latitude, longitude);

        Collection<Branch> branchesCollection = Collections2.filter(unfiltered, new Predicate<Branch>() {
            @Override
            public boolean apply(Branch branch) {
                return branch.distanceFromPointInMeters(point) <= boundRadius;
            }
        });

        List<Branch> branchesFiltered = new ArrayList<>(branchesCollection);

        Collections.sort(branchesFiltered, new Comparator<Branch>() {
            @Override
            public int compare(Branch branch1, Branch branch2) {
                return (int) (branch1.distanceFromPointInMeters(point) - branch2.distanceFromPointInMeters(point));
            }
        });

        BranchListActivity.this.branches = branchesFiltered;

        listView.setAdapter(new BranchListAdapter(BranchListActivity.this, branches, BranchListActivity.this, point));

        if (branches.isEmpty()) {
            emptyListText.setText("No te encuentras cerca de alguna sucursal");
        }

        manager = getSupportFragmentManager();

        showProgress(true);

        //Request current user to validate if its blocked
        getBus().post(new CurrentUserEvent());

    }

    @Subscribe
    /*
     * Validate if users account is blocked, then notify via dialog.
     */
    public void onCurrentUser(CurrentUserResultEvent event) {

        if (!event.user.isBlocked()) {
            //Request conversations so we can recover its conversation id
            getBus().post(new GetConversationsEvent());
            return;
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.tattler_blocked_message);
        alertDialogBuilder.setNeutralButton(R.string.accept_tattler_blocked_acknowledge, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = alertDialogBuilder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                finishAndClearTask();
            }
        });

        dialog.show();

    }

    @Subscribe
    public void onConversations(GetConversationsResultEvent event) {

        Log.i(TAG, String.format("Conversaciones: %d", event.conversations.size()));

        List<Conversation> conversations = event.conversations;

        for (Conversation conversation : conversations) {
            for (Branch branch : branches) {
                if (conversation.getReceiverId().equals(branch.getMessagingId())) {
                    branch.setConversationId(conversation.getId());
                }
            }
        }

        showProgress(false);

    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {
        Toast.makeText(this, "Error interno", Toast.LENGTH_LONG).show();
        finishAndClearTask();
    }

    private void finishAndClearTask() {
        Intent intent = new Intent(BranchListActivity.this, MainDrawerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected View getContainerView() {
        return containerView;
    }

    @Override
    protected View getProgressView() {
        return progressbarContainer;
    }

    @Override
    public void initConversation(Branch branch) {

        TattlerConversation conversation = TattlerConversationFactory.create(branch, company);

        Intent intent = new Intent(this, ConversationActivity.class);
        Bundle bundle = new Bundle();
        Parcelable wrapped = Parcels.wrap(conversation);
        bundle.putParcelable(ConversationConstants.CONVERSATION, wrapped);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void initCall(Branch branch) {

        TattlerConversation conversation = TattlerConversationFactory.createCall(branch, company);

        Intent intent = new Intent(this, CallScreenActivity.class);
        Bundle bundle = new Bundle();
        Parcelable wrapped = Parcels.wrap(conversation);
        bundle.putParcelable(ConversationConstants.CONVERSATION, wrapped);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        Branch branch = branches.get(position);

        DialogFragment dialog = BranchDialogFragment.newInstance(branch);
        dialog.show(manager, "branch");

    }

}
