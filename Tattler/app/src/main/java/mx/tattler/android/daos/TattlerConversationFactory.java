package mx.tattler.android.daos;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.rest.tattler.models.Branch;
import mx.tattler.android.rest.tattler.models.Company;
import mx.tattler.android.rest.tattler.models.Participant;
import mx.tattler.android.rest.tattler.models.chat.Conversation;
import mx.tattler.android.rest.tattler.models.chat.Message;

public class TattlerConversationFactory {

    public static List<TattlerConversation> batchConvert(List<Conversation> conversations) {

        List<TattlerConversation> tattlerConversations = new ArrayList<>();

        for (Conversation conversation : conversations) {
            tattlerConversations.add(convert(conversation));
        }

        return tattlerConversations;

    }

    public static TattlerConversation convert(Conversation conversation) {

        TattlerConversation tattlerConversation = new TattlerConversation();
        tattlerConversation.setConversationId(conversation.getId());

        Message lastMessage = conversation.getLastMessage();

        if (lastMessage != null) {
            tattlerConversation.setLastMessage(lastMessage.getBody());
            tattlerConversation.setLastMessageDate(lastMessage.getSentAtDate());
            tattlerConversation.setLastMessageType(lastMessage.getType());
        }

        Participant receiver = conversation.getParticipant();

        if (receiver != null) {
            tattlerConversation.setReceiverId(receiver.getMessagingId());
            tattlerConversation.setReceiverCompanyName(receiver.getCompanyName());
            tattlerConversation.setReceiverName(receiver.getName());
            tattlerConversation.setReceiverPicture(receiver.getProfileImage());
        }

        return tattlerConversation;

    }

    public static TattlerConversation create(Branch branch, Company company) {

        TattlerConversation tattlerConversation = new TattlerConversation();
        tattlerConversation.setConversationId(branch.getConversationId());

        tattlerConversation.setReceiverId(branch.getMessagingId());
        tattlerConversation.setReceiverCompanyName(company.getName());
        tattlerConversation.setReceiverName(branch.getName());
        tattlerConversation.setReceiverPicture(company.getProfileImage());

        return tattlerConversation;

    }

    public static TattlerConversation createCall(Branch branch, Company company) {

        TattlerConversation tattlerConversation = new TattlerConversation();
        tattlerConversation.setConversationId(branch.getConversationId());
        tattlerConversation.setPhone((branch.getPhoneNumber()));
        tattlerConversation.setReceiverId(branch.getMessagingId());
        tattlerConversation.setReceiverCompanyName(company.getName());
        tattlerConversation.setReceiverName(branch.getName());
        tattlerConversation.setReceiverPicture(company.getProfileImage());

        return tattlerConversation;

    }

}
