package mx.tattler.android.adapters.chat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.thin.downloadmanager.ThinDownloadManager;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import github.ankushsachdeva.emojicon.EmojiconTextView;
import mx.tattler.android.R;
import mx.tattler.android.daos.TattlerMessage;
import mx.tattler.android.rest.tattler.models.Promotion;
import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.utils.image.MessageBitmapTransform;
import mx.tattler.android.utils.resource.DownloadRequestFactory;
import mx.tattler.android.utils.text.DateTextHelper;
import mx.tattler.android.widgets.StaticMapView;

public class MessagesAdapter extends BaseAdapter {

    private static final String TAG = MessagesAdapter.class.getName();

    public interface DateMessageListener {
        void onDateChanged(Date date);
    }

    protected final LayoutInflater inflater;
    private List<TattlerMessage> messages;
    private Context context;
    private Picasso picasso;
    private DateMessageListener listener;

    private ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;

    public MessagesAdapter(Context context, DateMessageListener listener) {
        this.listener = listener;
        this.context = context;
        this.messages = new ArrayList<>();
        this.picasso = Picasso.with(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);

    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public TattlerMessage getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);
        final TattlerMessage message = messages.get(position);

        switch (type) {

            case Message.MessageTypes.MESSAGE_TYPE_NORMAL:

                TextMessageViewHolder holderTextMessage;

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.item_list_message, parent, false);
                    holderTextMessage = new TextMessageViewHolder();
                    holderTextMessage.inflate(convertView);
                } else {
                    holderTextMessage = (TextMessageViewHolder) convertView.getTag();
                }

                holderTextMessage.setMessage(message);

                break;

            case Message.MessageTypes.MESSAGE_TYPE_PHOTO:

                PhotoMessageViewHolder holderPhotoMessage;

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.item_list_message_photo, parent, false);
                    holderPhotoMessage = new PhotoMessageViewHolder();
                    holderPhotoMessage.inflate(convertView);
                } else {
                    holderPhotoMessage = (PhotoMessageViewHolder) convertView.getTag();
                }

                holderPhotoMessage.setMessage(message);

                break;

            case Message.MessageTypes.MESSAGE_TYPE_LOCATION:

                LocationMessageViewHolder locationMessage;

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.item_list_message_location, parent, false);
                    locationMessage = new LocationMessageViewHolder();
                    locationMessage.inflate(convertView);
                } else {
                    locationMessage = (LocationMessageViewHolder) convertView.getTag();
                }

                locationMessage.setMessage(message);

                break;

            case Message.MessageTypes.MESSAGE_TYPE_FILE_ATTACHMENT:

                FileMessageViewHolder fileMessage;

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.item_list_message_attachment, parent, false);
                    fileMessage = new FileMessageViewHolder();
                    fileMessage.inflate(convertView);
                } else {
                    fileMessage = (FileMessageViewHolder) convertView.getTag();
                }

                fileMessage.setMessage(message);

                break;

            case Message.MessageTypes.MESSAGE_TYPE_VIDEO:

                VideoMessageViewHolder videoMessage;

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.item_list_message_video, parent, false);
                    videoMessage = new VideoMessageViewHolder();
                    videoMessage.inflate(convertView);
                } else {
                    videoMessage = (VideoMessageViewHolder) convertView.getTag();
                }

                videoMessage.setMessage(message);

                break;
            case Message.MessageTypes.MESSAGE_TYPE_PROMOTION:

                PromotionMessageViewHolder promotionMessage;

                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.item_list_message_promotion, parent, false);
                    promotionMessage = new PromotionMessageViewHolder();
                    promotionMessage.inflate(convertView);
                } else {
                    promotionMessage = (PromotionMessageViewHolder) convertView.getTag();
                }

                promotionMessage.setMessage(message);

                break;

        }

        listener.onDateChanged(message.getSentAt());

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {

        TattlerMessage message = messages.get(position);
        String type = message.getType();

        int intType = 0;

        if (type.equals(Message.Types.Text.name())) {
            intType = Message.MessageTypes.MESSAGE_TYPE_NORMAL;
        } else if (type.equals(Message.Types.File.name())) {
            intType = Message.MessageTypes.MESSAGE_TYPE_FILE_ATTACHMENT;
        } else if (type.equals(Message.Types.Geolocation.name())) {
            intType = Message.MessageTypes.MESSAGE_TYPE_LOCATION;
        } else if (type.equals(Message.Types.Image.name())) {
            intType = Message.MessageTypes.MESSAGE_TYPE_PHOTO;
        } else if (type.equals(Message.Types.Video.name())) {
            intType = Message.MessageTypes.MESSAGE_TYPE_VIDEO;
        } else if (type.equals(Message.Types.Promotion.name())) {
            intType = Message.MessageTypes.MESSAGE_TYPE_PROMOTION;
        }

        return intType;

    }

    @Override
    public int getViewTypeCount() {
        return 6;
    }

    public void setMessages(List<TattlerMessage> messages) {
        Log.i(TAG, messages.toString());
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void addMessage(TattlerMessage message) {
        this.messages.add(message);
        notifyDataSetChanged();
    }


    public void addMessages(List<TattlerMessage> messages) {
        this.messages.addAll(messages);
        notifyDataSetChanged();
    }

    public void stopDownloads() {
        try {
            downloadManager.cancelAll();
            downloadManager.release();
        } catch (Exception ignored) {
        }
    }

    private class BaseViewHolder {

        protected RelativeLayout contentHolder;
        protected LinearLayout contentWithBG;
        protected TextView hourView;

        public void inflate(View convertView) {
            this.contentHolder = (RelativeLayout) convertView.findViewById(R.id.content_holder);
            this.contentWithBG = (LinearLayout) convertView.findViewById(R.id.contentWithBackground);
            this.hourView = (TextView) convertView.findViewById(R.id.tv_message_date_sent);
        }

        protected void setMessage(TattlerMessage message) {

            if (message.getMine()) {
                contentWithBG.setBackgroundResource(R.drawable.chat_icon_incoming);
                contentHolder.setGravity(Gravity.START);
                hourView.setTextColor(Color.parseColor("#0b91c9"));
                hourView.setGravity(Gravity.RIGHT);
            } else {
                contentWithBG.setBackgroundResource(R.drawable.chat_icon_local);
                contentHolder.setGravity(Gravity.END);
                hourView.setTextColor(Color.WHITE);
                hourView.setGravity(Gravity.LEFT);
            }

            String hour = DateTextHelper.formatMessageDateToChatHour(message.getSentAt());
            hourView.setText(hour);
        }

    }

    private class TextMessageViewHolder extends BaseViewHolder {

        protected EmojiconTextView messageView;

        @Override
        public void inflate(View convertView) {
            super.inflate(convertView);
            this.messageView = (EmojiconTextView) convertView.findViewById(R.id.tv_message);
            convertView.setTag(this);
        }

        public void setMessage(TattlerMessage message) {
            super.setMessage(message);

            if (message.getMine()) {
                messageView.setTextColor(Color.parseColor("#0b91c9"));
            } else {
                messageView.setTextColor(Color.WHITE);
            }

            messageView.setText(message.getBody());

        }

    }

    private class LocationMessageViewHolder extends BaseViewHolder {

        private StaticMapView mapView;
        private View touchInterceptorView;
        private TextView locationDescriptionView;

        @Override
        protected void setMessage(final TattlerMessage message) {
            super.setMessage(message);

            String[] location = new String[0];
            String body = message.getBody();

            try {
                Uri googleMapsUri = Uri.parse(body);
                location = googleMapsUri.getQueryParameter("center").split(",");
            } catch (Exception e) {
                if (body.contains(","))
                    location = body.split(",");
            }

            if (location.length != 2) {
                Log.e(TAG, "No se reconoce dirección");
                return;
            }

            final double latitude = Double.parseDouble(location[0]);
            final double longitude = Double.parseDouble(location[1]);
            final float zoom = 15f;

            mapView.configure(latitude, longitude, zoom);

            touchInterceptorView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String label = message.getMine() ? "Mi ubicación" : message.getSender();
                    String encodedLabel = Uri.encode(label);

                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f?z=%f&q=%f,%f(%s)", latitude, longitude, zoom, latitude, longitude, encodedLabel);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    context.startActivity(intent);
                }
            });

            String description;
            if (message.getMine()) {
                locationDescriptionView.setTextColor(Color.parseColor("#0b91c9"));
                description = "Mi ubicación";
            } else {
                locationDescriptionView.setTextColor(Color.WHITE);

                //TODO: Append sender name to db message
                description = String.format("Ubicación de %s", message.getSender());
            }

            locationDescriptionView.setText(description);

        }

        @Override
        public void inflate(View convertView) {
            super.inflate(convertView);

            mapView = (StaticMapView) convertView.findViewById(R.id.mapview_message_location);
            touchInterceptorView = convertView.findViewById(R.id.view_map_interceptor);
            locationDescriptionView = (TextView) convertView.findViewById(R.id.tv_location_message_description);
            convertView.setTag(this);

        }
    }

    private class FileMessageViewHolder extends BaseViewHolder {

        private Button downloadButton;
        private ProgressBar progressBarDownload;

        @Override
        protected void setMessage(final TattlerMessage message) {
            super.setMessage(message);

            downloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DownloadRequestFactory.download(context, message.getBody(), progressBarDownload, downloadManager);
                }
            });

        }

        @Override
        public void inflate(View convertView) {
            super.inflate(convertView);

            downloadButton = (Button) convertView.findViewById(R.id.btn_message_download_file);
            progressBarDownload = (ProgressBar) convertView.findViewById(R.id.pb_message_file);

            convertView.setTag(this);

        }
    }

    private class VideoMessageViewHolder extends BaseViewHolder {

        private ImageView videoThumbView;
        private ImageView playVideoView;
        private ProgressBar progressBarDownload;

        @Override
        protected void setMessage(final TattlerMessage message) {
            super.setMessage(message);

            playVideoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DownloadRequestFactory.download(context, message.getBody(), progressBarDownload, downloadManager);
                }
            });

        }

        @Override
        public void inflate(View convertView) {
            super.inflate(convertView);

            this.videoThumbView = (ImageView) convertView.findViewById(R.id.iv_message_video_thumbnail);
            this.playVideoView = (ImageView) convertView.findViewById(R.id.iv_play_message_video);
            this.progressBarDownload = (ProgressBar) convertView.findViewById(R.id.pb_message_video);
            convertView.setTag(this);

        }

    }

    private class PhotoMessageViewHolder extends BaseViewHolder {

        private static final int MAX_WIDTH = 1024;
        private static final int MAX_HEIGHT = 768;

        private final int SIZE = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

        ImageView photoView;
        ProgressBar progressBar;

        public void setMessage(final TattlerMessage message) {
            super.setMessage(message);

            String dataUri = message.getBody();

            picasso.load(dataUri)
                    .transform(new MessageBitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                    .skipMemoryCache()
                    .resize(SIZE, SIZE)
                    .centerInside()
                    .into(photoView);

            photoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DownloadRequestFactory.download(context, message.getBody(), progressBar, downloadManager);
                }
            });
        }

        @Override
        public void inflate(View convertView) {
            super.inflate(convertView);

            this.photoView = (ImageView) convertView.findViewById(R.id.iv_message_image);
            this.progressBar = (ProgressBar) convertView.findViewById(R.id.pb_message_image);
            convertView.setTag(this);
        }
    }

    private class PromotionMessageViewHolder extends BaseViewHolder {

        private final int SIZE = (int) Math.ceil(Math.sqrt(PhotoMessageViewHolder.MAX_WIDTH * PhotoMessageViewHolder.MAX_HEIGHT));

        ImageView promotionImageView;
        ProgressBar progressBar;
        TextView promotionTitle;

        public void setMessage(final TattlerMessage message) {
            super.setMessage(message);

            String json = message.getBody();

            Log.i(TAG, json);

            Promotion promotion = new Gson().fromJson(json, Promotion.class);
            final String imageUrl = promotion.getImage();

            picasso.load(imageUrl)
                    .transform(new MessageBitmapTransform(PhotoMessageViewHolder.MAX_WIDTH, PhotoMessageViewHolder.MAX_HEIGHT))
                    .skipMemoryCache()
                    .resize(SIZE, SIZE)
                    .centerInside()
                    .into(promotionImageView);

            promotionImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DownloadRequestFactory.download(context, imageUrl, progressBar, downloadManager);
                }
            });

            promotionTitle.setText(promotion.getTitle());

        }

        @Override
        public void inflate(View convertView) {
            super.inflate(convertView);

            this.promotionImageView = (ImageView) convertView.findViewById(R.id.iv_message_promotion_image);
            this.progressBar = (ProgressBar) convertView.findViewById(R.id.pb_message_promotion_image);
            this.promotionTitle = (TextView) convertView.findViewById(R.id.tv_message_promotion_title);
            convertView.setTag(this);
        }
    }

}