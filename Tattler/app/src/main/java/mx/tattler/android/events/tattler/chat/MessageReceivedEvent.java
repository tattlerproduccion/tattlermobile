package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.daos.TattlerMessage;

public class MessageReceivedEvent {

    public final TattlerMessage message;

    public MessageReceivedEvent(TattlerMessage message) {
        this.message = message;
    }

}
