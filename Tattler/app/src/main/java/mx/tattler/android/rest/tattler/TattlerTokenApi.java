package mx.tattler.android.rest.tattler;

import mx.tattler.android.rest.tattler.models.AccessResponse;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface TattlerTokenApi {

    @FormUrlEncoded
    @POST("/accounts/login")
    public AccessResponse refreshToken(@Field("refresh_token") String token, @Field("client_id") String clientId,
                                       @Field("client_secret") String clientSecret, @Field("grant_type") String granType);

}
