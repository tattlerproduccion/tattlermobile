package mx.tattler.android.adapters;

final class Data {

    static final String[] ITEMS = {
            "Pizzas y pastas",
            "Comida japonesa",
            "Comida mexicana",
            "Comida china"
    };

    private Data() {
    }
}