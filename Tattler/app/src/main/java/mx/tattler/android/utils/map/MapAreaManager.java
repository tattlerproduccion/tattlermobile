package mx.tattler.android.utils.map;

import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import mx.tattler.android.R;
import mx.tattler.android.utils.map.MapAreaWrapper.MarkerMoveResult;

/**
 * This class manages the map areas
 * <p/>
 * On long click on any empty area of the map, a new area will be created
 * <p/>
 * The areas can be moved or resized using markers
 *
 * @author ivanschuetz
 *         <p/>
 *         Based on functionality of Google APIs v19,
 *         com.example.mapdemo.CircleDemoActivity
 */
public class MapAreaManager implements OnMarkerDragListener,
        OnMapLongClickListener {

    public static float DEFAULT_MAP_AREA_RADIUS = 5000;

    private static int DEFAULT_FILL_COLOR = 0xff0000ff;
    private static int DEFAULT_STROKE_COLOR = 0xff000000;
    private static int DEFAULT_STROKE_WIDTH = 1;

    private MapAreaWrapper area;
    private GoogleMap map;

    private int fillColor = DEFAULT_FILL_COLOR;
    private int strokeWidth = DEFAULT_STROKE_WIDTH;
    private int strokeColor = DEFAULT_STROKE_COLOR;

    private int minRadiusMeters = 2000;
    private int maxRadiusMeters = 10000;

    private MapAreaMeasure initRadius;

    private CircleManagerListener circleManagerListener;

    private int moveDrawableId = -1;
    private int radiusDrawableId = -1;

    private float moveDrawableAnchorU;
    private float moveDrawableAnchorV;
    private float resizeDrawableAnchorU;
    private float resizeDrawableAnchorV;

    public static MapAreaManager newInstance(GoogleMap map, LatLng point, Double radius, CircleManagerListener listener) {

        return new MapAreaManager(map, point,
                4, Color.BLUE, Color.HSVToColor(70, new float[]{199, 86, 85}), // styling
                R.drawable.ic_btn_move, R.drawable.ic_btn_resize, // custom drawables for ic_btn_move
                // and ic_btn_resize icons
                0.5f, 0.5f, 0.5f, 0.5f, // sets anchor point of ic_btn_move / ic_btn_resize
                // drawable in the middle
                new MapAreaMeasure(radius, MapAreaMeasure.Unit.meters), // circle
                listener);

    }

    /**
     * Primary constructor
     *
     * @param map
     * @param point                 point for current area
     * @param strokeWidth           circle stroke with in pixels
     * @param strokeColor           circle stroke color
     * @param circleColor           circle fill color
     * @param moveDrawableId
     * @param moveDrawableId        drawable resource id for positioning marker. If not set a
     *                              default geomarker is used
     * @param resizeDrawableId      drawable resource id for resizing marker. If not set a
     *                              default geomarker is used
     * @param moveDrawableAnchorU   horizontal anchor for ic_btn_move drawable
     * @param moveDrawableAnchorV   vertical anchor for ic_btn_move drawable
     * @param resizeDrawableAnchorU horizontal anchor for ic_btn_resize drawable
     * @param resizeDrawableAnchorV vertical anchor for ic_btn_resize drawable
     * @param initRadius            init radius for all circles, currently supported pixels
     *                              (constant in all zoom levels) or meters
     * @param circleManagerListener listener for circle events
     */
    public MapAreaManager(GoogleMap map, LatLng point, int strokeWidth, int strokeColor,
                          int circleColor, int moveDrawableId, int resizeDrawableId,
                          float moveDrawableAnchorU, float moveDrawableAnchorV,
                          float resizeDrawableAnchorU, float resizeDrawableAnchorV,
                          MapAreaMeasure initRadius,
                          CircleManagerListener circleManagerListener) {

        this.map = map;
        this.circleManagerListener = circleManagerListener;

        this.strokeWidth = strokeWidth;
        this.strokeColor = strokeColor;
        this.fillColor = circleColor;

        this.moveDrawableId = moveDrawableId;
        this.radiusDrawableId = resizeDrawableId;

        this.moveDrawableAnchorU = moveDrawableAnchorU;
        this.moveDrawableAnchorV = moveDrawableAnchorV;
        this.resizeDrawableAnchorU = resizeDrawableAnchorU;
        this.resizeDrawableAnchorV = resizeDrawableAnchorV;

        this.initRadius = initRadius;

        map.setOnMarkerDragListener(this);
        map.setOnMapLongClickListener(this);

        //Init area
        this.area = new MapAreaWrapper(map, point,
                initRadius.value, strokeWidth, strokeColor, fillColor,
                minRadiusMeters, maxRadiusMeters, moveDrawableId,
                radiusDrawableId, moveDrawableAnchorU, moveDrawableAnchorV,
                resizeDrawableAnchorU, resizeDrawableAnchorV);

        circleManagerListener.onCreateCircle(area);
        area.setCenter(point);

    }

    /**
     * Convenience constructor
     * <p/>
     * Will pass -1 as ic_btn_move and ic_btn_resize drawable resource id, with means we will
     * use default geo markers
     *
     * @params see primary constructor
     */
    public MapAreaManager(GoogleMap map, LatLng point, int strokeWidth, int strokeColor,
                          int circleColor, MapAreaMeasure initRadius,
                          CircleManagerListener circleManagerListener) {

        this(map, point, strokeWidth, strokeColor, circleColor, -1, -1, initRadius,
                circleManagerListener);
    }

    /**
     * Convenience constructor
     * <p/>
     * Uses default values for marker's drawable anchors
     *
     * @params see primary constructor
     */
    public MapAreaManager(GoogleMap map, LatLng point, int strokeWidth, int strokeColor,
                          int circleColor, int moveDrawableId, int radiusDrawableId,
                          MapAreaMeasure initRadius,
                          CircleManagerListener circleManagerListener) {

        this(map, point, strokeWidth, strokeColor, circleColor, moveDrawableId,
                radiusDrawableId, 0.5f, 1f, 0.5f, 1f, initRadius,
                circleManagerListener);
    }

    /**
     * Set min radius in meters. The circles will shrink bellow this, and
     * onMinRadius will be called when reached
     *
     * @param minRadius
     */
    public void setMinRadius(int minRadius) {
        this.minRadiusMeters = minRadius;
    }

    /**
     * Set max radius in meters. The circles will expand above this, and
     * onMaxRadius will be called when reached
     *
     * @param maxRadius
     */
    public void setMaxRadius(int maxRadius) {
        this.maxRadiusMeters = maxRadius;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        MarkerMoveResultWithCircle result = onMarkerMoved(marker);
        switch (result.markerMoveResult) {
            case minRadius: {
                circleManagerListener.onMinRadius(result.draggableCircle);
                break;
            }
            case maxRadius: {
                circleManagerListener.onMaxRadius(result.draggableCircle);
                break;
            }
            case radiusChange: {
                circleManagerListener.onResizeCircleStart(result.draggableCircle);
                break;
            }
            case moved: {
                circleManagerListener.onMoveCircleStart(result.draggableCircle);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        MarkerMoveResultWithCircle result = onMarkerMoved(marker);
        switch (result.markerMoveResult) {
            case minRadius: {
                circleManagerListener.onMinRadius(result.draggableCircle);
                break;
            }
            case maxRadius: {
                circleManagerListener.onMaxRadius(result.draggableCircle);
                break;
            }
            case radiusChange: {
                circleManagerListener.onResizeCircleEnd(result.draggableCircle);
                break;
            }
            case moved: {
                circleManagerListener.onMoveCircleEnd(result.draggableCircle);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        MarkerMoveResultWithCircle result = onMarkerMoved(marker);
        switch (result.markerMoveResult) {
            case minRadius: {
                circleManagerListener.onMinRadius(result.draggableCircle);
                break;
            }
            case maxRadius: {
                circleManagerListener.onMaxRadius(result.draggableCircle);
                break;
            }
            default:
                break;
        }
    }

    public void moveArea(LatLng point, boolean notifyListener) {
        area.setCenter(point);

        if (notifyListener)
            circleManagerListener.onMoveCircleEnd(area);
    }

    public LatLng getCenter() {
        return area.getCenter();
    }

    public float getRadius() {
        return (float) area.getRadius();
    }

    /**
     * Wrapper for result of gesture with affected circle
     */
    private class MarkerMoveResultWithCircle {
        MarkerMoveResult markerMoveResult;
        MapAreaWrapper draggableCircle;

        public MarkerMoveResultWithCircle(MarkerMoveResult markerMoveResult,
                                          MapAreaWrapper draggableCircle) {
            this.markerMoveResult = markerMoveResult;
            this.draggableCircle = draggableCircle;
        }
    }

    /**
     * When marker is moved, notify circles The circle containing the marker
     * will execute necessary actions
     *
     * @param marker
     * @return
     */
    private MarkerMoveResultWithCircle onMarkerMoved(Marker marker) {
        MapAreaWrapper affectedDraggableCircle = null;

        MarkerMoveResult result = area.onMarkerMoved(marker);
        if (result != MarkerMoveResult.none) {
            affectedDraggableCircle = area;
        }

        return new MarkerMoveResultWithCircle(result, affectedDraggableCircle);
    }

    @Override
    public void onMapLongClick(LatLng point) {
        //moveArea(point, true);
    }

}
