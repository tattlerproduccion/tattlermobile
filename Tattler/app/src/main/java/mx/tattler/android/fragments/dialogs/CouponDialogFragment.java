package mx.tattler.android.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import org.parceler.Parcels;

import mx.tattler.android.R;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.RequestCouponEvent;
import mx.tattler.android.events.tattler.RequestCouponResultEvent;
import mx.tattler.android.rest.tattler.models.Promotion;
import mx.tattler.android.utils.text.DateTextHelper;

public class CouponDialogFragment extends AnimatedBusDialogFragment {

    private static final String TAG = CouponDialogFragment.class.getName();

    private Promotion promotion;

    private TextView promotionExpirationDate;
    private TextView couponCode;

    private LinearLayout container;
    private ProgressBar progressBar;

    private Button button;

    private boolean couponRequested = false;

    public static CouponDialogFragment newInstance(Promotion promotion) {
        CouponDialogFragment fragment = new CouponDialogFragment();
        Bundle args = new Bundle();
        Parcelable wrapped = Parcels.wrap(promotion);
        args.putParcelable("promotion", wrapped);
        fragment.setArguments(args);
        return fragment;
    }

    public CouponDialogFragment() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //String title = getArguments().getString("title");

        //List<Branch> branches = Parcels.unwrap(getIntent().getExtras().getParcelable("branches"));
        promotion = Parcels.unwrap(getArguments().getParcelable("promotion"));

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_fragment_coupon, null);

        progressBar = (ProgressBar) view.findViewById(R.id.pb_loading);
        container = (LinearLayout) view.findViewById(R.id.ly_container);

        TextView promotionTitle = (TextView) view.findViewById(R.id.tv_promotion_title);
        promotionExpirationDate = (TextView) view.findViewById(R.id.tv_promotion_expiration_date);
        couponCode = (TextView) view.findViewById(R.id.tv_coupon_code);
        TextView promotionBranches = (TextView) view.findViewById(R.id.tv_promotion_branches);

        promotionTitle.setText(promotion.getTitle());

        if(!TextUtils.isEmpty(promotion.getExpirationDate())){
            promotionExpirationDate.setText("Vigencia: " + DateTextHelper.formatToPromotionDate(promotion.getExpirationDate()));
        }

        promotionBranches.setText(promotion.describeBranches());

        alertDialogBuilder.setView(view);
        alertDialogBuilder.setNeutralButton("Solicitar cupón", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        //alertDialogBuilder.setTitle(title);
        //alertDialogBuilder.setMessage(R.string.promotion_detail_message);

        return alertDialogBuilder.create();
    }

    @Override
    public void onStart() {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        AlertDialog d = (AlertDialog) getDialog();
        if (d != null) {
            button = d.getButton(Dialog.BUTTON_NEUTRAL);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (couponRequested) {
                        dismiss();
                        return;
                    }

                    showProgress(true);
                    listener.onRequest().post(new RequestCouponEvent(promotion));

                    //Boolean wantToCloseDialog = false;
                    //Do stuff, possibly set wantToCloseDialog to true then...
                    //dismiss();
                    //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                }
            });
        }
    }

    @Subscribe
    public void onCouponRetreived(RequestCouponResultEvent event) {
        showProgress(false);
        couponCode.setText("Código Promoción\n" + event.coupon.getCouponCode());
        couponCode.setVisibility(View.VISIBLE);
        button.setText("Cerrar");
        couponRequested = true;
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {
        showProgress(false);
        Toast.makeText(getActivity(), "No hay cupones disponibles", Toast.LENGTH_LONG).show();
    }

    @Override
    protected View getContainerView() {
        return container;
    }

    @Override
    protected View getProgressView() {
        return progressBar;
    }


}
