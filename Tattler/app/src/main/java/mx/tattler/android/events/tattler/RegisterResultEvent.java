package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.User;

public class RegisterResultEvent {

    public final User user;
    public final String password;

    public RegisterResultEvent(User user, String password) {
        this.user = user;
        this.password = password;
    }
}
