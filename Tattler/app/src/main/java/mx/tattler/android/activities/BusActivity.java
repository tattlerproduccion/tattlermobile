package mx.tattler.android.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Bus;

import mx.tattler.android.BusProvider;
import mx.tattler.android.R;
import mx.tattler.android.fragments.listeners.OnBusRequestListener;

public abstract class BusActivity extends Activity implements OnBusRequestListener{

    private Bus mBus;

    private TextView titleView;
    private TextView subtitleView;
    private ImageView logo;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBus().unregister(this);
    }

    public void setBus(Bus bus) {
        mBus = bus;
    }

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    @Override
    public Bus onRequest() {
        return getBus();
    }

    protected void inflateCustomActionbar() throws Exception {

        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.actionbar_tattler, null);

        ImageView actionbarUp = (ImageView) mCustomView.findViewById(R.id.iv_actionbar_up);
        actionbarUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavUtils.navigateUpFromSameTask(BusActivity.this);
            }
        });

        titleView = (TextView) mCustomView.findViewById(R.id.tv_title);
        subtitleView = (TextView) mCustomView.findViewById(R.id.tv_subtitle);
        logo = (ImageView) mCustomView.findViewById(R.id.iv_tattler_actionbar_logo);

        ActionBar actionBar = getActionBar();

        if (actionBar == null) {
            throw new Exception("Actionbar no existe");
        }

        actionBar.setIcon(new ColorDrawable(getResources().getColor(R.color.tattler_transparent)));
        actionBar.setCustomView(mCustomView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

    }

    protected void setActionbarTitle(String title, String subtitle) {
        titleView.setText(title);

        if (TextUtils.isEmpty(subtitle)) {
            subtitleView.setVisibility(View.GONE);
            titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) titleView.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        } else {
            subtitleView.setText(subtitle);
        }

    }

    protected void setActionbarSubtitle(String subtitle) {
        subtitleView.setText(subtitle);
    }

    protected void hideTattlerActionbarLogo() {
        logo.setVisibility(View.GONE);
    }

    protected void setCustomLogo(int resource) {
        logo.setImageResource(resource);
        logo.setPadding(0, 0, 0, 0);
    }

    protected void askForGps() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        Toast.makeText(this, "Habilita GPS primero", Toast.LENGTH_SHORT).show();
        startActivity(intent);
    }

}
