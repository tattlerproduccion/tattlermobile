package mx.tattler.android.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.squareup.otto.Subscribe;

import org.parceler.Parcels;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.activities.chat.ConversationActivity;
import mx.tattler.android.adapters.chat.ConversationsAdapter;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.ConversationRepository;
import mx.tattler.android.daos.MessageRepository;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.daos.TattlerMessage;
import mx.tattler.android.events.tattler.chat.ConversationReadyToExportEvent;
import mx.tattler.android.events.tattler.chat.ConversationUpdatedEvent;
import mx.tattler.android.events.tattler.chat.ExportConversationEvent;
import mx.tattler.android.events.tattler.chat.GetConversationMessagesEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsResultEvent;
import mx.tattler.android.fragments.dialogs.ConversationDialogFragment;
import mx.tattler.android.rest.tattler.models.chat.Conversation;
import mx.tattler.android.widgets.SearchViewListener;
import mx.tattler.android.widgets.SearchViewStyler;

public class ConversationsFragment extends AnimatedBusFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, ConversationsAdapter.OnFilterListener {

    private static final String TAG = ConversationsFragment.class.getName();

    private View progressbarContainer;
    private View containerView;
    private ConversationsAdapter adapter;

    private ProgressDialog loadingDialog;
    private TextView emptyConversationsView;

    public ConversationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        //Request conversations from Tattler API

        List<TattlerConversation> conversationList = ConversationRepository.getAll(getActivity());
        adapter.setConversations(conversationList, false);

        listener.onRequest().post(new GetConversationsEvent());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_conversations, container, false);

        SearchView conversationSearchView = (SearchView) rootView.findViewById(R.id.sv_conversations);
        SearchViewStyler.apply(getActivity(), conversationSearchView);

        progressbarContainer = rootView.findViewById(R.id.pb_loading);
        containerView = rootView.findViewById(R.id.ly_container);

        adapter = new ConversationsAdapter(getActivity(), this);

        ListView listView = (ListView) rootView.findViewById(R.id.lv_conversations);

        emptyConversationsView = (TextView) rootView.findViewById(R.id.tv_empty_conversations);

        listView.setEmptyView(emptyConversationsView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        conversationSearchView.setIconifiedByDefault(false);
        conversationSearchView.setSubmitButtonEnabled(true);
        conversationSearchView.setOnQueryTextListener(new SearchViewListener(adapter.getFilter()));

        return rootView;

    }

    @Subscribe
    public void onConversations(GetConversationsResultEvent event) {

        Log.i(TAG, String.format("Conversaciones del ws: %d", event.conversations.size()));

        Collection<Conversation> putasconversaciones = Collections2.filter(event.conversations, new Predicate<Conversation>() {
            @Override
            public boolean apply(Conversation input) {
                return input.getLastMessage() != null;
            }
        });

        List<Conversation> conversations = new ArrayList<>(putasconversaciones);

        ConversationRepository.batchUpsert(getActivity(), conversations, new ConversationRepository.BatchUpsertCallback() {
            @Override
            public void afterTransaction(List<TattlerConversation> conversations) {
                Log.i(TAG, String.format("Conversaciones insertadas: %d", conversations.size()));

                adapter.setConversations(conversations, false);
            }
        });

    }

    @Subscribe
    public void onExportConversation(ExportConversationEvent event) {

        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadingDialog.setMessage("Generando archivo, espera un segundo...");
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.show();

        Conversation remote = new Conversation();
        remote.setId(event.conversation.getConversationId());

        listener.onRequest().post(new GetConversationMessagesEvent(remote, true));

    }

    @Subscribe
    public void onConversationReady(ConversationReadyToExportEvent event) {

        Log.i(TAG, "Conversación lista para exportar");

        TattlerConversation selected = ConversationRepository.getByConversationId(getActivity(), event.conversationId);

        if (selected == null) {
            Log.e(TAG, "Conversacion a exportar no existe!");
            return;
        }

        String SDpath = Environment.getExternalStorageDirectory().getPath();
        List<TattlerMessage> messages = MessageRepository.getAllForConversation(getActivity(), selected);

        StringBuilder builder = new StringBuilder();

        for (TattlerMessage message : messages) {
            builder.append(message.getSender());
            builder.append(": ");
            builder.append(message.getBody());
            builder.append("\n");
        }

        try {

            File file = new File(SDpath + File.separator + "Conversacion Tattler con " + selected.getReceiverName() + ".txt");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(builder.toString().getBytes());
            fos.close();
            Intent sendConversationToEmail = new Intent(Intent.ACTION_SEND);

            //sendConversationToEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{user_name});
            sendConversationToEmail.putExtra(Intent.EXTRA_SUBJECT, "Conversacion Tattler con " + selected.getReceiverName());
            sendConversationToEmail.putExtra(Intent.EXTRA_TEXT, "Historial de conversacion adjunto en este correo en el archivo " + "Conversacion Tattler con " + selected.getReceiverName() + ".txt ");
            sendConversationToEmail.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getAbsolutePath()));
            sendConversationToEmail.setType("message/rfc822");
            startActivityForResult(Intent.createChooser(sendConversationToEmail, "Selecciona un cliente de correo: "), 123);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        loadingDialog.dismiss();

    }

    @Subscribe
    public void onConversationUpdated(ConversationUpdatedEvent event){
        adapter.itemUpdated(event.conversation);
    }

    @Override
    protected View getContainerView() {
        return containerView;
    }

    @Override
    protected View getProgressView() {
        return progressbarContainer;
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {

        TattlerConversation conversation = adapter.getItem(position);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        ConversationDialogFragment alertDialog = ConversationDialogFragment.newInstance(conversation);
        alertDialog.show(fm, "conversation_dialog");

        return true;

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        TattlerConversation conversation = adapter.getItem(position);

        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        Bundle bundle = new Bundle();

        //TODO: Wrap conversation to target activity
        Parcelable wrapped = Parcels.wrap(conversation);
        bundle.putParcelable(ConversationConstants.CONVERSATION, wrapped);
        intent.putExtras(bundle);

        startActivity(intent);

    }

    @Override
    public void onEmptyResult() {
        emptyConversationsView.setText("No existen conversaciones para tu búsqueda");
    }

}
