package mx.tattler.android.chat;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;

import org.parceler.Parcels;

import mx.tattler.android.R;
import mx.tattler.android.activities.chat.ConversationActivity;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.daos.TattlerMessage;

public class ConversationNotificationFactory {

    private ConversationNotificationFactory(){}

    public static Notification create(Context context, TattlerMessage message, TattlerConversation conversation){

        Intent intent = new Intent(context, ConversationActivity.class);
        Bundle bundle = new Bundle();

        Parcelable wrapped = Parcels.wrap(conversation);
        bundle.putParcelable(ConversationConstants.CONVERSATION, wrapped);
        intent.putExtras(bundle);

        String title = "Tattler";
        String subtitle = "Nuevo mensaje de " + conversation.getReceiverName();

        PendingIntent activityIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri alarmsound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        long[] vibrationpattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.drawable.ic_stat_tattler_logo); // notification icon
        mBuilder.setSound(alarmsound);
        mBuilder.setContentTitle(title); // title for notification
        mBuilder.setContentText(subtitle); // message for notification
        mBuilder.setLights(Color.RED, 500, 500);
        mBuilder.setVibrate(vibrationpattern);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentIntent(activityIntent);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle("Tattler");

        // Moves events into the big view
        inboxStyle.addLine(message.getBody());

        mBuilder.setStyle(inboxStyle);

        return mBuilder.build();

    }

}
