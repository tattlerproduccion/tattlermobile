package mx.tattler.android.rest.tattler.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import mx.tattler.android.rest.tattler.models.Participant;

@Parcel
public class Conversation {

    @Expose
    private String id;

    @SerializedName("receiver")
    @Expose
    private Participant participant;

    @SerializedName("last_message")
    @Expose
    private Message lastMessage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public boolean hasSameReceiver(Integer receiverId){
        return this.participant.getMessagingId().equals(receiverId);
    }

    public Integer getReceiverId(){
        return this.participant.getMessagingId();
    }

    public Message getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Message lastMessage) {
        this.lastMessage = lastMessage;
    }
}
