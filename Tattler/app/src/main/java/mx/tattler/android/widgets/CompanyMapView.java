package mx.tattler.android.widgets;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.mapbox.mapboxsdk.overlay.UserLocationOverlay;
import com.mapbox.mapboxsdk.tileprovider.MapTileLayerBase;
import com.mapbox.mapboxsdk.views.MapView;

public class CompanyMapView extends MapView {

    public interface OnTouchListener{

        boolean isMapOpen();

        void expandMap();

        void closeMap();

    }

    private OnTouchListener listener;

    protected CompanyMapView(Context aContext, int tileSizePixels, MapTileLayerBase tileProvider, Handler tileRequestCompleteHandler, AttributeSet attrs) {
        super(aContext, tileSizePixels, tileProvider, tileRequestCompleteHandler, attrs);
        this.setEnabled(false);
        setCurrentLocation();
    }

    public CompanyMapView(Context aContext) {
        super(aContext);
        this.setEnabled(false);
        setCurrentLocation();
    }

    public CompanyMapView(Context aContext, AttributeSet attrs) {
        super(aContext, attrs);
        this.setEnabled(false);
        setCurrentLocation();
    }

    protected CompanyMapView(Context aContext, int tileSizePixels, MapTileLayerBase aTileProvider) {
        super(aContext, tileSizePixels, aTileProvider);
        this.setEnabled(false);
        setCurrentLocation();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(listener.isMapOpen()){

            return super.onTouchEvent(event);

        }else{

            listener.expandMap();
            return true;

        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        if(listener.isMapOpen()){

            return super.dispatchTouchEvent(event);

        }else{

            listener.expandMap();
            return true;

        }

    }

    public void setListener(OnTouchListener listener) {
        this.listener = listener;
    }

    private void setCurrentLocation(){

        // Setup UserLocation monitoring
        this.setUserLocationEnabled(true);
        this.setUserLocationTrackingMode(UserLocationOverlay.TrackingMode.NONE);

        //Retrieve user location
        this.getUserLocation();

    }

}
