package mx.tattler.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.rest.tattler.models.Contact;

public class FriendContactListAdapter extends BaseAdapter implements Filterable{

    private final Picasso picasso;

    private final LayoutInflater inflater;
    private List<Contact> friends = new ArrayList<Contact>();
    private List<Contact> filterList = new ArrayList<Contact>();
    private ValueFilter valueFilter;

    public FriendContactListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        picasso = Picasso.with(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder;
        if (view == null) {

            view = inflater.inflate(R.layout.item_list_contact_friend, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(R.id.tv_profile_name);
            holder.profileImage = (ImageView)view.findViewById(R.id.iv_profile_image);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        Contact contact = friends.get(position);
        holder.name.setText(String.format("%s %s", contact.getName(),contact.getLastName()));

        picasso.load(contact.getProfileImage())
                .fit()
                .into(holder.profileImage);

        return view;

    }

    @Override
    public int getCount() {
        return friends.size();
    }

    @Override
    public Contact getItem(int position) {
        return friends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    static class ViewHolder {
        TextView name;
        ImageView profileImage;
    }

    public void setData(List<Contact> friends) {
        this.friends = friends;
        this.filterList = friends;
        notifyDataSetChanged();
    }

    private class ValueFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                List<Contact> filterList = new ArrayList<Contact>();

                for (Contact one : filterList) {

                    if ( (one.getName().toUpperCase() )
                            .contains(constraint.toString().toUpperCase())) {

                        Contact contact = new Contact();

                        contact.setId(one.getId());
                        contact.setEmail(one.getEmail());
                        contact.setName(one.getName());
                        contact.setLastName(one.getLastName());
                        contact.setProfileImage(one.getProfileImage());

                        filterList.add(contact);

                    }
                }

                results.count = filterList.size();
                results.values = filterList;

            } else {

                results.count = filterList.size();
                results.values = filterList;

            }

            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            friends = (List<Contact>) results.values;
            notifyDataSetChanged();
        }

    }

}