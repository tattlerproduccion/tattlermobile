package mx.tattler.android.events.tattler.profile;

import mx.tattler.android.rest.tattler.models.User;

/**
 * Created by irving on 23/02/15.
 */
public class InitialCurrentUserResultEvent {

    public final User user;

    public InitialCurrentUserResultEvent(User user) {
        this.user = user;
    }

}
