package mx.tattler.android.widgets;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;

import mx.tattler.android.R;

public class SearchViewStyler {

    public static void apply(Context context, SearchView searchView){

        int backgroundId = context.getResources().getIdentifier("android:id/search_bar", null, null);
        int searchSrcTextId = context.getResources().getIdentifier("android:id/search_src_text", null, null);
        int submitAreaId = context.getResources().getIdentifier("android:id/submit_area", null, null);

        int searchButtonId = context.getResources().getIdentifier("search_button", "id", "android");

        LinearLayout backgroundLayout = (LinearLayout)searchView.findViewById(backgroundId);
        backgroundLayout.setBackgroundColor(context.getResources().getColor(R.color.tattlertheme_color));

        EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
        searchEditText.setTextColor(Color.WHITE);
        searchEditText.setHintTextColor(Color.WHITE);

        LinearLayout submitLayout = (LinearLayout)searchView.findViewById(submitAreaId);
        submitLayout.setVisibility(View.GONE);

        ImageView searchButton = (ImageView)searchView.findViewById(searchButtonId);
        searchButton.setImageResource(R.drawable.ic_action_search);

    }

}
