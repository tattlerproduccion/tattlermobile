package mx.tattler.android.events.tattler.chat;

import mx.tattler.android.rest.tattler.models.chat.QbTattlerToken;

/**
 * Created by Irving on 18/04/2015.
 */
public class RequestQbTokenResultEvent {

    public final QbTattlerToken token;

    public RequestQbTokenResultEvent(QbTattlerToken token) {
        this.token = token;
    }

}
