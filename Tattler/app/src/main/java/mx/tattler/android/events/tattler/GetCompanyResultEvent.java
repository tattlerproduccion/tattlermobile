package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.models.Company;

/**
 * Created by irving on 11/11/14.
 */
public class GetCompanyResultEvent {

    public Company company;

    public GetCompanyResultEvent(Company company) {
        this.company = company;
    }
}
