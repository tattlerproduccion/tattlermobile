package mx.tattler.android.fragments.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

/**
 * A page asking for a name and an email.
 */
public class ProfileInfoPage extends Page {

    public static final String FIRST_NAME_DATA_KEY = "first_name";
    public static final String LAST_NAME_DATA_KEY = "last_name";
    public static final String BIRTHDATE_DATA_KEY = "birthday";

    public ProfileInfoPage(ModelCallbacks callbacks, String title) {
        super(callbacks, title);
    }

    @Override
    public Fragment createFragment() {
        return ProfileInfoFragment.create(getKey());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem("Nombre", mData.getString(FIRST_NAME_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Apellido", mData.getString(LAST_NAME_DATA_KEY), getKey(), -1));
        dest.add(new ReviewItem("Cumpleaños", mData.getString(BIRTHDATE_DATA_KEY), getKey(), -1));
    }

    @Override
    public boolean isCompleted() {
        return !TextUtils.isEmpty(mData.getString(FIRST_NAME_DATA_KEY)) && !TextUtils.isEmpty(mData.getString(LAST_NAME_DATA_KEY));
    }
}