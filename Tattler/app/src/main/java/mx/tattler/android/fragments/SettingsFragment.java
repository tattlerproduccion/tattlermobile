package mx.tattler.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import mx.tattler.android.R;
import mx.tattler.android.TattlerApplication;
import mx.tattler.android.activities.settings.EditProfileActivity;
import mx.tattler.android.activities.settings.EditProfileImageActivity;
import mx.tattler.android.activities.settings.ShareActivity;
import mx.tattler.android.utils.ApplicationStateHelper;

public class SettingsFragment extends BusFragment {

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        Button closeSessionBtn = (Button) rootView.findViewById(R.id.btn_close_session);
        closeSessionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((TattlerApplication) getActivity().getApplication()).clearSession();
                ApplicationStateHelper.doRestart(getActivity());
            }
        });

        Button editProfileImgBtn = (Button) rootView.findViewById(R.id.btn_edit_profile_image);
        editProfileImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileImageActivity.class);
                startActivity(intent);
            }
        });

        Button shareButton = (Button)rootView.findViewById(R.id.btn_share_with_others);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ShareActivity.class);
                startActivity(intent);
            }
        });

        Button editProfileButton = (Button)rootView.findViewById(R.id.btn_edit_profile);
        editProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

}