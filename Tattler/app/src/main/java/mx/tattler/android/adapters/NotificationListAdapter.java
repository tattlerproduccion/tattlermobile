package mx.tattler.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.rest.tattler.models.Notification;

public class NotificationListAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final Picasso picasso;
    private List<Notification> notifications = new ArrayList<Notification>();

    public NotificationListAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        picasso = Picasso.with(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        Notification notification = notifications.get(position);
        int layout = getNotificationLayout(notification.getType());

        ViewHolder holder;
        if (view == null) {

            view = inflater.inflate(layout, parent, false);
            holder = new ViewHolder();

            holder.profileImage = (ImageView) view.findViewById(R.id.iv_profile_image);
            holder.name = (TextView) view.findViewById(R.id.tv_profile_name);
            holder.description = (TextView) view.findViewById(R.id.tv_notification_message);
            holder.createdAt = (TextView) view.findViewById(R.id.tv_notification_created_at);

            if(layout == R.layout.item_list_notification_base)
                holder.promotionImage = (ImageView) view.findViewById(R.id.iv_promotion_image);
            else
                holder.callElapsedTime = (TextView) view.findViewById(R.id.tv_call_elapsed_time);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.name.setText(notification.getProfileName());
        holder.createdAt.setText(notification.getCreatedAt());

        int description = getTypeDescription(notification.getType());
        holder.description.setText(description);

        if(layout == R.layout.item_list_notification_base){

            if(notification.isSharedPromotion()){
                picasso.load(notification.getImage())
                        .fit()
                        .into(holder.promotionImage);
            }else{
                holder.promotionImage.setVisibility(View.GONE);
            }

        }else{
            holder.callElapsedTime.setText(notification.getCallElapsedTime());
        }

        return view;

    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public Notification getItem(int position) {
        return notifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private int getNotificationLayout(String type) {
        return type.equalsIgnoreCase("finished_call") ? R.layout.item_list_notification_finished_call : R.layout.item_list_notification_base;
    }

    private int getTypeDescription(String type) {

        int description = R.string.business_description;

        if (type.equalsIgnoreCase("frienship_request")) {
            description = R.string.friendship_request;
        } else if (type.equalsIgnoreCase("missed_call")) {
            description = R.string.missed_call;
        } else if (type.equalsIgnoreCase("shared_promotion")) {
            description = R.string.promotion_shared;
        } else if (type.equalsIgnoreCase("finished_call")) {
            description = R.string.finished_call;
        } else if (type.equalsIgnoreCase("friendship_accepted")) {
            description = R.string.friendship_accepted;
        }

        return description;

    }

    public void setData(List<Notification> notifications) {
        this.notifications = notifications;
        notifyDataSetChanged();
    }

    static class ViewHolder {

        ImageView profileImage;
        TextView name;
        TextView description;
        TextView createdAt;
        ImageView promotionImage;
        TextView callElapsedTime;

    }

}