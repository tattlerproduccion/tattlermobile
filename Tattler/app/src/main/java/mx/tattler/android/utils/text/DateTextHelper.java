package mx.tattler.android.utils.text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTextHelper {

    private static String[] MONTHS = {"ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"};


    //private static  DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern();
    //private static  DateTimeFormatter CHAT_HOUR_DATE_FORMAT = DateTimeFormat.forPattern().withZone(DateTimeZone.getDefault());

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    private final static SimpleDateFormat SENT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.ENGLISH);
    private final static SimpleDateFormat CHAT_HOUR_DATE_FORMAT = new SimpleDateFormat("h:mm a");
    private final static SimpleDateFormat CHAT_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy h:mm a");
    private final static SimpleDateFormat PROMOTION_DATE_FORMAT = new SimpleDateFormat("dd MMMMMMMMMMMMMMMMMMMMMM yyyy");

    private final static SimpleDateFormat PROFILE_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");

    private static DateFormat PROFILE_VISIBLE_DATE_FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);

    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
        SENT_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
        CHAT_HOUR_DATE_FORMAT.setTimeZone(TimeZone.getDefault());
        CHAT_DATE_FORMAT.setTimeZone(TimeZone.getDefault());
        PROMOTION_DATE_FORMAT.setTimeZone(TimeZone.getDefault());
        PROFILE_DATE_FORMAT.setTimeZone(TimeZone.getDefault());
        PROFILE_VISIBLE_DATE_FORMAT.setTimeZone(TimeZone.getDefault());
    }

    public static String formatToChatHeader(Date date) {

        String output = "";
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

        try {

            //Date parsed = DATE_FORMAT.parse(rawDate);
            calendar.setTime(date);
            //calendar.setTimeZone(TimeZone.getDefault());
            output = calendar.get(Calendar.DAY_OF_MONTH) + " DE " + MONTHS[calendar.get(Calendar.MONTH)] + " DE " + calendar.get(Calendar.YEAR);

        } catch (Exception ignored) {

        }

        return output;

    }

    public static String formatMessageDate(Date date) {
        return SENT_DATE_FORMAT.format(date);
    }

    public static String formatMessageDateToChatHour(Date date) {
        return CHAT_HOUR_DATE_FORMAT.format(date);
    }

    public static String formatToChatDate(Date date) {
        return CHAT_DATE_FORMAT.format(date);
    }

    public static String formatToPromotionDate(String rawDateTime) {

        Date parsed;

        try {
            parsed = DATE_FORMAT.parse(rawDateTime);
        } catch (Exception e) {
            return "";
        }

        return PROMOTION_DATE_FORMAT.format(parsed);

    }

    public static Date toProfileDate(String rawDateTime) {

        Date parsed = new Date();

        try {
            parsed = DATE_FORMAT.parse(rawDateTime);
        } catch (Exception ignored) {
        }

        return parsed;

    }

    public static String formatToProfile(Date date) {
        return DATE_FORMAT.format(date);
    }

    public static String formatVisibleProfileDate(Date date) {
        return PROFILE_VISIBLE_DATE_FORMAT.format(date);
    }

    public static String formatToWizardDate(Date date) {
        return PROFILE_DATE_FORMAT.format(date);
    }

    public static String formatFromWizardDate(String rawDate) throws ParseException {
        return DATE_FORMAT.format(PROFILE_DATE_FORMAT.parse(rawDate));
    }

    public static Date parseProfileDate(String rawDate) throws ParseException {
        return PROFILE_DATE_FORMAT.parse(rawDate);
    }

    public static Date parseMessageDate(String sentAt){

        Date parsed;

        try {
            parsed = SENT_DATE_FORMAT.parse(sentAt);
        } catch (ParseException e) {
            parsed = new Date();
        }

        return parsed;

    }

    public static String nowChatHeaderFormat() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_MONTH) + " DE " + MONTHS[calendar.get(Calendar.MONTH)] + " DE " + calendar.get(Calendar.YEAR);
    }

    private DateTextHelper() {
    }

}