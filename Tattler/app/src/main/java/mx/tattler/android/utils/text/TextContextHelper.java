package mx.tattler.android.utils.text;

import android.text.TextUtils;

public class TextContextHelper {

    private TextContextHelper(){}

    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isNotEmpty(CharSequence target) {
        return !TextUtils.isEmpty(target);
    }

    public static boolean passwordMatches(String password, String passwordConfirmation){

        if (password == null || passwordConfirmation == null)
            return false;

        return password.equals(passwordConfirmation);

    }

}
