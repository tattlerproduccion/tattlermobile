package mx.tattler.android.activities.wizard;

import android.content.Context;

import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.PageList;

import mx.tattler.android.fragments.pages.CustomImagePage;
import mx.tattler.android.fragments.pages.ProfileGenderPage;
import mx.tattler.android.fragments.pages.ProfileInfoPage;

public class ProfileWizardModel extends AbstractWizardModel {

    private static final String[] genre = {"Hombre", "Mujer"};

    public ProfileWizardModel(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return
                new PageList(
                        new ProfileInfoPage(this, "¿Cómo quieres que te vean?").setRequired(true),
                        new ProfileGenderPage(this, "Sexo"),
                        new CustomImagePage(this, "Agrega tu foto")
                );
    }
}