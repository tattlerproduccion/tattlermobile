package mx.tattler.android.events.tattler;

import java.util.List;

import mx.tattler.android.rest.tattler.models.Promotion;

public class GetPromotionsForCompanyResultEvent {

    public List<Promotion> promotions;

    public GetPromotionsForCompanyResultEvent(List<Promotion> promotions) {
        this.promotions = promotions;
    }
}
