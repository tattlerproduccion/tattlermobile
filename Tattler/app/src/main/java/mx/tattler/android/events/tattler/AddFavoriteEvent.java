package mx.tattler.android.events.tattler;

public class AddFavoriteEvent {

    public final String companyId;

    public AddFavoriteEvent(String companyId) {
        this.companyId = companyId;
    }
}
