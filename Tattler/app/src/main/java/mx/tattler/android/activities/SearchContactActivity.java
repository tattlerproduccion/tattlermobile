package mx.tattler.android.activities;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import mx.tattler.android.R;

public class SearchContactActivity extends FragmentActivity {

    ActionBar.Tab contactTab, shareTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contact);

        ActionBar actionBar = getActionBar();

        // Specify that tabs should be displayed in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        contactTab = actionBar.newTab().setText("Buscar en Tattler");
        shareTab = actionBar.newTab().setText("Invitar Amigos");

        //TODO: Migrate compat fragments to actual fragments
        //contactTab.setTabListener(new FragmentTabListener(new ContactListFragment()));
        //shareTab.setTabListener(new FragmentTabListener(new ShareWithOthersFragment()));

        actionBar.addTab(contactTab);
        actionBar.addTab(shareTab);

    }

}
