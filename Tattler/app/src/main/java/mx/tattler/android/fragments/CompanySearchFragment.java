package mx.tattler.android.fragments;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.squareup.otto.Subscribe;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.tattler.android.R;
import mx.tattler.android.activities.EditSearchAreaActivity;
import mx.tattler.android.adapters.CompanyListAdapter;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.AllCompaniesResultEvent;
import mx.tattler.android.events.tattler.SearchCompaniesEvent;
import mx.tattler.android.utils.map.MapAreaManager;
import mx.tattler.android.widgets.MapCircleBoundaryOverlay;
import mx.tattler.android.widgets.StaticMapView;

public class CompanySearchFragment extends AnimatedBusFragment implements SearchView.OnQueryTextListener {

    private static final String TAG = CompanySearchFragment.class.getName();

    public static final int LOCATION_AREA_REQUEST = 230;  // The request code

    private View progressbarContainer;
    private View containerView;

    private StaticMapView map;
    private CompanyListAdapter adapter;
    private Button btnChangeLocation;
    private SearchView searchView;

    private Double latitude;
    private Double longitude;
    private float radius;
    private ListView listView;

    public CompanySearchFragment() {
        // Required empty public constructor
    }

    @Subscribe
    public void onCompaniesLoaded(AllCompaniesResultEvent event) {
        adapter.setCompanies(event.companies);
        listView.post(new Runnable() {
            @Override
            public void run() {
                showProgress(false);
            }
        });
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {
        Log.e(TAG, event.error.getRawBody());
        showProgress(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_company_search, container, false);

        progressbarContainer = rootView.findViewById(R.id.pb_loading);
        containerView = rootView.findViewById(R.id.ly_container);

        btnChangeLocation = (Button) rootView.findViewById(R.id.btn_change_search_area);
        btnChangeLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditSearchAreaActivity.class);
                startActivityForResult(intent, LOCATION_AREA_REQUEST);
            }
        });

        adapter = new CompanyListAdapter(getActivity());

        listView = (ListView) rootView.findViewById(R.id.lv_business_result);
        listView.setEmptyView(rootView.findViewById(R.id.tv_empty_company_search));
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(adapter);

        map = (StaticMapView) rootView.findViewById(R.id.mapview);

        setMapLocation();

        searchView = (SearchView) rootView.findViewById(R.id.sv_company_search);
        searchView.setOnQueryTextListener(this);

        doSearch("", true);

        return rootView;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOCATION_AREA_REQUEST && resultCode == Activity.RESULT_OK) {
            Log.i(TAG, "returning to company search fragment");
            setMapLocation();
            String queryName = searchView.getQuery().toString();
            doSearch(queryName, true);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        doSearch(query, false);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        if(TextUtils.isEmpty(query)){
            doSearch(query, false);
            return true;
        }
        return false;
    }

    @Override
    protected View getContainerView() {
        return containerView;
    }

    @Override
    protected View getProgressView() {
        return progressbarContainer;
    }

    private void setMapLocation() {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        latitude = Double.parseDouble(preferences.getString(getString(R.string.latitude), getString(R.string.default_latitude)));
        longitude = Double.parseDouble(preferences.getString(getString(R.string.longitude), getString(R.string.default_longitude)));
        radius = preferences.getFloat(getString(R.string.radius), MapAreaManager.DEFAULT_MAP_AREA_RADIUS);

        //Log.i(TAG, "radius: %.2f", radius);

        float displayRadius = radius / 1000;
        String locality = preferences.getString(getString(R.string.locality), "Mérida");

        btnChangeLocation.setText(String.format("%s - %.2f Km", locality, displayRadius));

        LatLng point = new LatLng(latitude, longitude);
        map.setCenter(point);

        map.setZoom(getZoomForRadius(radius));

        //Add area circle to map
        MapCircleBoundaryOverlay overlay = new MapCircleBoundaryOverlay();
        map.addOverlay(overlay);

    }

    private int getZoomForRadius(double radius) {

        int zoom;

        if (radius < 3000)
            zoom = 15;
        else if (radius > 3000 && radius < 5000)
            zoom = 15;
        else if (radius > 5000 && radius < 7000)
            zoom = 14;
        else if (radius > 7000 && radius < 10000)
            zoom = 13;
        else
            zoom = 12;

        return zoom;

    }

    private void doSearch(String queryName, boolean withLocation) {

        Map<String, String> params = new LinkedHashMap<>();

        if(withLocation){
            params.put("longitude", String.valueOf(longitude));
            params.put("latitude", String.valueOf(latitude));
            params.put("distance", String.valueOf(radius));
        }

        if (!TextUtils.isEmpty(queryName))
            params.put("name", queryName);

        showProgress(true);
        listener.onRequest().post(new SearchCompaniesEvent(params));

    }

}