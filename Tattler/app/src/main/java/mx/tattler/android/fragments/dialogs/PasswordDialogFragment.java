package mx.tattler.android.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.chat.UserCredentials;
import mx.tattler.android.events.tattler.LoginEvent;
import mx.tattler.android.events.tattler.recover.UpdatePasswordErrorEvent;
import mx.tattler.android.events.tattler.recover.UpdatePasswordEvent;
import mx.tattler.android.events.tattler.recover.UpdatePasswordResultEvent;
import mx.tattler.android.rest.tattler.models.RecoveryRequest;
import mx.tattler.android.utils.text.TextContextHelper;

public class PasswordDialogFragment extends AnimatedBusDialogFragment {

    private static final String TAG = PasswordDialogFragment.class.getName();

    private AlertDialog self;
    private String email;

    private LinearLayout container;
    private ProgressBar progressBar;

    private EditText newPasswordInput;
    private EditText newPasswordConfirmInput;

    public static PasswordDialogFragment newInstance(String email) {
        PasswordDialogFragment fragment = new PasswordDialogFragment();
        Bundle args = new Bundle();
        args.putString("email", email);
        fragment.setArguments(args);
        return fragment;
    }

    public PasswordDialogFragment() {

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        email = getArguments().getString("email");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_fragment_password, null);

        progressBar = (ProgressBar) view.findViewById(R.id.pb_loading);
        container = (LinearLayout) view.findViewById(R.id.ly_container);

        newPasswordInput = (EditText) view.findViewById(R.id.et_new_password);
        newPasswordConfirmInput = (EditText) view.findViewById(R.id.et_new_password_confirmation);

        alertDialogBuilder.setView(view);
        alertDialogBuilder.setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alertDialogBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        //alertDialogBuilder.setTitle(title);
        //alertDialogBuilder.setMessage(R.string.promotion_detail_message);

        // Show soft keyboard automatically
        newPasswordInput.requestFocus();
        return alertDialogBuilder.create();
    }

    @Override
    public void onStart() {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point

        this.self = (AlertDialog) getDialog();
        if (self == null)
            return;

        final Button button = self.getButton(Dialog.BUTTON_POSITIVE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newPassword = newPasswordInput.getText().toString();
                String newPasswordConfirm = newPasswordConfirmInput.getText().toString();

                if(TextUtils.isEmpty(newPassword)){
                    newPasswordInput.setError("Introduce nueva contraseña");
                    return;
                }

                if(TextUtils.isEmpty(newPasswordConfirm)){
                    newPasswordConfirmInput.setError("Introduce nueva contraseña");
                    return;
                }

                if(!TextContextHelper.passwordMatches(newPassword, newPasswordConfirm)){
                    newPasswordInput.setError("Contraseñas no coinciden");
                    newPasswordConfirmInput.setError("Contraseñas no coinciden");
                    return;
                }

                showProgress(true);

                Button cancelButton = self.getButton(Dialog.BUTTON_NEGATIVE);
                cancelButton.setVisibility(View.GONE);

                button.setVisibility(View.GONE);

                RecoveryRequest request = new RecoveryRequest();
                request.setNewPassword(newPassword);
                request.setPasswordConfirmation(newPassword);

                listener.onRequest().post(new UpdatePasswordEvent(request));

                //listener.onRequest().post(new UpdateProfilePasswordEvent(newPassword));
                //dismiss();

                //Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                //dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            }
        });

        self.setCanceledOnTouchOutside(false);

        self.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Subscribe
    public void onPasswordError(UpdatePasswordErrorEvent event){

        Button cancelButton = self.getButton(Dialog.BUTTON_NEGATIVE);
        cancelButton.setVisibility(View.VISIBLE);

        Button okButton = self.getButton(Dialog.BUTTON_POSITIVE);
        okButton.setVisibility(View.VISIBLE);

        String error = event.error.getRawBody();

        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        showProgress(false);

    }

    @Subscribe
    public void onPasswordUpdated(UpdatePasswordResultEvent event){

        Toast.makeText(getActivity(), "Contraseña actualizada", Toast.LENGTH_LONG).show();

        String password = newPasswordInput.getText().toString();

        UserCredentials credentials = UserCredentials.instance(email, password, "");
        listener.onRequest().post(new LoginEvent(credentials));

        self.dismiss();

    }

    @Override
    protected View getContainerView() {
        return container;
    }

    @Override
    protected View getProgressView() {
        return progressBar;
    }


}
