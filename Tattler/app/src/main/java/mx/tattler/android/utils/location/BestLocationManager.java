package mx.tattler.android.utils.location;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import java.util.List;

public class BestLocationManager {

    public interface LocationManagerCallback {
        void onLocation(Location location);

        void onGpsNotEnabled();

        void requestingLocation();

    }

    public static void getLastKnownLocation(Context context, final LocationManagerCallback callback) {

        final LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        boolean enabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled)
            callback.onGpsNotEnabled();

        Criteria criteria = new Criteria();
        String provider = mLocationManager.getBestProvider(criteria, false);
        Location location = mLocationManager.getLastKnownLocation(provider);

        if (location != null && location.getLatitude() != 0) {
            callback.onLocation(location);
            return;
        }

        LocationListener listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLocationManager.removeUpdates(this);
                callback.onLocation(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        mLocationManager.requestLocationUpdates(provider, 0, 0, listener);
        callback.requestingLocation();

    }

}