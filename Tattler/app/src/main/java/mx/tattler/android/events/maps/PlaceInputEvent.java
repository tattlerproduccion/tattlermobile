package mx.tattler.android.events.maps;

public class PlaceInputEvent {

    public final String input;
    public final String countryCode;

    public PlaceInputEvent(String input, String countryCode){
        this.input = input;
        this.countryCode = countryCode;
    }

}