package mx.tattler.android.events.tattler.chat;

import java.util.List;

import mx.tattler.android.rest.tattler.models.chat.Message;

public class GetConversationMessagesResultEvent {

    public final List<Message> messages;
    public final boolean toExport;

    public GetConversationMessagesResultEvent(List<Message> messages, boolean toExport) {
        this.messages = messages;
        this.toExport = toExport;
    }

}
