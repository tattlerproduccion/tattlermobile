package mx.tattler.android.constants;

public class SettingsConstants {

    public static final String CALL_LIMITATION_ACKNOWLEDGE = "CALL_LIMITATION_ACKNOWLEDGE";

    private SettingsConstants(){}

}
