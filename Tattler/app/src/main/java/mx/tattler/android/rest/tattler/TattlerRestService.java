package mx.tattler.android.rest.tattler;

import android.util.Log;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.AddFavoriteEvent;
import mx.tattler.android.events.tattler.AddFavoriteResultEvent;
import mx.tattler.android.events.tattler.AllCompaniesResultEvent;
import mx.tattler.android.events.tattler.AllContactsEvent;
import mx.tattler.android.events.tattler.AllContactsResultEvent;
import mx.tattler.android.events.tattler.AllNotificationsEvent;
import mx.tattler.android.events.tattler.AllNotificationsResultEvent;
import mx.tattler.android.events.tattler.CurrentUserEvent;
import mx.tattler.android.events.tattler.CurrentUserResultEvent;
import mx.tattler.android.events.tattler.DeleteFavoriteEvent;
import mx.tattler.android.events.tattler.DeleteFavoriteResultEvent;
import mx.tattler.android.events.tattler.GetBranchesForCompanyEvent;
import mx.tattler.android.events.tattler.GetBranchesForCompanyResultEvent;
import mx.tattler.android.events.tattler.GetCompanyEvent;
import mx.tattler.android.events.tattler.GetCompanyResultEvent;
import mx.tattler.android.events.tattler.GetContactContactsEvent;
import mx.tattler.android.events.tattler.GetContactContactsResultEvent;
import mx.tattler.android.events.tattler.GetFavoritesEvent;
import mx.tattler.android.events.tattler.GetFavoritesResultEvent;
import mx.tattler.android.events.tattler.GetPromotionEvent;
import mx.tattler.android.events.tattler.GetPromotionResultEvent;
import mx.tattler.android.events.tattler.GetPromotionsForCompanyEvent;
import mx.tattler.android.events.tattler.GetPromotionsForCompanyResultEvent;
import mx.tattler.android.events.tattler.LoginEvent;
import mx.tattler.android.events.tattler.LoginResultEvent;
import mx.tattler.android.events.tattler.RegisterEvent;
import mx.tattler.android.events.tattler.RegisterResultEvent;
import mx.tattler.android.events.tattler.RequestCouponEvent;
import mx.tattler.android.events.tattler.RequestCouponResultEvent;
import mx.tattler.android.events.tattler.SearchCompaniesEvent;
import mx.tattler.android.events.tattler.chat.GetConversationMessagesEvent;
import mx.tattler.android.events.tattler.chat.GetConversationMessagesResultEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsEvent;
import mx.tattler.android.events.tattler.chat.GetConversationsResultEvent;
import mx.tattler.android.events.tattler.chat.RequestQbTokenEvent;
import mx.tattler.android.events.tattler.chat.RequestQbTokenResultEvent;
import mx.tattler.android.events.tattler.chat.SendTTMessageEvent;
import mx.tattler.android.events.tattler.chat.TTMessageSentEvent;
import mx.tattler.android.events.tattler.chat.UploadAttachmentErrorEvent;
import mx.tattler.android.events.tattler.profile.InitialCurrentUserEvent;
import mx.tattler.android.events.tattler.profile.InitialCurrentUserResultEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileImageEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileImageResultEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileResultEvent;
import mx.tattler.android.events.tattler.recover.QueryUserEvent;
import mx.tattler.android.events.tattler.recover.RecoverEmailSentEvent;
import mx.tattler.android.events.tattler.recover.SendRecoverEmailEvent;
import mx.tattler.android.events.tattler.recover.UpdatePasswordErrorEvent;
import mx.tattler.android.events.tattler.recover.UpdatePasswordEvent;
import mx.tattler.android.events.tattler.recover.UpdatePasswordResultEvent;
import mx.tattler.android.events.tattler.recover.UserFoundEvent;
import mx.tattler.android.events.tattler.recover.UserNotFoundEvent;
import mx.tattler.android.rest.tattler.models.AccessResponse;
import mx.tattler.android.rest.tattler.models.Branch;
import mx.tattler.android.rest.tattler.models.Company;
import mx.tattler.android.rest.tattler.models.Contact;
import mx.tattler.android.rest.tattler.models.Coupon;
import mx.tattler.android.rest.tattler.models.FavoriteCompany;
import mx.tattler.android.rest.tattler.models.Notification;
import mx.tattler.android.rest.tattler.models.Promotion;
import mx.tattler.android.rest.tattler.models.UploadFileResponse;
import mx.tattler.android.rest.tattler.models.User;
import mx.tattler.android.rest.tattler.models.VisitCompanyPromotionsEvent;
import mx.tattler.android.rest.tattler.models.chat.Conversation;
import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.rest.tattler.models.chat.QbTattlerToken;
import mx.tattler.android.utils.resource.TypedFileFromBytes;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TattlerRestService {

    private static final String TAG = TattlerRestService.class.getName();

    private TattlerApi api;
    private Bus bus;

    public TattlerRestService(TattlerApi api, Bus bus) {
        this.api = api;
        this.bus = bus;
    }

    public void setApi(TattlerApi api) {
        this.api = api;
    }

    /*
    Chat
     */
    @Subscribe
    public void onConversations(GetConversationsEvent event) {

        Log.e(TAG, "onConversations");

        api.conversations(new Callback<List<Conversation>>() {
            @Override
            public void success(List<Conversation> conversations, Response response) {
                bus.post(new GetConversationsResultEvent(conversations));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });
    }

    @Subscribe
    public void onConversationMessages(final GetConversationMessagesEvent event) {

        Log.e(TAG, "onConversationMessages");

        api.conversationMessages(event.conversation.getId(), new Callback<List<Message>>() {
            @Override
            public void success(List<Message> messages, Response response) {
                bus.post(new GetConversationMessagesResultEvent(messages, event.toExport));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onSendTattlerMessage(final SendTTMessageEvent event) {

        //TODO: Check if tt message has uri, so upload attach first, the assign url to body and sent message.
        final Message remoteMessage = event.message;
        TypedFileFromBytes attachment = event.attachment;

        if (attachment == null) {
            sendMessage(remoteMessage);
            return;
        }

        String fileName = attachment.fileName();
        Log.i(TAG, String.format("Subiendo archivo con name %s", fileName));

        api.uploadMessageAttachment(attachment, new Callback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse uploadFileResponse, Response response) {

                Log.e(TAG, String.format("Archivo subido, url: %s", uploadFileResponse.getUrl()));
                remoteMessage.setBody(uploadFileResponse.getUrl());
                sendMessage(remoteMessage);

            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new UploadAttachmentErrorEvent(error));
            }

        });

    }

    private void sendMessage(Message remoteMessage) {

        remoteMessage.getType();

        Log.i(TAG, remoteMessage.getType());

        api.sendMessage(remoteMessage, new Callback<Message>() {
            @Override
            public void success(Message message, Response response) {

                //API doesn't return data uri, so set it again
                message.setMine(true);
                bus.post(new TTMessageSentEvent(message));

            }

            @Override
            public void failure(RetrofitError error) {
                ApiErrorEvent apiErrorEvent = new ApiErrorEvent(error);
                Log.e(TAG, apiErrorEvent.error.getRawBody());
                bus.post(apiErrorEvent);
            }
        });
    }

    @Subscribe
    public void onLoadNotifications(AllNotificationsEvent event) {

        Log.e(TAG, "onLoadNotifications");

        api.allNotifications(new Callback<List<Notification>>() {
            @Override
            public void success(List<Notification> notifications, Response response) {
                bus.post(new AllNotificationsResultEvent(notifications));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onLoadCompanies(SearchCompaniesEvent event) {

        Log.e(TAG, "onLoadCompanies");

        api.searchCompanies(event.params, new Callback<List<Company>>() {
            @Override
            public void success(List<Company> companies, Response response) {
                bus.post(new AllCompaniesResultEvent(companies));
            }

            @Override
            public void failure(RetrofitError error) {

                Log.e(TAG, "Retrofit error: " + error.getMessage());

                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onLoadCompany(GetCompanyEvent event) {

        Log.i(TAG, "onLoadCompany");

        api.getCompany(event.companyId, new Callback<Company>() {

            @Override
            public void success(Company company, Response response) {
                bus.post(new GetCompanyResultEvent(company));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }

        });

    }

    @Subscribe
    public void onLoadContacts(AllContactsEvent event) {

        Log.e(TAG, "onLoadContacts");

        api.allContacts(new Callback<List<Contact>>() {
            @Override
            public void success(List<Contact> contacts, Response response) {
                bus.post(new AllContactsResultEvent(contacts));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onLoadContactContacts(GetContactContactsEvent event) {

        Log.e(TAG, "onLoadContactContacts");

        api.userContacts(event.id, new Callback<List<Contact>>() {
            @Override
            public void success(List<Contact> contacts, Response response) {
                bus.post(new GetContactContactsResultEvent(contacts));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onCurrent(CurrentUserEvent event) {

        Log.i(TAG, "onCurrent");
        api.current(new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                bus.post(new CurrentUserResultEvent(user));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onCurrentInitial(InitialCurrentUserEvent event) {

        Log.i(TAG, "onCurrentInitial");
        api.current(new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                bus.post(new InitialCurrentUserResultEvent(user));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onLogin(final LoginEvent event) {

        Log.i(TAG, "onLogin");

        String username = event.credentials.getEmail();
        String password = event.credentials.getPassword();
        String clientId = "tattlerMobileApp";
        String clientSecret = "tattler@mobile";
        String grantType = "password";

        api.login(username, password, clientId, clientSecret, grantType, new Callback<AccessResponse>() {

            @Override
            public void success(AccessResponse accessResponse, Response response) {

                Log.i(TAG, "login correcto");

                //Set username from login response.
                event.credentials.setUsername(accessResponse.getUsername());

                bus.post(new LoginResultEvent(accessResponse, event.credentials));

            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }

        });

    }

    @Subscribe
    public void onRegister(final RegisterEvent event) {

        Log.i(TAG, "onRegister");

        api.register(event.registrationDetails, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                bus.post(new RegisterResultEvent(user, event.registrationDetails.getPassword()));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onUpdateProfileImage(UpdateProfileImageEvent event) {

        Log.i(TAG, "onUpdateProfileImage");

        api.updateProfileImage(event.imageFile, new Callback<UploadFileResponse>() {
            @Override
            public void success(UploadFileResponse uploadFileResponse, Response response) {
                bus.post(new UpdateProfileImageResultEvent(uploadFileResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });
    }

    @Subscribe
    public void onUpdateProfile(final UpdateProfileEvent event) {

        Log.i(TAG, "onUpdateProfile");

        api.updateProfile(event.user, new Callback<User>() {

            @Override
            public void success(final User user, Response response) {
                bus.post(new UpdateProfileResultEvent(user));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, error.toString());
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onLoadBranchesForCompany(final GetBranchesForCompanyEvent event) {

        Log.i(TAG, "onLoadBranchesForCompany");

        api.getBranchesForCompany(event.company.getId(), new Callback<List<Branch>>() {
            @Override
            public void success(List<Branch> branches, Response response) {
                bus.post(new GetBranchesForCompanyResultEvent(branches));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });
    }

    @Subscribe
    public void onLoadPromotionsForCompany(final GetPromotionsForCompanyEvent event) {

        Log.i(TAG, "onLoadPromotionsForCompany");

        api.getPromotionsForCompany(event.companyId, new Callback<List<Promotion>>() {
            @Override
            public void success(List<Promotion> promotions, Response response) {
                for (Promotion promotion : promotions)
                    promotion.setCompanyId(event.companyId);
                bus.post(new GetPromotionsForCompanyResultEvent(promotions));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });
    }

    @Subscribe
    public void onLoadPromotion(final GetPromotionEvent event) {

        Log.i(TAG, "onLoadPromotion");

        api.getCompanyPromotion(event.companyId, event.promotionId, new Callback<Promotion>() {
            @Override
            public void success(Promotion promotion, Response response) {
                promotion.setCompanyId(event.companyId);
                bus.post(new GetPromotionResultEvent(promotion));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onRequestCoupon(final RequestCouponEvent event) {
        Log.i(TAG, "onRequestCoupon");
        api.requestCoupon(event.promotion.getCompanyId(), event.promotion.getId(), new Callback<Coupon>() {
            @Override
            public void success(Coupon coupon, Response response) {
                bus.post(new RequestCouponResultEvent(coupon));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });
    }

    @Subscribe
    public void onVisitPromotion(final VisitCompanyPromotionsEvent event) {
        Log.i(TAG, "onVisitPromotion");

        Promotion promotion = new Promotion();
        promotion.setCompanyId(event.companyId);

        api.visitPromotions(event.companyId, promotion, new Callback<OperationResponse>() {
            @Override
            public void success(OperationResponse operationResponse, Response response) {
                Log.i(TAG, String.format("Company %s promotions visited", event.companyId));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onUserFavorites(GetFavoritesEvent event) {

        Log.i(TAG, "onUserFavorites");
        api.userFavorites(new Callback<List<Company>>() {
            @Override
            public void success(List<Company> companies, Response response) {
                bus.post(new GetFavoritesResultEvent(companies));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onAddToFavorites(AddFavoriteEvent event) {

        Log.i(TAG, "onAddToFavorites");

        Company company = new Company();
        company.setId(event.companyId);

        FavoriteCompany favorite = new FavoriteCompany();
        favorite.setCompanyId(company.getId());

        api.addToFavorites(favorite, new Callback<Company>() {
            @Override
            public void success(Company company, Response response) {
                bus.post(new AddFavoriteResultEvent(company));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onDeleteFromFavorites(final DeleteFavoriteEvent event) {

        Log.i(TAG, "onDeleteFromFavorites");
        api.deleteFromFavorites(event.companyId, new Callback<OperationResponse>() {
            @Override
            public void success(OperationResponse operationResponse, Response response) {
                bus.post(new DeleteFavoriteResultEvent(operationResponse, event.companyId));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    /*
    Account Recovery
     */
    @Subscribe
    public void onQueryAccount(final QueryUserEvent event) {

        api.queryUserByEmail(event.email, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                user.setEmail(event.email);
                bus.post(new UserFoundEvent(user));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new UserNotFoundEvent(event.email));
            }
        });

    }

    @Subscribe
    public void onUpdatePassword(UpdatePasswordEvent event) {

        api.updatePassword(event.recoveryRequest, new Callback<OperationResponse>() {

            @Override
            public void success(OperationResponse operationResponse, Response response) {
                bus.post(new UpdatePasswordResultEvent(operationResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new UpdatePasswordErrorEvent(error));
            }

        });

    }

    @Subscribe
    public void onSendRecoverEmail(SendRecoverEmailEvent event) {

        api.sendRecoverEmail(event.user, new Callback<OperationResponse>() {
            @Override
            public void success(OperationResponse operationResponse, Response response) {
                bus.post(new RecoverEmailSentEvent(operationResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

    @Subscribe
    public void onRequestQbToken(RequestQbTokenEvent event) {

        api.requestToken(new Callback<QbTattlerToken>() {
            @Override
            public void success(QbTattlerToken qbTattlerToken, Response response) {
                bus.post(new RequestQbTokenResultEvent(qbTattlerToken));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, error.getMessage());
                bus.post(new ApiErrorEvent(error));
            }
        });

    }

}