package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class User {

    @Expose
    private String id;
    @Expose
    private String name;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @Expose
    private String email;
    @Expose
    private String gender;
    @Expose
    private String username;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("is_blocked")
    @Expose
    private boolean blocked;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public String getCompleteName() {
        String name = getName() == null ? "" : getName();
        String lastName = getLastName() == null ? "" : getLastName();
        return String.format("%s %s", name, lastName);
    }

}
