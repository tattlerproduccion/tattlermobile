package mx.tattler.android.events.maps;

public class PlaceDetailEvent {

    public final String placeId;

    public PlaceDetailEvent(String placeId){
        this.placeId = placeId;
    }

}