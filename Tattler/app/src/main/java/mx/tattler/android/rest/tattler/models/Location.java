package mx.tattler.android.rest.tattler.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

@Parcel
public class Location {

    @Expose
    private String address;
    @Expose
    private Double longitude;
    @Expose
    private Double latitude;

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public LatLng getLatLng() {
        return new LatLng(getLatitude(), getLongitude());
    }

}