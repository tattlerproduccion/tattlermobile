
package mx.tattler.android.rest.tattler.models.schedule;

import com.google.gson.annotations.Expose;

import org.parceler.Parcel;

@Parcel
public class Schedule {

    @Expose
    private Day monday;
    @Expose
    private Day tuesday;
    @Expose
    private Day wednesday;
    @Expose
    private Day thursday;
    @Expose
    private Day friday;
    @Expose
    private Day saturday;
    @Expose
    private Day sunday;

    public Day getMonday() {
        return monday;
    }

    public void setMonday(Day monday) {
        this.monday = monday;
    }

    public Day getTuesday() {
        return tuesday;
    }

    public void setTuesday(Day tuesday) {
        this.tuesday = tuesday;
    }

    public Day getWednesday() {
        return wednesday;
    }

    public void setWednesday(Day wednesday) {
        this.wednesday = wednesday;
    }

    public Day getThursday() {
        return thursday;
    }

    public void setThursday(Day thursday) {
        this.thursday = thursday;
    }

    public Day getFriday() {
        return friday;
    }

    public void setFriday(Day friday) {
        this.friday = friday;
    }

    public Day getSaturday() {
        return saturday;
    }

    public void setSaturday(Day saturday) {
        this.saturday = saturday;
    }

    public Day getSunday() {
        return sunday;
    }

    public void setSunday(Day sunday) {
        this.sunday = sunday;
    }

}
