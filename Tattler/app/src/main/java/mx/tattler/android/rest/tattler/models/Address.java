package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class Address {

    @SerializedName("street_line")
    @Expose
    private String streetLine;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("state_id")
    @Expose
    private String stateId;

    /**
     *
     * @return
     * The streetLine
     */
    public String getStreetLine() {
        return streetLine;
    }

    /**
     *
     * @param streetLine
     * The street_line
     */
    public void setStreetLine(String streetLine) {
        this.streetLine = streetLine;
    }

    /**
     *
     * @return
     * The countryId
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     *
     * @param countryId
     * The country_id
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     *
     * @return
     * The stateId
     */
    public String getStateId() {
        return stateId;
    }

    /**
     *
     * @param stateId
     * The state_id
     */
    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String describe() {
        return getStreetLine();
    }
}