package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegistrationDetails {

    @SerializedName("phone_number_or_email")
    @Expose
    private String phoneNumberOrEmail;
    @Expose
    private String password;

    /**
     * @return The phoneNumberOrEmail
     */
    public String getPhoneNumberOrEmail() {
        return phoneNumberOrEmail;
    }

    /**
     * @param phoneNumberOrEmail The phone_number_or_email
     */
    public void setPhoneNumberOrEmail(String phoneNumberOrEmail) {
        this.phoneNumberOrEmail = phoneNumberOrEmail;
    }

    /**
     * @return The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}