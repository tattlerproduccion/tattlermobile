package mx.tattler.android.activities.chat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.activities.AnimatedBusFragmentActivity;
import mx.tattler.android.adapters.chat.ConversationsAdapter;
import mx.tattler.android.constants.ConversationConstants;
import mx.tattler.android.daos.ConversationRepository;
import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.rest.tattler.models.Promotion;

public class SharePromotionActivity extends AnimatedBusFragmentActivity implements ConversationsAdapter.OnFilterListener, AdapterView.OnItemClickListener {

    private Promotion promotion;
    private ConversationsAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_share_promotion);

        //Get Conversation from Extras
        promotion = Parcels.unwrap(getIntent().getExtras().getParcelable(ConversationConstants.PROMOTION));

        ListView listView = (ListView) findViewById(R.id.lv_conversations);
        TextView emptyConversationsView = (TextView) findViewById(R.id.tv_empty_conversations);

        adapter = new ConversationsAdapter(this, this);
        listView.setEmptyView(emptyConversationsView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        List<TattlerConversation> conversationList = ConversationRepository.getAll(this);
        adapter.setConversations(conversationList, false);

    }

    @Override
    protected View getContainerView() {
        return null;
    }

    @Override
    protected View getProgressView() {
        return null;
    }

    @Override
    public void onEmptyResult() {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        TattlerConversation conversation = adapter.getItem(position);

        Intent intent = new Intent(this, ConversationActivity.class);
        Bundle bundle = new Bundle();

        //TODO: Wrap conversation to target activity
        Parcelable wrapped = Parcels.wrap(conversation);
        bundle.putParcelable(ConversationConstants.CONVERSATION, wrapped);
        wrapped = Parcels.wrap(promotion);
        bundle.putParcelable(ConversationConstants.PROMOTION, wrapped);
        intent.putExtras(bundle);

        startActivity(intent);

    }

}
