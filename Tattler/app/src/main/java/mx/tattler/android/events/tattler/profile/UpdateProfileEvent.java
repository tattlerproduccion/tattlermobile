package mx.tattler.android.events.tattler.profile;

import mx.tattler.android.rest.tattler.models.User;

public class UpdateProfileEvent {

    public final User user;

    public UpdateProfileEvent(User user) {
        this.user = user;
    }
}
