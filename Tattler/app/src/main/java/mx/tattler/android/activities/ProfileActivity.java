package mx.tattler.android.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import mx.tattler.android.R;
import mx.tattler.android.constants.ContactConstants;
import mx.tattler.android.fragments.FriendContactListFragment;

public class ProfileActivity extends BusFragmentActivity{

    private static final String TAG = ProfileActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);

        String contactId = getIntent().getStringExtra(ContactConstants.CONTACT_ID);
        Log.i(TAG, String.format("Profile id: %s", contactId));

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

            Button btnContacts = (Button)rootView.findViewById(R.id.btn_profile_contacts);

            btnContacts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, new FriendContactListFragment())
                            .addToBackStack(null)
                            .commit();
                }
            });

            return rootView;
        }
    }
}