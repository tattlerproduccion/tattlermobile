package mx.tattler.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import mx.tattler.android.R;
import mx.tattler.android.daos.FavoritesRepository;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.AddFavoriteEvent;
import mx.tattler.android.events.tattler.AddFavoriteResultEvent;
import mx.tattler.android.events.tattler.DeleteFavoriteEvent;
import mx.tattler.android.events.tattler.DeleteFavoriteResultEvent;
import mx.tattler.android.events.tattler.GetCompanyEvent;
import mx.tattler.android.events.tattler.GetCompanyResultEvent;
import mx.tattler.android.events.tattler.GetFavoritesEvent;
import mx.tattler.android.events.tattler.GetFavoritesResultEvent;
import mx.tattler.android.fragments.listeners.OnBusinessInteractionListener;
import mx.tattler.android.rest.tattler.models.Company;

public class CompanyDetailFragment extends BusFragment implements View.OnClickListener {

    private static final String TAG = CompanyDetailFragment.class.getName();

    private Picasso picasso;
    private OnBusinessInteractionListener activityListener;

    private ImageView companyImage;
    private ImageView companyBackgroundImage;
    private TextView companyDescription;
    private Button favoriteButton;
    private String companyId;

    private boolean isFavorite;

    public CompanyDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        picasso = Picasso.with(activity);

        try {
            activityListener = (OnBusinessInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnBusinessInteractionListener");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_business_detail, container, false);

        companyDescription = (TextView) rootView.findViewById(R.id.tv_company_description);

        companyImage = (ImageView) rootView.findViewById(R.id.iv_company_logo);
        companyBackgroundImage = (ImageView) rootView.findViewById(R.id.iv_company_background);

        favoriteButton = (Button) rootView.findViewById(R.id.btn_favorite);

        setButtonListener(rootView, R.id.btn_favorite);
        setButtonListener(rootView, R.id.iv_company_background);
        setButtonListener(rootView, R.id.btn_branches);

        return rootView;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_company_background:
                activityListener.onPromotionsDetail();
                break;
            case R.id.btn_branches:
                activityListener.onBranchesDetail();
                break;
            case R.id.btn_favorite:
                changeFavoriteState();
                break;
        }

    }

    @Subscribe
    public void onFavorites(GetFavoritesResultEvent event) {
        isFavorite = FavoritesRepository.isFavoriteFromList(event.companies, companyId);
        int drawable = isFavorite ? android.R.drawable.btn_star_big_on : android.R.drawable.btn_star_big_off;
        favoriteButton.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0);
    }

    @Subscribe
    public void onCompanyLoaded(GetCompanyResultEvent event) {

        Company company = event.company;
        this.companyId = company.getId();

        Log.i(TAG, String.format("Fragment's Company[id]: %s, [name]: %s", company.getId(), company.getName()));

        getActivity().getActionBar().setTitle(company.getName());
        String description = (company.getDescription() != null) ? company.getDescription() : "";

        companyDescription.setText(description);

        picasso.load(company.getProfileImage())
                .fit()
                .placeholder(R.drawable.ic_default_company)
                .error(R.drawable.ic_default_company)
                .into(companyImage);

        picasso.load(company.getBackgroundImage())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_default_background)
                .error(R.drawable.ic_default_background)
                .into(companyBackgroundImage);


        listener.onRequest().post(new GetFavoritesEvent());
        favoriteButton.setText(String.valueOf(company.getFavoritesCount()));

    }

    @Subscribe
    public void onFavoriteAdded(AddFavoriteResultEvent event) {
        listener.onRequest().post(new GetCompanyEvent(companyId));
        listener.onRequest().post(new GetFavoritesEvent());
    }

    @Subscribe
    public void onDeleteFromFavorite(DeleteFavoriteResultEvent event) {
        listener.onRequest().post(new GetCompanyEvent(companyId));
        listener.onRequest().post(new GetFavoritesEvent());
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {

        String url = event.error.getUrl();
        if (!TextUtils.isEmpty(url)) {
            if (url.contains("user/favorites")) {
                listener.onRequest().post(new GetCompanyEvent(companyId));
                listener.onRequest().post(new GetFavoritesEvent());
            }
        }else{
            Toast.makeText(getActivity(), event.error.getRawBody(), Toast.LENGTH_LONG).show();
        }

    }

    private void changeFavoriteState() {

        if (companyId == null)
            return;

        if (isFavorite)
            listener.onRequest().post(new DeleteFavoriteEvent(companyId));
        else
            listener.onRequest().post(new AddFavoriteEvent(companyId));

    }

    private void setButtonListener(View root, int id) {
        View view = (View) root.findViewById(id);
        view.setOnClickListener(this);
    }

}