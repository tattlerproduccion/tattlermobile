package mx.tattler.android.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.adapters.chat.ConversationsAdapter;
import mx.tattler.android.rest.tattler.models.Company;

public class FilterableCompanyAdapter extends CompanyListAdapter implements Filterable {

    private static final String TAG = FilterableCompanyAdapter.class.getName();

    private List<Company> companiesBackup = new ArrayList<>();
    private ConversationsAdapter.OnFilterListener listener;

    public FilterableCompanyAdapter(Context context, ConversationsAdapter.OnFilterListener listener) {
        super(context);
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public void setCompanies(List<Company> companies, boolean filtered) {

        if (!filtered) {
            this.companiesBackup = companies;
        }

        super.setCompanies(companies);

    }

    private Filter filter = new Filter() {
        @Override
        protected Filter.FilterResults performFiltering(CharSequence constraint) {

            Log.i(TAG, "performFiltering");

            Filter.FilterResults filterResults = new Filter.FilterResults();

            if (TextUtils.isEmpty(constraint)) {

                filterResults.count = companiesBackup.size();
                filterResults.values = companiesBackup;

            } else {

                Log.i(TAG, "performFiltering for " + constraint);

                List<Company> filteredConversations = new ArrayList<>();

                String companyName;
                for (Company company : companies) {

                    companyName = company.getName();

                    if (!TextUtils.isEmpty(companyName) && companyName.toLowerCase().contains(constraint.toString().toLowerCase()))
                        filteredConversations.add(company);

                }

                filterResults.count = filteredConversations.size();
                filterResults.values = filteredConversations;

            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {

            if (results.values == null) {
                Log.e(TAG, "valores filtrados nulos!");
                return;
            }

            Log.i(TAG, "Results size for " + constraint + ": " + results.count);

            List<Company> result = (List<Company>) results.values;
            Log.i(TAG, "Conversations found" + result.size());
            setCompanies(result, true);

            if (result.isEmpty())
                listener.onEmptyResult();

        }

    };

}
