package mx.tattler.android.utils.intent;

import android.content.Intent;

/**
 * Created by irving on 31/10/14.
 */
public class URLIntentFactory {

    public static Intent create(String subject, String url){

        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, url);

        return intent;
    }

    private URLIntentFactory(){}

}
