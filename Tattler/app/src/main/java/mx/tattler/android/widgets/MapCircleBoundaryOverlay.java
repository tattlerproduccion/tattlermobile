package mx.tattler.android.widgets;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.mapbox.mapboxsdk.overlay.SafeDrawOverlay;
import com.mapbox.mapboxsdk.views.MapView;
import com.mapbox.mapboxsdk.views.safecanvas.ISafeCanvas;
import com.mapbox.mapboxsdk.views.safecanvas.SafeDashPathEffect;
import com.mapbox.mapboxsdk.views.safecanvas.SafePaint;

public class MapCircleBoundaryOverlay extends SafeDrawOverlay {

    private int size;

    @Override
    protected void drawSafe(ISafeCanvas canvas, MapView mapView, boolean b) {

        SafePaint circlePaint = new SafePaint();
        SafePaint textPaint = new SafePaint();
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setColor(Color.BLACK);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(100);
        circlePaint.setPathEffect(new SafeDashPathEffect(new float[]{10, 20}, 0, 0));
        circlePaint.setStrokeWidth(2);

        int width = canvas.getWidth();
        int height = canvas.getHeight();
        RectF scrollableAreaLimit = mapView.getScrollableAreaLimit();
        float height1 = scrollableAreaLimit.height();
        float width1 = scrollableAreaLimit.width();
        int padding = 200;

        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, width - padding, circlePaint);

    }
}