package mx.tattler.android.utils.resource;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;

import retrofit.mime.TypedByteArray;

public class TypedFileFromBytes extends TypedByteArray {

    private static final String TAG = TypedFileFromBytes.class.getName();

    private final String filename;

    public static TypedFileFromBytes instance(Context context, Uri uri) {

        try{

            ContentResolver cR = context.getContentResolver();
            String mime = cR.getType(uri);

            File imageFile = new File(FileUtils.getPath(context, uri));

            String fileName = imageFile.getName();

            int dot = fileName.lastIndexOf(".");
            if (dot >= 0) {
                fileName = String.format("file%s", fileName.substring(dot));
            } else {
                // No extension.
                fileName = "";
            }

            byte[] blob = new byte[(int) imageFile.length()];
            FileInputStream fis = new FileInputStream(imageFile);
            fis.read(blob);
            fis.close();

            return new TypedFileFromBytes(mime, fileName, blob);

        }catch (Exception exception){
            Log.e(TAG, exception.toString());
            return null;
        }

    }

    public TypedFileFromBytes(String mimeType, String filename, byte[] bytes) {
        super(mimeType, bytes);
        this.filename = filename;
    }

    @Override
    public String fileName() {
        return filename;
    }

}