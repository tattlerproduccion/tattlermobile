package mx.tattler.android.events.tattler.recover;

import mx.tattler.android.rest.tattler.models.RecoveryRequest;

public class UpdatePasswordEvent {

    public final RecoveryRequest recoveryRequest;

    public UpdatePasswordEvent(RecoveryRequest recoveryRequest) {
        this.recoveryRequest = recoveryRequest;
    }

}
