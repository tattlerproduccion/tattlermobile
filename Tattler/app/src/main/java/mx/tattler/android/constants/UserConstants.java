package mx.tattler.android.constants;

public class UserConstants {

    public static final String USER_ID = "USER_ID";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";

    public static final String USER_CHAT_SESSION= "CHAT_SESSION";

    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PASSWORD = "USER_PASSWORD";
    public static final String USER = "USER";
    public static final String USER_CREDENTIALS = "USER_CREDENTIALS";

    private UserConstants(){}

}
