package mx.tattler.android.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.RadioGroup;

import com.squareup.otto.Subscribe;

import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.adapters.MainTabsPagerAdapter;
import mx.tattler.android.daos.FavoritesRepository;
import mx.tattler.android.events.tattler.GetFavoritesEvent;
import mx.tattler.android.events.tattler.GetFavoritesResultEvent;
import mx.tattler.android.fragments.CompanySearchFragment;
import mx.tattler.android.fragments.pages.CustomImageFragment;
import mx.tattler.android.rest.tattler.models.Company;

public class MainDrawerActivity extends BusFragmentActivity implements RadioGroup.OnCheckedChangeListener {

    private static final String TAG = MainDrawerActivity.class.getName();

    private ViewPager pager;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_drawer);

        final RadioGroup group = (RadioGroup) findViewById(R.id.rg_main_tabs);

        group.setOnCheckedChangeListener(this);

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new MainTabsPagerAdapter(getSupportFragmentManager()));

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                int viewId = getViewForIndex(i);
                group.check(viewId);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int currentTabId = preferences.getInt(getString(R.string.current_main_tab_id), R.id.rb_company_search);
        group.check(currentTabId);

        //Get User favorites
        getBus().post(new GetFavoritesEvent());

    }

    @Subscribe
    public void onFavorites(GetFavoritesResultEvent event){
        List<Company> favorites = event.companies;
        FavoritesRepository.insertBatch(this, favorites);
    }

    private int getIndexForView(int id) {

        int index;

        switch (id) {
            case R.id.rb_settings:
                index = 0;
                break;
            case R.id.rb_messages:
                index = 1;
                break;
            case R.id.rb_company_search:
                index = 2;
                break;
            default:
                index = 0;
                break;
        }

        return index;
    }

    private int getViewForIndex(int index) {

        int id;

        switch (index) {
            case 0:
                id = R.id.rb_settings;
                break;
            case 1:
                id = R.id.rb_messages;
                break;
            case 2:
                id = R.id.rb_company_search;
                break;
            default:
                id = R.id.rb_settings;
                break;
        }

        return id;

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int id) {
        int index = getIndexForView(id);
        preferences.edit().putInt(getString(R.string.current_main_tab_id), id).apply();
        pager.setCurrentItem(index);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof CompanySearchFragment) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

}
