package mx.tattler.android.utils.cursors;

import android.database.MatrixCursor;

import java.util.List;

import mx.tattler.android.rest.maps.models.Prediction;

/**
 * Created by irving on 1/11/14.
 */
public class CursorFactory {

    public static MatrixCursor convertToCursor(List<Prediction> predictions, String[] from) {
        MatrixCursor cursor = new MatrixCursor(from);
        Integer i = 0;
        for (Prediction prediction : predictions) {
            String[] temp = new String[3];
            temp[0] = (i++).toString();
            temp[1] = prediction.getPlaceId();
            temp[2] = prediction.getDescription();
            cursor.addRow(temp);
        }
        return cursor;
    }

    private CursorFactory(){

    }

}
