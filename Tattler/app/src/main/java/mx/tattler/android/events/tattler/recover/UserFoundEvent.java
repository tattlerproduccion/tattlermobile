package mx.tattler.android.events.tattler.recover;

import mx.tattler.android.rest.tattler.models.User;

public class UserFoundEvent {

    public final User user;

    public UserFoundEvent(User user) {
        this.user = user;
    }
}
