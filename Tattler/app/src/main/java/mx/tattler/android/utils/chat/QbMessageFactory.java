package mx.tattler.android.utils.chat;

import com.quickblox.chat.model.QBChatMessage;

import java.util.Date;

import mx.tattler.android.daos.TattlerMessage;
import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.utils.text.DateTextHelper;

public class QbMessageFactory {

//    public static final String MESSAGE_TYPE = "message_type";
//    public static final String MESSAGE_TYPE_NORMAL = "text";
//    public static final String MESSAGE_TYPE_FILE_ATTACHMENT = "file_attachment";
//    public static final String MESSAGE_TYPE_LOCATION = "location";
//    public static final String MESSAGE_TYPE_PHOTO = "photo";
//    public static final String MESSAGE_TYPE_AUDIO = "audio";
//    public static final String MESSAGE_TYPE_VIDEO = "video";
//    public static final String MESSAGE_TYPE_CONTACT = "contact";

    private QbMessageFactory(){}

    public static QBChatMessage create(TattlerMessage message){

        QBChatMessage chatMessage = new QBChatMessage();
        chatMessage.setBody(message.getBody());
        chatMessage.setSaveToHistory(true);
        chatMessage.setProperty(Message.MESSAGE_TYPE, message.getType());
        chatMessage.setProperty(Message.TATTLER_MESSAGE_ID, message.getMessageId());
        chatMessage.setProperty(Message.DATE_SENT, DateTextHelper.formatMessageDate(new Date()));

        return chatMessage;

    }


}
