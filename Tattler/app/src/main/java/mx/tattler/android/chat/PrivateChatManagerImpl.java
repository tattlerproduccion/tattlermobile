package mx.tattler.android.chat;

import android.util.Log;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBPrivateChat;
import com.quickblox.chat.QBPrivateChatManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBMessageListenerImpl;
import com.quickblox.chat.listeners.QBPrivateChatManagerListener;
import com.quickblox.chat.model.QBChatMessage;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.daos.TattlerMessage;
import mx.tattler.android.daos.TattlerMessageFactory;
import mx.tattler.android.utils.chat.QbMessageFactory;

public class PrivateChatManagerImpl extends QBMessageListenerImpl<QBPrivateChat> implements ChatManager, QBPrivateChatManagerListener {

    private static final String TAG = ChatManager.class.getName();

    private final QBPrivateChatManager privateChatManager;

    private QBPrivateChat privateChat;
    private OnMessageLister listener;


    public PrivateChatManagerImpl(OnMessageLister listener, Integer receiverId) {

        this.listener = listener;

        privateChatManager = QBChatService.getInstance().getPrivateChatManager();

        if(privateChatManager == null){
            listener.onTokenExpired();
            return;
        }

        privateChatManager.addPrivateChatManagerListener(this);

        // init private chat
        //
        privateChat = privateChatManager.getChat(receiverId);
        if (privateChat == null) {
            privateChat = privateChatManager.createChat(receiverId, this);
        }else if(privateChat.getMessageListeners().isEmpty()){
                privateChat.addMessageListener(this);
        }

    }

    @Override
    public void sendMessage(TattlerMessage message) throws XMPPException, SmackException.NotConnectedException {
        QBChatMessage qbMessage = QbMessageFactory.create(message);
        privateChat.sendMessage(qbMessage);
        listener.onMessageSent(message);
    }

    @Override
    public void release() {
        Log.w(TAG, "release private chat");
        privateChat.removeMessageListener(this);
        privateChatManager.removePrivateChatManagerListener(this);
    }

    @Override
    public void processMessage(QBPrivateChat chat, QBChatMessage qbMessage) {
        Log.w(TAG, "new incoming message: " + qbMessage + " from " + chat.getParticipant());
        listener.onMessageReceived(qbMessage);
    }

    @Override
    public void processError(QBPrivateChat chat, QBChatException error, QBChatMessage originChatMessage){

    }

    @Override
    public void chatCreated(QBPrivateChat incomingPrivateChat, boolean createdLocally) {
        if(!createdLocally){
            privateChat = incomingPrivateChat;
            if(privateChat.getMessageListeners().isEmpty())
                privateChat.addMessageListener(PrivateChatManagerImpl.this);
        }

        Log.i(TAG, "private chat created: " + incomingPrivateChat.getParticipant() + ", createdLocally:" + createdLocally);
    }

}