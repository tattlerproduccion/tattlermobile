package mx.tattler.android.chat;

import android.content.Context;

import com.quickblox.chat.model.QBChatMessage;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import mx.tattler.android.daos.TattlerConversation;
import mx.tattler.android.daos.TattlerMessage;

public interface ChatManager {

    public interface OnMessageLister{
        void onMessageSent(TattlerMessage message);
        void onMessageReceived(QBChatMessage qbMessage);
        Context getContext();
        void onTokenExpired();
    }

    void sendMessage(TattlerMessage message) throws XMPPException, SmackException.NotConnectedException;

    void release() throws XMPPException;

}