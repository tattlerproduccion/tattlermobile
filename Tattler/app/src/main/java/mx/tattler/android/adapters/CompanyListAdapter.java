package mx.tattler.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.activities.CompanyDetailActivity;
import mx.tattler.android.constants.CompanyConstants;
import mx.tattler.android.rest.tattler.models.Company;

public class CompanyListAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

    private static final String TAG = CompanyListAdapter.class.getName();

    private final Picasso picasso;
    private final Context context;
    private final LayoutInflater inflater;
    protected List<Company> companies;

    public CompanyListAdapter(Context context) {

        this.context = context;
        inflater = LayoutInflater.from(context);
        picasso = Picasso.with(context);

        companies = new ArrayList<Company>();

    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_company, parent, false);
            holder = new ViewHolder();
            holder.profileImage = (ImageView) view.findViewById(R.id.iv_business_image);
            holder.allowedPromotionsImage = (ImageView) view.findViewById(R.id.ib_promotions_allowed);
            holder.allowedTattlerCallsImage = (ImageView) view.findViewById(R.id.ib_tattler_calls_allowed);
            holder.allowedTattlerMessagesImage = (ImageView) view.findViewById(R.id.ib_tattler_messages_allowed);
            holder.companyName = (TextView) view.findViewById(R.id.tv_company_name);
            holder.categoryName = (TextView) view.findViewById(R.id.tv_category_name);
            holder.favoritesCount = (TextView) view.findViewById(R.id.tv_favorites_count);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final Company company = companies.get(position);

        holder.profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCompany(company);
            }
        });

        holder.companyName.setText(company.getName());
        //holder.categoryName.setText(company.getName());
        holder.favoritesCount.setText(String.valueOf(company.getFavoritesCount()));

        int drawable = company.getAllowsTattlerCalling() ? R.drawable.ic_telephone_active : R.drawable.ic_telephone_inactive;
        holder.allowedTattlerCallsImage.setImageResource(drawable);

        drawable = company.getAllowsTattlerCalling() ? R.drawable.ic_messaging_active : R.drawable.ic_messaging_inactive;
        holder.allowedTattlerMessagesImage.setImageResource(drawable);

        //TODO: Check promotions size before
        drawable = company.getFavoritesCount() > 0 ? R.drawable.ic_promotions_active : R.drawable.ic_promotions_inactive;
        holder.allowedPromotionsImage.setImageResource(drawable);

        picasso.load(company.getProfileImage())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_default_company)
                .error(R.drawable.ic_default_company)
                .into(holder.profileImage);

        return view;

    }

    @Override
    public int getCount() {
        return companies.size();
    }

    @Override
    public Company getItem(int position) {
        return companies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
        notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Company company = companies.get(position);
        showCompany(company);
    }

    private void showCompany(Company company) {
        Intent intent = new Intent(context, CompanyDetailActivity.class);
        intent.putExtra(CompanyConstants.COMPANY_ID, company.getId());
        context.startActivity(intent);
    }

    static class ViewHolder {
        ImageView profileImage;
        ImageView allowedPromotionsImage;
        ImageView allowedTattlerCallsImage;
        ImageView allowedTattlerMessagesImage;
        TextView companyName;
        TextView categoryName;
        TextView favoritesCount;
    }

}
