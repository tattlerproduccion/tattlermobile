package mx.tattler.android.rest.tattler.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification {

    @Expose
    private String id;
    @SerializedName("contact_id")
    @Expose
    private String contactId;
    @SerializedName("profile_name")
    @Expose
    private String profileName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @Expose
    private String type;
    @Expose
    private String image;
    @SerializedName("call_elapsed_time")
    @Expose
    private String callElapsedTime;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The contactId
     */
    public String getContactId() {
        return contactId;
    }

    /**
     * @param contactId The contact_id
     */
    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    /**
     * @return The profileName
     */
    public String getProfileName() {
        return profileName;
    }

    /**
     * @param profileName The profile_name
     */
    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    /**
     * @return The profileImage
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * @param profileImage The profile_image
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return The callElapsedTime
     */
    public String getCallElapsedTime() {
        return callElapsedTime;
    }

    /**
     * @param callElapsedTime The call_elapsed_time
     */
    public void setCallElapsedTime(String callElapsedTime) {
        this.callElapsedTime = callElapsedTime;
    }

    public boolean isFinishedCall(){
        return this.type.equalsIgnoreCase("finished_call");
    }

    public boolean isSharedPromotion(){
        return this.type.equalsIgnoreCase("shared_promotion");
    }

}