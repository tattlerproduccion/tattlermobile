package mx.tattler.android.events.tattler;

import mx.tattler.android.rest.tattler.OperationResponse;

public class DeleteFavoriteResultEvent {

    public final OperationResponse response;
    public final String companyId;

    public DeleteFavoriteResultEvent(OperationResponse response, String companyId) {
        this.response = response;
        this.companyId = companyId;
    }
}
