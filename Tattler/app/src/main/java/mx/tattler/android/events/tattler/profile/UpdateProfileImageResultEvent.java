package mx.tattler.android.events.tattler.profile;

import mx.tattler.android.rest.tattler.models.UploadFileResponse;

public class UpdateProfileImageResultEvent {

    public final UploadFileResponse uploadFileResponse;

    public UpdateProfileImageResultEvent(UploadFileResponse uploadFileResponse) {
        this.uploadFileResponse = uploadFileResponse;
    }

}
