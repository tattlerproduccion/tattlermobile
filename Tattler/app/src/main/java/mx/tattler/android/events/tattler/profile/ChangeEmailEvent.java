package mx.tattler.android.events.tattler.profile;

public class ChangeEmailEvent {

    public final String newEmail;

    public ChangeEmailEvent(String newEmail) {
        this.newEmail = newEmail;
    }

}
