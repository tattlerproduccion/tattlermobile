package mx.tattler.android.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import mx.tattler.android.R;
import mx.tattler.android.chat.UserCredentials;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.LoginEvent;
import mx.tattler.android.events.tattler.RegisterEvent;
import mx.tattler.android.events.tattler.RegisterResultEvent;
import mx.tattler.android.events.tattler.login.EmailAsUsernameExceptionEvent;
import mx.tattler.android.rest.tattler.models.RegistrationDetails;

public class RegisterFragment extends AnimatedBusFragment {

    private static final String EMAIL_INPUT_HINT = "Ingresa tu dirección de correo electrónico:";
    private static final String CHANGE_EMAIL_INPUT_HINT = "Utilizar mi correo electrónico";
    private static final String CHANGE_CELLPHONE_INPUT_HINT = "Utilizar mi número celular";
    private static final String CELLPHONE_INPUT_HINT = "Ingresa tu número de celular:";

    private View progressbarContainer;
    private View containerView;

    private TextView tvHintMessage;

    private EditText etNumberPhone;
    private EditText etPassword;

    public RegisterFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);

        containerView = rootView.findViewById(R.id.container);
        progressbarContainer = (ProgressBar) rootView.findViewById(R.id.pb_loading);

        tvHintMessage = (TextView) rootView.findViewById(R.id.tv_input_hint);

        etNumberPhone = (EditText) rootView.findViewById(R.id.et_email);
        etPassword = (EditText) rootView.findViewById(R.id.et_password);

        Button btn_createAccount = (Button) rootView.findViewById(R.id.btn_create_account);

        btn_createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createAccount();
            }
        });

        tvHintMessage.setText(EMAIL_INPUT_HINT);

        return rootView;
    }

    private void createAccount() {

        String numberPhone = etNumberPhone.getText().toString();
        String password = etPassword.getText().toString();

        if (numberPhone.length() == 0 || password.length() == 0) {
            Toast.makeText(getActivity(), "Llena tus datos primero", Toast.LENGTH_SHORT).show();
            return;
        }

        RegistrationDetails details = new RegistrationDetails();
        details.setPhoneNumberOrEmail(numberPhone);
        details.setPassword(password);

        showProgress(true);
        listener.onRequest().post(new RegisterEvent(details));

    }

    @Subscribe
    public void onRegisteredAccount(RegisterResultEvent event) {
        UserCredentials credentials = UserCredentials.instance(event.user.getUsername(), event.password, "");
        listener.onRequest().post(new LoginEvent(credentials));
    }


    @Subscribe
    public void onDeprecatedAccount(EmailAsUsernameExceptionEvent event){
        Toast.makeText(getActivity(), "Tu cuenta tiene email como nombre de usuario en QB, no se puede loggear!!! Pidele al admin que cambie tu cuenta o reinicie la BD", Toast.LENGTH_LONG).show();
        showProgress(false);
    }

    @Subscribe
    public void onApiError(ApiErrorEvent event) {

        showProgress(false);

        String message = event.error.getMessage();
        message = (message == null) ? "No se pudo completar el registro" : message;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Error en registro");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", null);
        builder.create();
        builder.show();

        //Toast.makeText(getActivity(), , Toast.LENGTH_SHORT).show();

    }


    @Override
    protected View getContainerView() {
        return containerView;
    }

    @Override
    protected View getProgressView() {
        return progressbarContainer;
    }

}