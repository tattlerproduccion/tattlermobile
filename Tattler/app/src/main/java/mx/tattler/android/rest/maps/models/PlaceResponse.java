package mx.tattler.android.rest.maps.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PlaceResponse {

    @Expose
    private Result result;

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    public Location getLocation(){
        return result.getGeometry().getLocation();
    }

    public AddressComponent getLocality(){
        return result.getLocalityComponent();
    }

}


class Result {

    @Expose
    private Geometry geometry;

    @SerializedName("address_components")
    @Expose
    private List<AddressComponent> addressComponents = new ArrayList<AddressComponent>();

    /**
     *
     * @return
     * The addressComponents
     */
    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    /**
     *
     * @param addressComponents
     * The address_components
     */
    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    /**
     * @return The geometry
     */
    public Geometry getGeometry() {
        return geometry;
    }

    /**
     * @param geometry The geometry
     */
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public AddressComponent getLocalityComponent(){

        for(AddressComponent component : addressComponents)
            for(String type : component.getTypes())
                if(type.contains(AddressComponent.LOCALITIY_TYPE))
                    return component;

        return null;

    }


}

class Geometry {

    @Expose
    private Location location;

    /**
     * @return The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

}