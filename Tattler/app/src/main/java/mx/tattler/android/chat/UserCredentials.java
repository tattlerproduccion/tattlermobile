package mx.tattler.android.chat;

import com.google.gson.annotations.Expose;

public class UserCredentials {

    @Expose
    private String email;

    @Expose
    private String username;

    @Expose
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static UserCredentials instance(String email, String password, String username) {

        UserCredentials credentials = new UserCredentials();
        credentials.email = email;
        credentials.password = password;
        credentials.username = username;

        return credentials;

    }
}
