package mx.tattler.android.events;

import mx.tattler.android.rest.tattler.models.RestError;
import retrofit.RetrofitError;

public class ApiErrorEvent {

    public RestError error;

    public ApiErrorEvent(RetrofitError retrofitError){

        error = new RestError();

        try {

            if (retrofitError.getResponse() != null) {
                error = (RestError) retrofitError.getBodyAs(RestError.class);
            } else {
                error = new RestError();
                error.setMessage(retrofitError.getMessage());
            }

        } catch (Exception ignored) {
            error.setMessage(retrofitError.getMessage());
        }finally{

            error.setInnerError(retrofitError);

        }

    }

}