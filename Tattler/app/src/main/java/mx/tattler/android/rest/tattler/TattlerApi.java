package mx.tattler.android.rest.tattler;

import java.util.List;
import java.util.Map;

import mx.tattler.android.rest.tattler.models.AccessResponse;
import mx.tattler.android.rest.tattler.models.Branch;
import mx.tattler.android.rest.tattler.models.Company;
import mx.tattler.android.rest.tattler.models.Contact;
import mx.tattler.android.rest.tattler.models.Coupon;
import mx.tattler.android.rest.tattler.models.FavoriteCompany;
import mx.tattler.android.rest.tattler.models.Notification;
import mx.tattler.android.rest.tattler.models.Promotion;
import mx.tattler.android.rest.tattler.models.RecoveryRequest;
import mx.tattler.android.rest.tattler.models.RegistrationDetails;
import mx.tattler.android.rest.tattler.models.UploadFileResponse;
import mx.tattler.android.rest.tattler.models.User;
import mx.tattler.android.rest.tattler.models.chat.Conversation;
import mx.tattler.android.rest.tattler.models.chat.Message;
import mx.tattler.android.rest.tattler.models.chat.QbTattlerToken;
import mx.tattler.android.utils.resource.TypedFileFromBytes;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.QueryMap;
import retrofit.mime.TypedFile;

public interface TattlerApi {

    //public static final String API_URL = "http://private-f3b54-tattler.apiary-mock.com";

    //public static final String API_URL = "http://192.168.1.77:5000";
    //public static final String API_URL = "http://ec2-54-69-1-179.us-west-2.compute.amazonaws.com/api";
    public static final String API_URL = "https://t946e8afee7d1b678dc0d683d3b6fd88d.business-tattler.com";

    /*
    Conversations
     */
    @GET("/conversations")
    public void conversations(Callback<List<Conversation>> callback);

    @GET("/conversations/{conversation_id}/messages")
    public void conversationMessages(@Path("conversation_id") String conversationId, Callback<List<Message>> callback);

    @POST("/conversations/messages")
    public void sendMessage(@Body Message message, Callback<Message> callback);

    @Multipart
    @POST("/conversations/files")
    public void uploadMessageAttachment(@Part("file") TypedFileFromBytes file,
                                   Callback<UploadFileResponse> callback);

    /*
    Companies
     */
    @GET("/companies")
    public void searchCompanies(@QueryMap Map<String, String> parameters, Callback<List<Company>> callback);

    @GET("/companies/{id}")
    public void getCompany(@Path("id") String id, Callback<Company> callback);

    /*
    Branches
     */
    @GET("/companies/{id}/branches")
    public void getBranchesForCompany(@Path("id") String id, Callback<List<Branch>> callback);

    /*
    Promotions
     */
    @GET("/companies/{id}/promotions")
    public void getPromotionsForCompany(@Path("id") String id, Callback<List<Promotion>> callback);

    @GET("/companies/{company_id}/promotions/{promotion_id}")
    public void getCompanyPromotion(@Path("company_id") String companyId, @Path("promotion_id") String promotionId,Callback<Promotion> callback);

     @POST("/companies/{company_id}/promotions/{promotion_id}/coupons")
     public void requestCoupon(@Path("company_id") String companyId, @Path("promotion_id") String promotionId, Callback<Coupon> callback);

    @PUT("/companies/{company_id}/promotions/visiting")
    public void visitPromotions(@Path("company_id") String companyId, @Body Promotion promotion, Callback<OperationResponse> callback);

    /*
    Favorites
     */
    @GET("/user/favorites")
    public void userFavorites(Callback<List<Company>> callback);

    @PUT("/user/favorites")
    public void addToFavorites(@Body FavoriteCompany company, Callback<Company> callback);

    @DELETE("/user/favorites/{company_id}")
    public void deleteFromFavorites(@Path("company_id") String companyId, Callback<OperationResponse> callback);

    @GET("/user")
    public void current(Callback<User> callback);

    @PUT("/user/profile")
    public void updateProfile(@Body User user, Callback<User> callback);

    @Multipart
    @POST("/user/image")
    public void updateProfileImage(@Part("file") TypedFile file,
                            Callback<UploadFileResponse> callback);

    @POST("/users")
    public void register(@Body RegistrationDetails details, Callback<User> callback);

    @GET("/user/contacts")
    public void allContacts(Callback<List<Contact>> callback);

    @GET("/user/{id}/contacts")
    public void userContacts(@Path("id") String id, Callback<List<Contact>> callback);

    @GET("/notifications")
    public void allNotifications(Callback<List<Notification>> callback);

    /*
    Account Recovery
     */
    @FormUrlEncoded
    @POST("/accounts/login")
    public void login(@Field("username") String username, @Field("password") String password,
                      @Field("client_id") String clientId, @Field("client_secret") String clientSecret,
                      @Field("grant_type") String granType, Callback<AccessResponse> callback);


    @GET("/users/{email}")
    public void queryUserByEmail(@Path("email") String email, Callback<User> callback);

    @POST("/user/account/recovery-token")
    public void sendRecoverEmail(@Body User user, Callback<OperationResponse> callback);

    @PUT("/user/password")
    public void updatePassword(@Body RecoveryRequest recoveryRequest, Callback<OperationResponse> callback);

    /*
    Conversation Token
     */
    @GET("/communications/messaging/token")
    public void requestToken(Callback<QbTattlerToken> callback);


}
