package mx.tattler.android.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.parceler.guava.base.Predicate;
import org.parceler.guava.collect.Collections2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.constants.SettingsConstants;
import mx.tattler.android.rest.tattler.models.Branch;

public class BranchListAdapter extends BaseAdapter {

    public interface OnBranchListener {
        void initConversation(Branch branch);

        void initCall(Branch branch);
    }

    private final LayoutInflater inflater;
    private final List<Branch> branches;

    private SharedPreferences preferences;
    private Context context;
    private OnBranchListener listener;
    private LatLng currentLocation;

    public BranchListAdapter(Context context, List<Branch> branches, OnBranchListener listener, final LatLng currentLocation) {
        this.context = context;
        this.listener = listener;

        inflater = LayoutInflater.from(context);
        this.branches = branches;

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.currentLocation = currentLocation;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder;
        if (view == null) {

            view = inflater.inflate(R.layout.item_list_branch, parent, false);
            holder = new ViewHolder();
            holder.branchName = (TextView) view.findViewById(R.id.tv_branch_name);
            holder.branchDistance = (TextView) view.findViewById(R.id.tv_branch_distance);
            holder.btnCall = (Button) view.findViewById(R.id.btn_call_branch);
            holder.btnMessage = (Button) view.findViewById(R.id.btn_message_branch);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        final Branch branch = branches.get(position);

        holder.branchName.setText(branch.getName());

        String distance = branch.distanceFromPoint(currentLocation);
        holder.branchDistance.setText(distance);

        holder.btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tryMakeCall(branch);
            }
        });

        holder.btnMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.initConversation(branch);
            }
        });

        if (!branch.isAllowsMessaging())
            holder.btnMessage.setEnabled(false);

        if (!branch.isAllowsPhoneCalling())
            holder.btnCall.setEnabled(false);

        return view;

    }

    private void tryMakeCall(Branch branch) {

        if (preferences.getBoolean(SettingsConstants.CALL_LIMITATION_ACKNOWLEDGE, false)) {
            listener.initCall(branch);
            return;
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(R.string.tattler_call_limitation_message);
        alertDialogBuilder.setPositiveButton(R.string.cancel, null)
                .setNegativeButton(R.string.tattler_call_acknowledge, new PositiveDialogListener(branch));

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public int getCount() {
        return branches.size();
    }

    @Override
    public Branch getItem(int position) {
        return branches.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {

        TextView branchName;
        TextView branchDistance;
        Button btnCall;
        Button btnMessage;

    }

    class PositiveDialogListener implements DialogInterface.OnClickListener {

        private final Branch branch;

        public PositiveDialogListener(Branch branch) {
            this.branch = branch;
        }

        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(SettingsConstants.CALL_LIMITATION_ACKNOWLEDGE, true);
            editor.apply();

            listener.initCall(branch);

        }

    }

}