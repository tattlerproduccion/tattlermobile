package mx.tattler.android.events.tattler.chat;

import java.util.List;

import mx.tattler.android.daos.TattlerMessage;

public class NewMessagesArrivedEvent {

    public final List<TattlerMessage> messages;

    public NewMessagesArrivedEvent(List<TattlerMessage> messages) {
        this.messages = messages;
    }

}
