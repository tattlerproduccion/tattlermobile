package mx.tattler.android.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ListActivity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.otto.Bus;

import mx.tattler.android.BusProvider;
import mx.tattler.android.R;

public abstract class AnimatedListBusActivity extends ListActivity {

    private Bus mBus;

    private TextView titleView;
    private TextView subtitleView;
    private ImageView logo;

    protected abstract View getContainerView();

    protected abstract View getProgressView();

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {

        final View containerView = getContainerView();
        final View progressView = getProgressView();

        if (containerView == null || progressView == null)
            return;

        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
            containerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    containerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            containerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getBus().unregister(this);
    }

    public void setBus(Bus bus) {
        mBus = bus;
    }

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    protected void inflateCustomActionbar() throws Exception {

        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.actionbar_tattler, null);

        ImageView actionbarUp = (ImageView) mCustomView.findViewById(R.id.iv_actionbar_up);
        actionbarUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavUtils.navigateUpFromSameTask(AnimatedListBusActivity.this);
            }
        });

        titleView = (TextView) mCustomView.findViewById(R.id.tv_title);
        subtitleView = (TextView) mCustomView.findViewById(R.id.tv_subtitle);
        logo = (ImageView) mCustomView.findViewById(R.id.iv_tattler_actionbar_logo);

        ActionBar actionBar = getActionBar();

        if (actionBar == null) {
            throw new Exception("Actionbar no existe");
        }

        actionBar.setIcon(new ColorDrawable(getResources().getColor(R.color.tattler_transparent)));
        actionBar.setCustomView(mCustomView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

    }

    protected void setActionbarTitle(String title, String subtitle) {
        titleView.setText(title);

        if (TextUtils.isEmpty(subtitle)) {
            subtitleView.setVisibility(View.GONE);
            titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) titleView.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        } else {
            subtitleView.setText(subtitle);
        }

    }

    protected void setActionbarSubtitle(String subtitle) {
        subtitleView.setText(subtitle);
    }

    protected void hideTattlerActionbarLogo() {
        logo.setVisibility(View.GONE);
    }

    protected void setCustomLogo(int resource) {
        logo.setImageResource(resource);
        logo.setPadding(0, 0, 0, 0);
    }

}
