package mx.tattler.android.listeners;

import android.util.Log;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;

public class ChatConnectionListener implements ConnectionListener {

    private static final String TAG = ChatConnectionListener.class.getName();

    @Override
    public void connected(XMPPConnection connection) {
        Log.d(TAG, "connected");
    }

    @Override
    public void authenticated(XMPPConnection connection) {
        Log.d(TAG, "authenticated");
    }

    @Override
    public void connectionClosed() {
        Log.d(TAG, "connectionClosed");
    }

    @Override
    public void connectionClosedOnError(final Exception e) {
        Log.d(TAG, "connectionClosedOnError: " + e.getLocalizedMessage());
    }

    @Override
    public void reconnectingIn(final int seconds) {
        if(seconds % 5 == 0) {
            Log.d(TAG, "reconnectingIn: " + seconds);
        }
    }

    @Override
    public void reconnectionSuccessful() {
        Log.d(TAG, "reconnectionSuccessful");
    }

    @Override
    public void reconnectionFailed(final Exception error) {
        Log.d(TAG, "reconnectionFailed: " + error.getLocalizedMessage());
    }
}
