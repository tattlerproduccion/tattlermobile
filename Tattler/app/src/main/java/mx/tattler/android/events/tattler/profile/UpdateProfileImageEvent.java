package mx.tattler.android.events.tattler.profile;

import retrofit.mime.TypedFile;

public class UpdateProfileImageEvent {

    public final TypedFile imageFile;

    public UpdateProfileImageEvent(TypedFile imageFile) {
        this.imageFile = imageFile;
    }

}
