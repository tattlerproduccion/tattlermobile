package mx.tattler.android.rest.tattler.models.chat;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

import mx.tattler.android.utils.text.DateTextHelper;

@Parcel
public class Message {

    public static final String MESSAGE_TYPE = "message_type";
    public static final String TATTLER_MESSAGE_ID = "tattler_message_id";
    public static final String DATE_SENT = "date_sent";

    public enum Types{
        Text, Video, Image, File, Geolocation, Promotion
    }

    public interface MessageTypes{

        int MESSAGE_TYPE_NORMAL = 0;
        int MESSAGE_TYPE_FILE_ATTACHMENT = 1;
        int MESSAGE_TYPE_LOCATION = 2;
        int MESSAGE_TYPE_PHOTO = 3;
        int MESSAGE_TYPE_VIDEO = 4;
        int MESSAGE_TYPE_PROMOTION = 5;

    }

    @Expose
    private String id;
    @Expose
    private String body;
    @Expose
    private String sender;
    @Expose
    private String receiver;
    @SerializedName("sent_at")
    @Expose
    private String sentAt;
    @SerializedName("conversation_id")
    @Expose
    private String conversationId;
    @Expose
    private String status;
    @Expose
    private String type;

    @SerializedName("messaging_id")
    @Expose
    private Integer tattlerReceiverId;

    private transient Uri dataUri;

    private transient boolean mine;

    public static Message toSentInstance(String body, Integer messagingId, String sender, Types type){

        Message message = new Message();
        message.setBody(body);
        message.setTattlerReceiverId(messagingId);
        message.setSender(sender);
        message.setType(type.name());
        message.setMine(true);

        return message;

    }

    public static String getLastMessageFromDb(String message, String type){

        if(message == null){
            return "";
        }

        if(type == null)
            return "Mensaje desconocido";

        String lastMessage = "";

        if(type.equals(Message.Types.Text.name())){
            lastMessage = message;
        }else if(type.equals(Message.Types.File.name())){
            lastMessage = "Archivo adjunto";
        }else if(type.equals(Message.Types.Image.name())){
            lastMessage = "Imagen";
        }else if(type.equals(Message.Types.Video.name())){
            lastMessage = "Video";
        }else if(type.equals(Message.Types.Geolocation.name())){
            lastMessage = "Ubicación";
        }else if(type.equals(Types.Promotion.name())){
            lastMessage = "Promoción";
        }

        return  lastMessage;

    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The body
     */
    public String getBody() {
        return body;
    }

    /**
     * @param body The body
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * @return The sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender The sender
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * @return The receiver
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * @param receiver The receiver
     */
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    /**
     * @return The sentAt
     */
    public String getSentAt() {
        return sentAt;
    }

    public Date getSentAtDate(){
        return DateTextHelper.parseMessageDate(this.sentAt);
    }

    /**
     * @param sentAt The sent_at
     */
    public void setSentAt(String sentAt) {
        this.sentAt = sentAt;
    }

    /**
     * @return The conversationId
     */
    public String getConversationId() {
        return conversationId;
    }

    /**
     * @param conversationId The conversation_id
     */
    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    public void markIfUserOwns(String currentUserEmail) {
        this.mine = this.sender.equals(currentUserEmail);
    }

    public void setMine(boolean mine) {
        this.mine = mine;
    }

    public boolean isMine() {
        return mine;
    }

    public void setTattlerReceiverId(Integer tattlerReceiverId) {
        this.tattlerReceiverId = tattlerReceiverId;
    }

    public Integer getTattlerReceiverId() {
        return tattlerReceiverId;
    }

    public Uri getDataUri() {
        return dataUri;
    }

    public void setDataUri(Uri dataUri) {
        this.dataUri = dataUri;
    }

    public boolean hasDataUri(){
        return this.dataUri != null;
    }

}