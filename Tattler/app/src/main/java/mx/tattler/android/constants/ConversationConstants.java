package mx.tattler.android.constants;

public class ConversationConstants {

    public static final String CONVERSATION = "CONVERSATION";
    public static final String PROMOTION = "PROMOTION";

    public static final int TATTLER_NOTIFICATION_ID = 399;
    public static final String TAG = ConversationConstants.class.getName();

    private ConversationConstants(){}


}
