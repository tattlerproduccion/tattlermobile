package mx.tattler.android.activities.wizard;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;
import com.tech.freak.wizardpager.ui.ReviewFragment;
import com.tech.freak.wizardpager.ui.StepPagerStrip;

import java.text.ParseException;
import java.util.List;

import mx.tattler.android.R;
import mx.tattler.android.activities.BusFragmentActivity;
import mx.tattler.android.activities.MainDrawerActivity;
import mx.tattler.android.events.ApiErrorEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileImageEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileImageResultEvent;
import mx.tattler.android.events.tattler.profile.UpdateProfileResultEvent;
import mx.tattler.android.fragments.FriendContactListFragment;
import mx.tattler.android.fragments.dialogs.ProgressDialogFragment;
import mx.tattler.android.fragments.pages.CustomImageFragment;
import mx.tattler.android.fragments.pages.CustomReviewFragment;
import mx.tattler.android.rest.tattler.models.User;
import mx.tattler.android.utils.TypedFileFactory;
import mx.tattler.android.utils.resource.ImageMediaHelper;
import mx.tattler.android.utils.text.DateTextHelper;
import retrofit.mime.TypedFile;

public class ProfileWizardActivity extends BusFragmentActivity implements
        PageFragmentCallbacks, ReviewFragment.Callbacks, ModelCallbacks {

    private static final String TAG = FriendContactListFragment.class.getName();

    private ViewPager mPager;
    private WizardPagerAdapter mPagerAdapter;

    private boolean mEditingAfterReview;

    private AbstractWizardModel mWizardModel = new ProfileWizardModel(this);

    private boolean mConsumePageSelectedEvent;

    private Button mNextButton;
    private Button mPrevButton;

    private List<Page> mCurrentPageSequence;
    private StepPagerStrip mStepPagerStrip;

    private boolean imageSelected = false;
    private Bundle profileBundle = null;
    private ProgressDialogFragment loadingDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_wizard);

        ActionBar actionBar = getActionBar();

        if(actionBar != null){
            View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_wizard, null);
            actionBar.setIcon(new ColorDrawable(getResources().getColor(R.color.tattler_transparent)));
            actionBar.setCustomView(mCustomView);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        }

        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }

        mWizardModel.registerListener(this);

        mPagerAdapter = new WizardPagerAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip
                .setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
                    @Override
                    public void onPageStripSelected(int position) {
                        position = Math.min(mPagerAdapter.getCount() - 1,
                                position);
                        if (mPager.getCurrentItem() != position) {
                            mPager.setCurrentItem(position);
                        }
                    }
                });

        mNextButton = (Button) findViewById(R.id.next_button);
        mPrevButton = (Button) findViewById(R.id.prev_button);

        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);

                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                updateBottomBar();
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {

                    profileBundle = mWizardModel.save();

                    updateProfile();

                } else {
                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            }
        });

        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });

        onPageTreeChanged();
        updateBottomBar();
    }

    private void updateProfile() {

        if (profileBundle == null)
            return;

        Bundle genderBundle = profileBundle.getBundle("Sexo");
        Bundle profileInfoBundle = profileBundle.getBundle("¿Cómo quieres que te vean?");

        String gender = genderBundle.getString("gender");

        String userGender = gender.equals("Hombre") ? "Male" : "Female";

        User user = new User();
        user.setName(profileInfoBundle.getString("first_name"));
        user.setLastName(profileInfoBundle.getString("last_name"));
        user.setGender(userGender);


        try {
            String rawDate = profileInfoBundle.getString("birthday");
            String birthDate = DateTextHelper.formatFromWizardDate(rawDate);
            user.setBirthDate(birthDate);
        } catch (ParseException ignored) {
        }


        getBus().post(new UpdateProfileEvent(user));

        FragmentManager fm = getSupportFragmentManager();
        loadingDialog = ProgressDialogFragment.newInstance(R.string.finishing_wizard);
        loadingDialog.show(fm, "fragment_wizard_loading");


    }

    @Subscribe
    public void onProfileUpdated(UpdateProfileResultEvent event) throws Exception {

        Bundle imageBundle = profileBundle.getBundle("Agrega tu foto");

        if (imageBundle.isEmpty()) {
            finishWizard();
            return;
        }

        Uri rawUri = Uri.parse(imageBundle.getString("_"));
        Uri imageUri = ImageMediaHelper.resizedBitmapUriIfNeeded(this, rawUri);
        TypedFile file = TypedFileFactory.create(this, imageUri);
        getBus().post(new UpdateProfileImageEvent(file));

    }

    @Subscribe
    public void onImageUpdated(UpdateProfileImageResultEvent event) {
        finishWizard();
    }


    @Subscribe
    public void onApiError(ApiErrorEvent event) {
        Log.e(TAG, event.error.getRawBody());
        Toast.makeText(this, "No pudimos actualizar tu perfil", Toast.LENGTH_SHORT).show();
        loadingDialog.dismiss();
    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // + 1 =
        // review
        // step
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }

    private void finishWizard() {

        loadingDialog.dismiss();

        startActivity(new Intent(this, MainDrawerActivity.class));
        finish();
    }

    private void updateBottomBar() {
        int position = mPager.getCurrentItem();

        if (position == 2) {
            String value = imageSelected ? "Continuar" : "Omitir";
            mNextButton.setText(value);
        } else if (position == mCurrentPageSequence.size()) {
            mNextButton.setText(R.string.finish);
        } else {
            mNextButton.setText(mEditingAfterReview ? R.string.review_data
                    : R.string.next);

            TypedValue v = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v,
                    true);
            mNextButton.setTextAppearance(this, v.resourceId);
            mNextButton.setTextColor(Color.WHITE);
            mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
        }

        mPrevButton
                .setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("model", mWizardModel.save());
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }

    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof CustomImageFragment) {
                imageSelected = true;
                updateBottomBar();
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    public class WizardPagerAdapter extends FragmentStatePagerAdapter {

        private int mCutOffPage;
        private Fragment mPrimaryItem;

        public WizardPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            if (i >= mCurrentPageSequence.size()) {
                return new CustomReviewFragment();
            }

            return mCurrentPageSequence.get(i).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }

            return POSITION_NONE;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position,
                                   Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;
        }

        @Override
        public int getCount() {
            return Math.min(mCutOffPage + 1, mCurrentPageSequence == null ? 1
                    : mCurrentPageSequence.size() + 1);
        }

        public void setCutOffPage(int cutOffPage) {
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            mCutOffPage = cutOffPage;
        }

        public int getCutOffPage() {
            return mCutOffPage;
        }
    }
}
