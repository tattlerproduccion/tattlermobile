package mx.tattler.android.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;

import mx.tattler.android.utils.resource.FileUtils;
import retrofit.mime.TypedFile;

public class TypedFileFactory {

    private static final String TAG = TypedFileFactory.class.getName();

    private TypedFileFactory(){}

    public static TypedFile create(Context context, Uri uri){
        ContentResolver cR = context.getContentResolver();
        String mime = cR.getType(uri);

        if(mime == null){
            mime = getMimeTypeFallback(uri.toString());
        }

        File imageFile = new File(FileUtils.getPath(context, uri));
        Log.i(TAG, String.format("Mime type for uri: %s, %s", uri.toString(), mime));
        return new TypedFile(mime, imageFile);
    }

    private static String getMimeTypeFallback(String url)
    {
        String extension = url.substring(url.lastIndexOf("."));
        String mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extension);
        String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap);
        return mimeType;
    }

}
