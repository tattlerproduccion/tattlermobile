package mx.tattler.android.fragments.search;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;

import mx.tattler.android.R;
import mx.tattler.android.fragments.BusFragment;
import mx.tattler.android.utils.social.SocialNetworkHelper;

public class ShareWithOthersFragment extends BusFragment {

    private UiLifecycleHelper uiHelper;

    public ShareWithOthersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        uiHelper = new UiLifecycleHelper(getActivity(), null);
        uiHelper.onCreate(savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_share_with_others, container, false);

        final String shareMessage = getActivity().getString(R.string.tattler_share_message);
        final String twitterMessage = getActivity().getString(R.string.tattler_twitter_share_message);

        Button whatsapp = (Button)rootView.findViewById(R.id.btn_share_whatsapp);
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SocialNetworkHelper.shareWhatsApp(getActivity(), shareMessage);
            }
        });

        Button facebook = (Button)rootView.findViewById(R.id.btn_share_facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SocialNetworkHelper.shareOnFacebook(getActivity(), shareMessage, ShareWithOthersFragment.this);
            }
        });

        Button twitter = (Button)rootView.findViewById(R.id.btn_share_twitter);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SocialNetworkHelper.shareOnTwitter(getActivity(), twitterMessage);
            }
        });

        Button instagram = (Button)rootView.findViewById(R.id.btn_share_instagram);
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SocialNetworkHelper.shareOnInstagram(getActivity(), shareMessage);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });
    }

}
