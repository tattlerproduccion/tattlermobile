package mx.tattler.android.events.tattler;

import java.util.List;

import mx.tattler.android.rest.tattler.models.Company;

/**
 * Created by irving on 11/11/14.
 */
public class AllCompaniesResultEvent {

    public List<Company> companies;

    public AllCompaniesResultEvent(List<Company> companies) {
        this.companies = companies;
    }
}
