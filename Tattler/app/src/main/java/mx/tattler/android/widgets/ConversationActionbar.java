package mx.tattler.android.widgets;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import mx.tattler.android.R;
import mx.tattler.android.daos.TattlerConversation;

public class ConversationActionbar extends RelativeLayout {

    private static final String TAG = ConversationActionbar.class.getName();

    private Picasso picasso;

    private ImageView companyImageView;
    private TextView companyName;
    private TextView branchName;
    private ProgressBar progressBar;

    public static ConversationActionbar instance(ActionBar actionBar, final Activity activity, TattlerConversation conversation) {

        if (actionBar == null) {
            return null;
        }

        ConversationActionbar conversationActionbar;

        LayoutInflater mInflater = LayoutInflater.from(activity);
        View mCustomView = mInflater.inflate(R.layout.actionbar_conversation, null);

        ImageView actionbarUp = (ImageView) mCustomView.findViewById(R.id.iv_actionbar_up);
        actionbarUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                NavUtils.navigateUpFromSameTask(activity);
            }
        });

        conversationActionbar = (ConversationActionbar) mCustomView.findViewById(R.id.actionbar_conversation_header);
        conversationActionbar.setData(conversation);

        actionBar.setIcon(new ColorDrawable(activity.getResources().getColor(R.color.tattler_transparent)));
        actionBar.setCustomView(mCustomView);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        return conversationActionbar;

    }

    public ConversationActionbar(Context context) {
        super(context);
        init(context);
    }

    public ConversationActionbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ConversationActionbar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void setData(final TattlerConversation conversation) {

        String imagePath = conversation.getReceiverPicture();

        if (TextUtils.isEmpty(imagePath)) {
            picasso.load(R.drawable.ic_default_company).error(R.drawable.ic_default_company).into(companyImageView);
        } else {
            picasso.load(imagePath).error(R.drawable.ic_default_company).into(companyImageView);
        }

        companyName.setText(conversation.getReceiverCompanyName());
        branchName.setText(conversation.getReceiverName());

    }

    public void showLoading(boolean show) {

        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }

    }

    private void init(final Context context) {

        if (!isInEditMode()) {
            picasso = Picasso.with(context);
        }

        View.inflate(context, R.layout.actionbar_conversation_header, this);
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);

        companyImageView = (ImageView) findViewById(R.id.iv_receiver_image);
        companyName = (TextView) findViewById(R.id.tv_title);
        branchName = (TextView) findViewById(R.id.tv_subtitle);
        progressBar = (ProgressBar) findViewById(R.id.pb_conversation_loading);

    }

}