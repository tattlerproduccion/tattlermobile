package mx.tattler.android.rest.tattler.models;

public class VisitCompanyPromotionsEvent {

    public final String companyId;

    public VisitCompanyPromotionsEvent(String companyId) {
        this.companyId = companyId;
    }

}
