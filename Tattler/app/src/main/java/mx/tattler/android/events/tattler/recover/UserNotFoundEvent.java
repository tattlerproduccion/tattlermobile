package mx.tattler.android.events.tattler.recover;

public class UserNotFoundEvent {

    public final String email;

    public UserNotFoundEvent(String email) {
        this.email = email;
    }

}
