package mx.tattler.android.events.tattler.recover;

public class QueryUserEvent {

    public final String email;

    public QueryUserEvent(String email) {
        this.email = email;
    }
}
