package mx.tattler.android.events.tattler.profile;

import mx.tattler.android.rest.tattler.models.profile.BirthDate;

public class ProfileBirthDateUpdatedEvent {

    public final BirthDate birthDate;

    public ProfileBirthDateUpdatedEvent(BirthDate birthDate) {
        this.birthDate = birthDate;
    }

}