package mx.tattler.android.events.tattler;

public class DeleteFavoriteEvent {

    public final String companyId;

    public DeleteFavoriteEvent(String companyId) {
        this.companyId = companyId;
    }
}
