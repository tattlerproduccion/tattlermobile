package mx.tattler.android.events.tattler.recover;

import mx.tattler.android.events.ApiErrorEvent;
import retrofit.RetrofitError;

public class UpdatePasswordErrorEvent extends ApiErrorEvent {

    public UpdatePasswordErrorEvent(RetrofitError retrofitError) {
        super(retrofitError);
    }

}
