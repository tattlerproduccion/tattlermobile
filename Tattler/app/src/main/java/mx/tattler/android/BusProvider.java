package mx.tattler.android;

import com.squareup.otto.Bus;

import mx.tattler.android.utils.bus.MainThreadBus;

public final class BusProvider {

    private static final Bus BUS = new MainThreadBus();

    public static Bus getInstance() {
        return BUS;
    }

    private BusProvider() {
    }

}